﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Quirco.Cqrs.Auditor.Commands;
using Quirco.Cqrs.Auditor.DataAccess;
using Quirco.Cqrs.Auditor.Interfaces;

namespace Quirco.Cqrs.Auditor;

public static class Extensions
{
    /// <summary>
    /// Registers auditing pipelines and audit database context
    /// </summary>
    public static IServiceCollection AddAuditor(
        this IServiceCollection serviceCollection,
        Action<DbContextOptionsBuilder> contextOptionsBuilder)
    {
        serviceCollection.AddDbContextFactory<AuditDataContext>(contextOptionsBuilder);
        serviceCollection.AddScoped<IAuditScopeFactory, AuditScopeFactory>();
        serviceCollection.AddScoped<IAuditEventsRepository, AuditEventsRepository>();
        serviceCollection.AddScoped(typeof(IPipelineBehavior<,>), typeof(CommandProcessingAuditorBehavior<,>));

        return serviceCollection;
    }
}