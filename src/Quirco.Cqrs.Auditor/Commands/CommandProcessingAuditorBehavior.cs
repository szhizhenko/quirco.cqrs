﻿using MediatR;
using Quirco.Cqrs.Auditor.Core.Interfaces;
using Quirco.Cqrs.Auditor.Interfaces;

namespace Quirco.Cqrs.Auditor.Commands;

public class CommandProcessingAuditorBehavior<TCommand, TResponse> : IPipelineBehavior<TCommand, TResponse>
    where TCommand : notnull
{
    private readonly IAuditScopeFactory _auditScopeFactory;
    private readonly IEnumerable<ICommandProcessingAuditor<TCommand>> _processorsNoResponse;
    private readonly ICollection<ICommandProcessingAuditor<TCommand, TResponse>> _processors;

    public CommandProcessingAuditorBehavior(
        IAuditScopeFactory auditScopeFactory,
        IEnumerable<ICommandProcessingAuditor<TCommand, TResponse>> processors,
        IEnumerable<ICommandProcessingAuditor<TCommand>> processorsNoResponse)
    {
        _auditScopeFactory = auditScopeFactory;
        _processorsNoResponse = processorsNoResponse;
        _processors = processors.ToList();
    }

    public async Task<TResponse> Handle(TCommand request, RequestHandlerDelegate<TResponse> next, CancellationToken cancellationToken)
    {
        if (request is IAuditableCommand && (_processors.Any() || _processorsNoResponse.Any()))
        {
            await using var scope = await _auditScopeFactory.Create(null, cancellationToken);

            foreach (var processor in _processors)
            {
                await processor.PreProcess(scope, request, cancellationToken);
            }

            foreach (var processor in _processorsNoResponse)
            {
                await processor.PreProcess(scope, request, cancellationToken);
            }

            var result = await next();

            foreach (var processor in _processorsNoResponse)
            {
                await processor.PostProcess(scope, request, cancellationToken);
            }

            foreach (var processor in _processors)
            {
                await processor.PostProcess(scope,
                                            request,
                                            result,
                                            cancellationToken);
            }

            return result;
        }

        return await next();
    }
}