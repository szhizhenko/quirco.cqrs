﻿using Quirco.Cqrs.Auditor.Core.Interfaces;
using Quirco.Cqrs.Auditor.Interfaces;

namespace Quirco.Cqrs.Auditor.Commands;

/// <summary>
/// Base abstract class to implement some command auditor
/// </summary>
/// <typeparam name="TCommand">Type of command this auditor is processing</typeparam>
public abstract class CommandProcessingAuditor<TCommand> : ICommandProcessingAuditor<TCommand>
    where TCommand : IAuditableCommand
{
    /// <inheritdoc cref="ICommandProcessingAuditor{TCommand,TResponse}"/>
    public virtual ValueTask PreProcess(AuditScope auditScope, TCommand command, CancellationToken cancellationToken)
    {
        return ValueTask.CompletedTask;
    }

    /// <inheritdoc cref="ICommandProcessingAuditor{TCommand,TResponse}"/>
    public virtual ValueTask PostProcess(
        AuditScope auditScope,
        TCommand command,
        CancellationToken cancellationToken)
    {
        return ValueTask.CompletedTask;
    }
}

/// <summary>
/// Base abstract class to implement some command auditor
/// </summary>
/// <typeparam name="TCommand">Type of command this auditor is processing</typeparam>
/// <typeparam name="TResponse">Type of command execution result</typeparam>
public abstract class CommandProcessingAuditor<TCommand, TResponse> : ICommandProcessingAuditor<TCommand, TResponse>
    where TCommand : IAuditableCommand
{
    /// <inheritdoc cref="ICommandProcessingAuditor{TCommand,TResponse}"/>
    public virtual ValueTask PreProcess(AuditScope auditScope, TCommand command, CancellationToken cancellationToken)
    {
        return ValueTask.CompletedTask;
    }

    /// <inheritdoc cref="ICommandProcessingAuditor{TCommand,TResponse}"/>
    public virtual ValueTask PostProcess(
        AuditScope auditScope,
        TCommand command,
        TResponse? response,
        CancellationToken cancellationToken)
    {
        return ValueTask.CompletedTask;
    }
}