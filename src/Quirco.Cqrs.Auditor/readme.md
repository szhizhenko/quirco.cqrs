To generate migration use Demo as strartup project. Keep in mind that in-memory EF provider does not supports migrations, so you need to replace
configuration from `.UseInMemoryDatabase` to `.UseNpgsql`. Revert back when migration will be applied.

Migrations command:
```shell
dotnet ef migrations add Initial --project ./Quirco.Cqrs.Auditor/Quirco.Cqrs.Auditor.csproj --startup-project demo/Quirco.Cqrs.Demo/Quirco.Cqrs.Demo.csproj --context Quirco.Cqrs.Auditor.DataAccess.AuditDataContext
```