﻿using Microsoft.EntityFrameworkCore;
using Quirco.Cqrs.Auditor.DataAccess.Models;
using Quirco.Cqrs.DataAccess.DataContext;

namespace Quirco.Cqrs.Auditor.DataAccess;

public class AuditDataContext : DbContext
{
    public DbSet<AuditEvent> AuditEvents { get; set; }

    protected AuditDataContext()
    {
    }

    public AuditDataContext(DbContextOptions<AuditDataContext> options) : base(options)
    {
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);
        modelBuilder.ApplyConfigurationsFromAssembly(GetType().Assembly);
        modelBuilder.EnumsAsString();
    }
}