﻿using Quirco.Cqrs.Auditor.Core.Models;
using Quirco.Cqrs.Auditor.DataAccess.Models;
using Quirco.Cqrs.Core.Models;

namespace Quirco.Cqrs.Auditor.DataAccess;

/// <summary>
/// Repository for store/retrieve audit events
/// </summary>
public interface IAuditEventsRepository : IDisposable
{
    /// <summary>
    /// Search for events
    /// </summary>
    ValueTask<PagedResponse<AuditEventModel>> FindEvents(AuditEventFilter filter, CancellationToken cancellationToken);

    /// <summary>
    /// Save list of events into store
    /// </summary>
    ValueTask<int> Save(IEnumerable<AuditEvent> events, CancellationToken cancellationToken);
}