﻿using Mapster;
using Microsoft.EntityFrameworkCore;
using Quirco.Cqrs.Auditor.Core.Models;
using Quirco.Cqrs.Auditor.DataAccess.Models;
using Quirco.Cqrs.Core.Models;

namespace Quirco.Cqrs.Auditor.DataAccess;

public class AuditEventsRepository : IAuditEventsRepository
{
    private readonly AuditDataContext _context;

    public AuditEventsRepository(AuditDataContext context)
    {
        _context = context;
    }

    public async ValueTask<PagedResponse<AuditEventModel>> FindEvents(AuditEventFilter filter, CancellationToken cancellationToken)
    {
        var query = _context.AuditEvents
            .AsNoTracking()
            .OrderByDescending(e => e.DateTime)
            .AsQueryable();

        if (filter.EntityType is not null)
            query = query.Where(q => q.EntityType == filter.EntityType);

        if (filter.EntityId is not null)
            query = query.Where(q => q.EntityId == filter.EntityId);

        if (filter.InnerEntityType is not null)
            query = query.Where(q => q.InnerEntityType == filter.InnerEntityType);

        if (filter.InnerEntityId is not null)
            query = query.Where(q => q.InnerEntityId == filter.InnerEntityId);

        if (filter.Offset > 0)
            query = query.Skip(filter.Offset);

        if (filter.Limit is not null)
            query = query.Take(filter.Limit.Value);

        if (filter.DateFrom is not null)
            query = query.Where(q => q.DateTime >= filter.DateFrom);

        if (filter.DateTo is not null)
            query = query.Where(q => q.DateTime < filter.DateTo);

        var res = await query.ProjectToType<AuditEventModel>().ToArrayAsync(cancellationToken);
        var count = await query.CountAsync(cancellationToken);

        return new PagedResponse<AuditEventModel>(res, count);
    }

    public async ValueTask<int> Save(IEnumerable<AuditEvent> events, CancellationToken cancellationToken)
    {
        await _context.AuditEvents.AddRangeAsync(events, cancellationToken);

        return await _context.SaveChangesAsync(cancellationToken);
    }

    public void Dispose()
    {
        _context.Dispose();
    }
}