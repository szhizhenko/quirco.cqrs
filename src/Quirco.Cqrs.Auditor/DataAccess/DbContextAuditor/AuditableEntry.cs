using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace Quirco.Cqrs.Auditor.DataAccess.DbContextAuditor;

public class AuditableEntry
{
    /// <summary>
    /// Entity state
    /// </summary>
    public EntityState State { get; }

    /// <summary>
    /// Entry from context
    /// </summary>
    public EntityEntry EntityEntry { get; }

    /// <summary>
    /// Entity instance
    /// </summary>
    public object Entity => EntityEntry.Entity;

    public AuditableEntry(EntityState state, EntityEntry entityEntry)
    {
        State = state;
        EntityEntry = entityEntry;
    }

    public override string ToString() => $"{State}({Entity.GetType().Name})";
}