using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Quirco.Cqrs.Auditor.Core.Notifications;
using Quirco.Cqrs.DataAccess.Models;

namespace Quirco.Cqrs.Auditor.DataAccess.DbContextAuditor;

/// <summary>
///     <para>
///         Interceptor to automatically fill audit fields (created/modified/deleted date, created user).
///         Also allows to file Mediator events before and after entities update.
///     </para>
/// </summary>
public class DbContextAuditorInterceptor<TUserId> : SaveChangesInterceptor
    where TUserId : struct
{
    public delegate bool TryGetValueDelegate<T>(out T output);
    private readonly IMediator _mediator;
    private readonly HashSet<Type> _auditableTypes;
    private readonly TryGetValueDelegate<TUserId>? _tryGetUserId;
    private AuditableEntry[]? _modifiedEntries;

    public DbContextAuditorInterceptor(
        IMediator mediator,
        HashSet<Type> auditableTypes,
        TryGetValueDelegate<TUserId>? tryGetUserId = null)
    {
        _mediator = mediator;
        _auditableTypes = auditableTypes;
        _tryGetUserId = tryGetUserId;
    }

    /// <inheritdoc />
    public override InterceptionResult<int> SavingChanges(DbContextEventData eventData, InterceptionResult<int> result)
    {
        if (eventData.Context != null)
        {
            ProcessSavingChanges(eventData.Context, CancellationToken.None).GetAwaiter().GetResult();
        }

        return result;
    }

    /// <inheritdoc />
    public override async ValueTask<InterceptionResult<int>> SavingChangesAsync(
        DbContextEventData eventData,
        InterceptionResult<int> result,
        CancellationToken cancellationToken = default)
    {
        if (eventData.Context != null)
        {
            await ProcessSavingChanges(eventData.Context, cancellationToken);
        }

        return result;
    }

    /// <inheritdoc />
    public override int SavedChanges(SaveChangesCompletedEventData eventData, int result)
    {
        if (eventData.Context != null && result > 0)
        {
            ProcessSavedChanges(CancellationToken.None).GetAwaiter().GetResult();
        }

        return result;
    }

    /// <inheritdoc />
    public override async ValueTask<int> SavedChangesAsync(
        SaveChangesCompletedEventData eventData,
        int result,
        CancellationToken cancellationToken = default)
    {
        if (eventData.Context != null && result > 0)
        {
            await ProcessSavedChanges(cancellationToken);
        }

        return result;
    }

    private async Task ProcessSavingChanges(DbContext context, CancellationToken cancellationToken)
    {
        _modifiedEntries = context.ChangeTracker
            .Entries()
            .Where(x => x.State != EntityState.Unchanged)
            .Select(x => new AuditableEntry(x.State, x))
            .ToArray();

        if (_modifiedEntries.Any())
        {
            UpdateAuditFields(_modifiedEntries, _tryGetUserId);
            await FireEntityEvents(_modifiedEntries, typeof(EntityUpdatingNotification<>), cancellationToken);
        }
    }

    private async Task ProcessSavedChanges(CancellationToken cancellationToken)
    {
        if (_modifiedEntries != null && _modifiedEntries.Length > 0)
        {
            await FireEntityEvents(_modifiedEntries, typeof(EntityUpdatedNotification<>), cancellationToken);
        }

        _modifiedEntries = Array.Empty<AuditableEntry>();
    }

    private static void UpdateAuditFields(
        IEnumerable<AuditableEntry> entries,
        TryGetValueDelegate<TUserId>? tryGetUserId)
    {
        var userInitialized = false;
        var userId = default(TUserId);

        foreach (var entry in entries)
        {
            if (entry.State == EntityState.Added && entry.Entity is IHasDateCreated newEntry)
            {
                newEntry.DateCreated = DateTime.UtcNow;

                if (entry.Entity is IHasUserCreated<TUserId> hasUser)
                {
                    if (!userInitialized && tryGetUserId is not null && tryGetUserId(out userId))
                    {
                        userInitialized = true;
                    }

                    if (userInitialized)
                        hasUser.UserCreatedId = userId;
                }
                else if (entry.Entity is IHasUserCreated<TUserId?> hasUserNullable)
                {
                    if (!userInitialized && tryGetUserId is not null && tryGetUserId(out userId))
                    {
                        userInitialized = true;
                    }

                    if (userInitialized)
                        hasUserNullable.UserCreatedId = userId;
                }
            }

            if (entry.State is EntityState.Modified or EntityState.Added &&
                entry.Entity is IHasDateModified modifiedEntry)
            {
                modifiedEntry.DateModified = DateTime.UtcNow;
            }

            if (entry.State == EntityState.Deleted && entry.Entity is IHasDateDeleted deletedEntry)
            {
                deletedEntry.DateDeleted = DateTime.UtcNow;
                entry.EntityEntry.State = EntityState.Modified;
            }
        }
    }

    /// <summary>
    /// Fires entity changed events to mediator pipeline
    /// </summary>
    /// <param name="modifiedEntries">List of modified entities</param>
    /// <param name="eventType">Type of event to fire (updating or updated)</param>
    /// <param name="cancellationToken">Cancellation token</param>
    protected async Task FireEntityEvents(
        IEnumerable<AuditableEntry> modifiedEntries,
        Type eventType,
        CancellationToken cancellationToken)
    {
        foreach (var entry in modifiedEntries.Where(e => _auditableTypes.Contains(e.Entity.GetType())))
        {
            var genericType = eventType.MakeGenericType(entry.Entity.GetType());
            var evt = Activator.CreateInstance(genericType, entry.Entity, MapEfState(entry.State));

            if (evt != null)
            {
                await _mediator.Publish(evt, cancellationToken);
            }
        }
    }

    private static EntityAction MapEfState(EntityState state)
    {
        return state switch
        {
            EntityState.Added => EntityAction.Added,
            EntityState.Deleted => EntityAction.Deleted,
            EntityState.Modified => EntityAction.Modified,
            _ => throw new NotSupportedException()
        };
    }
}