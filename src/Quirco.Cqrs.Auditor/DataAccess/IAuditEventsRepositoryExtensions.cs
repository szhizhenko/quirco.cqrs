﻿using Quirco.Cqrs.Auditor.Core.Models;
using Quirco.Cqrs.Core.Models;

namespace Quirco.Cqrs.Auditor.DataAccess;

public static class IAuditEventsRepositoryExtensions
{
    /// <summary>
    /// Get audit records for entity
    /// </summary>
    /// <param name="id">Entity id</param>
    /// <param name="filter">Additional filter parameters (paging, dates, user etc)</param>
    /// <typeparam name="TEntity">Type of entity</typeparam>
    public static ValueTask<PagedResponse<AuditEventModel>> GetForEntity<TEntity, TId>(
        this IAuditEventsRepository repository,
        TId id,
        AuditEventFilter filter,
        CancellationToken cancellationToken)
    {
        filter.EntityType = typeof(TEntity).Name;
        filter.EntityId = Convert.ToString(id);
        return repository.FindEvents(filter, cancellationToken);
    }

    /// <summary>
    /// Get audit records for inner entity
    /// </summary>
    /// <param name="id">Entity id</param>
    /// <param name="filter">Additional filter parameters (paging, dates, user etc)</param>
    /// <typeparam name="TEntity">Type of entity</typeparam>
    public static ValueTask<PagedResponse<AuditEventModel>> GetForInnerEntity<TEntity, TId>(
        this IAuditEventsRepository repository,
        TId id,
        AuditEventFilter filter,
        CancellationToken cancellationToken)
    {
        filter.InnerEntityType = typeof(TEntity).Name;
        filter.InnerEntityId = Convert.ToString(id);
        return repository.FindEvents(filter, cancellationToken);
    }
}