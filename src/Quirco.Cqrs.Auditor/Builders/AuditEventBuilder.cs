﻿using Mapster;
using Quirco.Cqrs.Auditor.Core.Models;
using Quirco.Cqrs.Auditor.DataAccess.Models;

namespace Quirco.Cqrs.Auditor.Builders;

public class AuditEventBuilder
{
    private readonly AuditScope _auditScope;
    public const string PropertyChangedEventType = "PropertyChanged";

    private bool _eventAdded;

    private AuditEvent _eventParameters { get; }

    static AuditEventBuilder()
    {
        TypeAdapterConfig<AuditEvent, AuditEvent>.NewConfig()
            .Ignore(r => r.IpAddress)
            .AfterMapping((src, dest) => dest.IpAddress = src.IpAddress);
    }

    public AuditEventBuilder(AuditScope auditScope, AuditEvent eventParameters)
    {
        _auditScope = auditScope;
        _eventParameters = eventParameters;
    }

    internal AuditEventBuilder WithEntity<TEntity>()
    {
        _eventParameters.EntityType = typeof(TEntity).Name;

        return this;
    }

    internal AuditEventBuilder WithEntity<TEntity, TId>(TId? id)
    {
        _eventParameters.EntityType = typeof(TEntity).Name;

        if (id is not null)
        {
            _eventParameters.EntityId = Convert.ToString(id);
        }

        return this;
    }

    /// <summary>
    /// Specifies event severity. See <see cref="AuditEventSeverity"/> for details
    /// </summary>
    public AuditEventBuilder WithSeverity(AuditEventSeverity severity)
    {
        _eventParameters.Severity = severity;

        return this;
    }

    /// <summary>
    /// Specifies inner or child (more detailed) entity this event is related to
    /// </summary>
    /// <param name="id">Id of entity</param>
    /// <typeparam name="TEntity">Type of entity</typeparam>
    /// <typeparam name="TId">Type of id</typeparam>
    public AuditEventBuilder ForInnerEntity<TEntity, TId>(TId id)
    {
        _eventParameters.InnerEntityType = typeof(TEntity).Name;
        _eventParameters.InnerEntityId = Convert.ToString(id);

        return this;
    }

    /// <summary>
    /// Set type of event
    /// </summary>
    internal AuditEventBuilder EventType(string eventType)
    {
        _eventParameters.EventType = eventType;

        return this;
    }

    /// <summary>
    /// Set message for audit event
    /// </summary>
    public AuditEventBuilder Message(string message)
    {
        _eventParameters.Message = message;

        return this;
    }

    /// <summary>
    /// Add audit event if property value changed. If no value change occured, event wouldn't be added
    /// </summary>
    /// <param name="propertyName">Name of property</param>
    /// <param name="oldValue">Old property value</param>
    /// <param name="newValue">New property value</param>
    public AuditEventBuilder PropertyChanged(string propertyName, object? oldValue, object? newValue)
    {
        PropertyChanged(propertyName, new PropertyValue(oldValue), new PropertyValue(newValue));

        return this;
    }

    /// <summary>
    /// Add audit event if property value changed. If no value change occured, event wouldn't be added
    /// </summary>
    /// <param name="propertyName">Name of property</param>
    /// <param name="oldValue">Old property value including accessor to human-readable value</param>
    /// <param name="newValue">New property value including accessor to human-readable value</param>
    public AuditEventBuilder PropertyChanged(string propertyName, PropertyValue oldValue, PropertyValue newValue)
    {
        if (Equals(oldValue, newValue))
            return this;

        _eventParameters.EventType = string.IsNullOrEmpty(_eventParameters.EventType)
            ? PropertyChangedEventType
            : _eventParameters.EventType;

        _eventParameters.Parameter = propertyName;
        _eventParameters.OldValue = oldValue.Value;
        _eventParameters.NewValue = newValue.Value;
        _eventParameters.OldValueReadable = oldValue.ReadableValue;
        _eventParameters.NewValueReadable = newValue.ReadableValue;

        _eventAdded = true;
        _auditScope.AddEvent(Build());

        _eventParameters.EventType = null;
        _eventParameters.Parameter = null;
        _eventParameters.OldValue = _eventParameters.NewValue = null;
        _eventParameters.OldValueReadable = _eventParameters.NewValueReadable = null;

        return this;
    }

    internal void Finish()
    {
        if (!_eventAdded)
            _auditScope.AddEvent(Build());
    }

    /// <summary>
    /// Property value descriptor. Includes both "raw" value and human-readable
    /// </summary>
    public record PropertyValue
    {
        /// <summary>
        /// Value of property
        /// </summary>
        public string? Value { get; }

        /// <summary>
        /// Human-readable value of property
        /// </summary>
        public string? ReadableValue => ReadableValueAccessor?.Invoke() ?? Value;

        private Func<string>? ReadableValueAccessor { get; }

        public PropertyValue(object? value, Func<string>? readableValueAccessor = null)
        {
            Value = value is null
                ? null
                : Convert.ToString(value);

            ReadableValueAccessor = readableValueAccessor;
        }
    }

    /// <summary>
    /// Makes a clone for event parameters
    /// </summary>
    protected AuditEvent Build()
    {
        return _eventParameters.Adapt<AuditEvent>();
    }
}