﻿using Microsoft.AspNetCore.Http;
using Quirco.Cqrs.Auditor.Interfaces;
using Quirco.Security;
using Quirco.Security.Extensions;
using Quirco.Security.Principal;

namespace Quirco.Cqrs.Auditor;

public class AuditScopeFactory : IAuditScopeFactory
{
    private readonly IServiceProvider _serviceProvider;
    private readonly IHttpContextAccessor? _contextAccessor;
    private readonly IPrincipalAccessor? _principalAccessor;

    public AuditScopeFactory(
        IServiceProvider serviceProvider,
        IHttpContextAccessor? contextAccessor,
        IPrincipalAccessor? principalAccessor
    )
    {
        _serviceProvider = serviceProvider;
        _contextAccessor = contextAccessor;
        _principalAccessor = principalAccessor;
    }

    public async Task<AuditScope> Create(Action<AuditScopeOptions>? optionsBuilder, CancellationToken cancellationToken)
    {
        var options = await BuildOptions();

        optionsBuilder?.Invoke(options);

        var scope = new AuditScope(options, _serviceProvider);

        return scope;
    }

    private async Task<AuditScopeOptions> BuildOptions()
    {
        var options = new AuditScopeOptions();

        var httpContext = _contextAccessor?.HttpContext;

        if (httpContext is not null)
        {
            options.WithIpAddress(httpContext.Connection.RemoteIpAddress);
        }

        if (options.CorrelationId is null)
        {
            if (httpContext?.Request.Headers.TryGetValue("x-correlation-id", out var correlationId) == true)
                options.WithCorrelationId(correlationId.ToString());
            else
                options.WithCorrelationId(Guid.NewGuid().ToString());
        }

        if (_principalAccessor is not null)
        {
            var principal = await _principalAccessor.GetPrincipal();

            if (options.UserId is null && principal.TryGetClaimValue<string>(Constants.ClaimTypes.UserId, out var userId))
            {
                options.WithUserId(userId);
            }

            if (options.UserName is null && principal.TryGetClaimValue<string>(Constants.ClaimTypes.Login, out var userName))

            {
                options.WithUserName(userName);
            }
        }

        return options;
    }
}