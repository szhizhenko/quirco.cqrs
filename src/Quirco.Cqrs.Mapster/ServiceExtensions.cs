﻿using System.Reflection;
using Mapster;
using MapsterMapper;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using IMapper = Quirco.Cqrs.Core.Mapping.IMapper;
using IMapsterMapper = MapsterMapper.IMapper;

namespace Quirco.Cqrs.Mapster;

public static class ServiceExtensions
{
    /// <summary>
    /// Adds automapper and initializes mapping adapter
    /// </summary>
    public static IServiceCollection AddMapsterAdapter(
        this IServiceCollection services,
        params Assembly[] assemblies)
    {
        var typeAdapterConfig = TypeAdapterConfig.GlobalSettings;
        typeAdapterConfig.Scan(assemblies);
        services.AddSingleton(typeAdapterConfig);
        services.TryAddScoped<IMapsterMapper, ServiceMapper>();
        services.TryAddScoped<IMapper, MapsterAdapter>();

        return services;
    }
}