﻿using Mapster;
using Quirco.Cqrs.Core.Mapping;

namespace Quirco.Cqrs.Mapster;

public class MapsterAdapter : IMapper
{
    private readonly MapsterMapper.IMapper _mapper;

    public MapsterAdapter(MapsterMapper.IMapper mapper)
    {
        _mapper = mapper;
    }

    public TDest Map<TDest>(object source) => _mapper.Map<TDest>(source);
    public TDest Map<TSrc, TDest>(TSrc source) => _mapper.Map<TSrc, TDest>(source);
    public TDest Map<TSrc, TDest>(TSrc source, TDest destination) => _mapper.Map(source, destination);
    public IQueryable<TDest> ProjectTo<TDest>(IQueryable sourceQuery) => sourceQuery.ProjectToType<TDest>();
    public IQueryable<TDest> ProjectTo<TDest>(IQueryable sourceQuery, object parameters) =>
        sourceQuery.ProjectToType<TDest>();
}