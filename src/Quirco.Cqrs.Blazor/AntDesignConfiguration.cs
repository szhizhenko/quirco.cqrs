namespace Quirco.Cqrs.Blazor;

/// <summary>
/// Глобальная конфигурация компонентов
/// </summary>
public static class AntDesignConfiguration
{
    /// <summary>
    /// Задержка для аггрегации введеных данных перед отправкой на сервер в мс.
    /// </summary>
    public static int InputDebounceMilliseconds = 750;

    /// <summary>
    /// Форматы ввода дат для DatePicker
    /// </summary>
    public static string[]? DatePickerFormats;
}