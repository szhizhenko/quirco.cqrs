﻿using System.Security.Claims;
using AntDesign;
using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.AspNetCore.Components.Forms;
using Microsoft.Extensions.Logging;
using Quirco.Cqrs.Blazor.Common;
using Quirco.Cqrs.Blazor.MediatR;
using Quirco.Cqrs.Blazor.Navigation;
using Quirco.Cqrs.Core.Mapping;
using Quirco.Cqrs.Domain.Commands;
using Quirco.Cqrs.Domain.Exceptions;
using Quirco.Cqrs.Domain.Queries;

namespace Quirco.Cqrs.Blazor.Edit;

public abstract class BaseEditComponent<TModel, TCreateUpdateModel> : ServiceScopedComponentBase
    where TModel : class
    where TCreateUpdateModel : class, new()
{
    protected bool IsLoading = true;
    protected TCreateUpdateModel? Model;
    protected TModel? ReadModel;
    protected bool GoBackAfterSave = true;

    [Parameter]
    public int Id { get; set; }

    [Inject]
    protected AuthenticationStateProvider AuthenticationStateProvider { get; set; }

    [Inject]
    public MessageService Messages { get; set; }

    [Inject]
    public IScopedMediator Mediator { get; set; }

    [Inject]
    public IMapper Mapper { get; set; }

    [Inject]
    public NavigationManager NavigationManager { get; set; }

    [Inject]
    public NavigationHistoryManager NavigationHistoryManager { get; set; }

    [Inject]
    public IAuthorizationService AuthorizationService { get; set; }

    [Inject]
    protected ILogger<BaseEditComponent<TModel, TCreateUpdateModel>> Logger { get; set; }

    protected ClaimsPrincipal User { get; private set; }

    public abstract string SuccessUrl { get; }

    public abstract string EntityName { get; }

    public abstract string? AddPermission { get; }

    public abstract string? EditPermission { get; }

    public abstract string OpenPermission { get; }

    public virtual string DeletePermission { get; }

    protected bool IsSaving { get; set; }

    public string? CurrentActionPermission => Id == 0
        ? AddPermission
        : EditPermission;

    protected bool HasErrors = false;

    protected override async Task OnInitializedAsync()
    {
        User = (await AuthenticationStateProvider.GetAuthenticationStateAsync()).User;
    }

    protected abstract GetEntityByIdQuery<TModel> BuildGetQuery(int id);

    protected abstract IRequest<int> BuildCreateUpdateCommand();

    /// <summary>
    /// Occurs when save button is pressed
    /// </summary>
    protected virtual async Task OnSave()
    {
        if (!await OnBeforeSave())
        {
            return;
        }

        try
        {
            IsSaving = true;

            if ((Id == 0 && AddPermission != null
                         && !(await AuthorizationService.AuthorizeAsync(User, AddPermission)).Succeeded)
                || (Id != 0 && EditPermission != null
                            && !(await AuthorizationService.AuthorizeAsync(User, EditPermission)).Succeeded))
            {
                throw new AccessDeniedException("Отказано в доступе.");
            }

            var command = BuildCreateUpdateCommand();
            var result = await Mediator.Send(command);
            Id = result;
            StateHasChanged();

            await OnAfterSave(result);

            Messages.Success($"'{EntityName}' успешно сохранен(а)");
        }
        catch (ValidationException ex)
        {
            var message = ex.Errors.FirstOrDefault()?.ErrorMessage ?? ex.Message;
            Messages.Warning("Предупреждение: " + message);
        }
        catch (Exception ex)
        {
            await OnSaveError(ex);
        }
        finally
        {
            IsSaving = false;
        }
    }

    /// <summary>
    /// Handler for exceptions during save. Default - shows message and throws
    /// </summary>
    /// <param name="ex">Exception</param>
    protected virtual Task OnSaveError(Exception ex)
    {
        Logger.LogError(ex, "Ошибка при сохранении");
        Messages.Error("Ошибка при сохранении: " + ex.Message);

        if (ex.InnerException != null)
            Messages.Error("Подробности: " + ex.InnerException.Message);

        throw ex;
    }

    protected virtual async Task OnAfterSave(int id)
    {
        if (GoBackAfterSave)
        {
            if (!string.IsNullOrEmpty(SuccessUrl))
                NavigationManager.NavigateTo(SuccessUrl);
            else if (await NavigationHistoryManager.CanGoBack())
                NavigationManager.NavigateTo((await NavigationHistoryManager.GetPreviousPage()).Url);
        }
    }

    protected virtual Task<bool> OnBeforeSave()
    {
        return Task.FromResult(true);
    }

    protected virtual async Task OnFinishFailed(EditContext context)
    {
        HasErrors = true;
        Messages.Error("Ошибка при сохранении");
    }

    protected virtual async Task OnDelete<TDModel>()
    {
        try
        {
            var command = new DeleteCommand<TDModel, int>(Id);
            await Mediator.Send(command);

            await NavigationHistoryManager.RemoveUrlFromHistory(NavigationManager.Uri);

            if (await NavigationHistoryManager.CanGoBack())
            {
                NavigationManager.NavigateTo((await NavigationHistoryManager.GetPreviousPageWithoutPopCurrent()).Url);
            }
            else if (!string.IsNullOrEmpty(SuccessUrl))
            {
                NavigationManager.NavigateTo(SuccessUrl);
            }

            Messages.Success($"'{EntityName}' успешно удален(а)");
        }
        catch (Exception ex)
        {
            Messages.Error(ex.Message);
        }
    }

    protected override async Task OnParametersSetAsync()
    {
        await base.OnParametersSetAsync();
        await Reload();
    }

    protected async Task Reload()
    {
        try
        {
            IsLoading = true;

            if (Id != 0)
            {
                ReadModel = await Mediator.Send(BuildGetQuery(Id));
                Model = Mapper.Map<TCreateUpdateModel>(ReadModel);
            }
            else
            {
                Model = CreateUpdateModelBuilder();
            }
        }
        catch (Exception ex)
        {
            Logger.LogError(ex, "Ошибка при загрузке карточки");
            Messages.Error(ex.Message);

            throw;
        }
        finally
        {
            IsLoading = false;
        }
    }

    protected virtual TCreateUpdateModel CreateUpdateModelBuilder()
    {
        return new TCreateUpdateModel();
    }
}