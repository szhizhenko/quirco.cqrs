using AntDesign;

namespace Quirco.Cqrs.Blazor.Components;

public class InputPasswordExtended : InputPassword
{
    public InputPasswordExtended()
    {
        DebounceMilliseconds = AntDesignConfiguration.InputDebounceMilliseconds;
    }
}