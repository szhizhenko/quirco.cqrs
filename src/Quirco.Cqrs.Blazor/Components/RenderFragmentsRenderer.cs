using System.Text;
using isdayoff.Contract;
using Microsoft.AspNetCore.Components;
using Quirco.Cqrs.Infrastructure.DayOff;

namespace Quirco.Cqrs.Blazor.Components;

public static class RenderFragmentsRenderer
{
    private const string DayOfColor = "#F008";

    private static RenderFragment CreateDayCell(DateTime date, string style, string classes) => builder =>
    {
        builder.OpenElement(1, "div");
        builder.AddAttribute(2, "class", $"ant-picker-cell-inner {classes}");
        builder.AddAttribute(3, "style", style);
        builder.AddContent(4, date.Day);
        builder.CloseElement();
    };

    public static Func<DateTime, DateTime, RenderFragment> GetDateFragment(IDayOffService? dayOffService, string? dayOffColor, Func<DateTime, DateTime, string>? dateRenderStyle)
        => (date, today) =>
        {
            var classes = new StringBuilder();
            var style = new StringBuilder("color: black; ");

            if (dayOffService is not null)
            {
                var isDayOff = dayOffService.CheckDay(date);

                if (isDayOff == DayType.NotWorkingDay)
                {
                    style.Append($"outline-width: 2px; outline-style: dotted; outline-color: {dayOffColor ?? DayOfColor}; ");
                    classes.Append("cell-day-off");
                }
            }

            if (dateRenderStyle is not null)
            {
                var overrideStyle = dateRenderStyle.Invoke(date, today);

                style.Append(' ');
                style.Append(overrideStyle);
            }

            return CreateDayCell(date, style.ToString(), classes.ToString());
        };
}