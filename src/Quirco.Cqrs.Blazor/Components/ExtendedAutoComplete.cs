﻿using AntDesign;

namespace Quirco.Cqrs.Blazor.Components;

public class ExtendedAutoComplete<TOption> : AutoComplete<TOption>
{
    public ExtendedAutoComplete()
    {
        DebounceMilliseconds = AntDesignConfiguration.InputDebounceMilliseconds;
    }
}