using System.Text;
using AntDesign;
using isdayoff;
using isdayoff.Contract;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Rendering;
using Microsoft.Extensions.DependencyInjection;
using Quirco.Cqrs.Infrastructure.DayOff;

namespace Quirco.Cqrs.Blazor.Components;

public class ExtendedDatePicker<TValue> : DatePicker<TValue>
{
    public IDayOffService? DayOffService { get; set; }

    [Inject] private IServiceProvider ServiceProvider { get; set; }

    [Parameter] public Func<DateTime, DateTime, string>? DateRenderStyle { get; set; }

    [Parameter] public bool DayOffEnabled { get; set; } = true;

    [Parameter] public string? DayOffColor { get; set; }

    protected override async Task OnInitializedAsync()
    {
        await base.OnInitializedAsync();

        if (DayOffEnabled)
        {
            DayOffService = ServiceProvider.GetService<IDayOffService>();

            DateRender = RenderFragmentsRenderer.GetDateFragment(DayOffService, DayOffColor, DateRenderStyle);
        }
    }

    protected override void BuildRenderTree(RenderTreeBuilder __builder)
    {
        __builder.OpenElement(1, "div");
        __builder.AddAttribute(1, "onclick", EventCallback.Factory.Create(this, DatePickerClick));

        base.BuildRenderTree(__builder);

        __builder.CloseElement();
    }

    public async Task DatePickerClick()
    {
        if (!_needRefresh)
        {
            await _inputStart.OnClick.InvokeAsync();
        }
    }
}