using AntDesign;

namespace Quirco.Cqrs.Blazor.Components;

public class InputExtended<TValue> : Input<TValue>
{
    public InputExtended()
    {
        DebounceMilliseconds = AntDesignConfiguration.InputDebounceMilliseconds;
    }
}