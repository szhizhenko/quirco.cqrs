using AntDesign;

namespace Quirco.Cqrs.Blazor.Components;

public class SearchExtended : Search
{
    public SearchExtended()
    {
        DebounceMilliseconds = AntDesignConfiguration.InputDebounceMilliseconds;
    }
}