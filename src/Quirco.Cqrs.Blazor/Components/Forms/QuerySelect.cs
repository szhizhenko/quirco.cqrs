using Quirco.Cqrs.Domain.Queries;

namespace Quirco.Cqrs.Blazor.Components.Forms;

public class QuerySelect<TItem, TItemValue, TQuery> : QuerySelectBase<TItem, int, TItemValue, TQuery>
    where TQuery : SearchQuery<TItem, int>, new()
{
}