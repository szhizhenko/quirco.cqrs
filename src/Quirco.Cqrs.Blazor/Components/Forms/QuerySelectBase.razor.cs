using AntDesign;
using AntDesign.Core.Helpers.MemberPath;
using AntDesign.Select.Internal;
using MediatR;
using Microsoft.AspNetCore.Components;
using Quirco.Cqrs.Domain.Queries;

namespace Quirco.Cqrs.Blazor.Components.Forms;

public partial class QuerySelectBase<TItem, TItemId, TItemValue, TQuery> : AntInputComponentBase<TItemValue>
    where TQuery : SearchQuery<TItem, TItemId>, new()
{
    private TQuery? _previousFilter;

    /// <summary>
    /// Template for select item.
    /// </summary>
    [Parameter]
    public RenderFragment<TItem> ItemTemplate { get; set; }

    /// <summary>
    /// Is used to customize the label style.
    /// </summary>
    [Parameter]
    public RenderFragment<TItem> LabelTemplate { get; set; }

    /// <summary>
    /// Limit search results.
    /// </summary>
    [Parameter]
    public int Limit { get; set; } = 20;

    /// <summary>
    /// Initial value for select control.
    /// </summary>
    [Parameter]
    public TItem? InitialValue { get; set; }

    /// <summary>
    /// Initial value for select control.
    /// </summary>
    [Parameter]
    public TItem[]? InitialValues { get; set; }

    /// <summary>
    /// Will match drowdown width
    /// </summary>
    [Parameter]
    public bool DropdownMatchSelectWidth { get; set; }

    /// <summary>
    /// The name of the property to be used for the value.
    /// </summary>
    [Parameter]
    public string ValueName { get; set; } = "Id";

    /// <summary>
    /// The name of the property to be used for the label.
    /// </summary>
    [Parameter]
    public string LabelName { get; set; } = "Name";

    /// <summary>
    /// Placeholder text.
    /// </summary>
    [Parameter]
    public string Placeholder { get; set; }

    /// <summary>
    /// Placeholder text.
    /// </summary>
    [Parameter]
    public SelectMode Mode { get; set; }

    /// <summary>
    /// Name in DOM-tree.
    /// </summary>
    [Parameter]
    public string DomName { get; set; }

    [Parameter]
    public bool Disabled { get; set; }

    [Parameter]
    public bool AllowClear { get; set; }

    [Parameter]
    public bool EnableSearch { get; set; } = true;

    [Parameter]
    public IEqualityComparer<TQuery>? FilterComparer { get; set; }

    /// <summary>
    /// Select the first value with pre-initialization
    /// </summary>
    [Parameter]
    public bool DefaultActiveFirstOption { get; set; }

    /// <summary>
    /// Loading indicator.
    /// </summary>
    protected bool IsLoading { get; set; }

    /// <summary>
    /// Modifier for search request.
    /// </summary>
    [Parameter]
    public Action<TQuery>? QueryModifier { get; set; }

    /// <summary>
    /// Called when the selected item changes.
    /// </summary>
    [Parameter]
    public Action<TItem>? OnSelectedItemChanged { get; set; }

    /// <summary>
    /// Called when the selected items changes.
    /// </summary>
    [Parameter]
    public Action<TItem[]>? OnSelectedItemsChanged { get; set; }

    [Inject]
    public IMediator Mediator { get; set; }

    protected TItem[] Items { get; set; } = Array.Empty<TItem>();

    public IReadOnlyCollection<TItem> InnerItems => Items;

    public bool IsMultiple => Mode is not SelectMode.Default;

    private object? _dependsOn;

    /// <summary>
    /// Ссылка на параметр, учавствующий в поиске.
    /// </summary>
    [Parameter]
    public object? DependsOn
    {
        get => _dependsOn;
        set
        {
            if (_dependsOn?.Equals(value) != true)
            {
                _dependsOn = value;

                if (_isInitialized)
                {
                    Items = Array.Empty<TItem>();
                    Value = default!;
                    _isItemsLoaded = false;
                    _depenseChanged = true;
                }
            }
            else if (_dependsOn.Equals(value))
            {
                return;
            }
            else
            {
                _dependsOn = value;
            }

            if (_dependsOn is not null)
            {
                EnsureInitialItemLoaded(false);
            }
        }
    }

    private bool _isDepended;
    private bool _depenseChanged;
    private bool _isInitialized;

    private TItemValue _value;

    /// <summary>
    /// Get or set the selected value.
    /// </summary>
    [Parameter]
    public override TItemValue Value
    {
        get => _value;
        set
        {
            if (_depenseChanged) return;

            var valueHasChanged = !EqualityComparer<TItemValue>.Default.Equals(value, _value);

            if (valueHasChanged)
            {
                _value = value;
                ValueChanged.InvokeAsync(value);
            }
        }
    }

    [Parameter]
    public EventCallback<TItemValue[]> ValuesChanged { get; set; }

    private TItemValue[] _values;

    /// <summary>
    /// Get or set the selected values.
    /// </summary>
    [Parameter]
    public TItemValue[] Values
    {
        get => _values;
        set
        {
            var valueHasChanged = !Equals(_values, value);

            if (valueHasChanged)
            {
                _values = value;
                ValuesChanged.InvokeAsync(value);
            }
        }
    }

    private bool _isItemsLoaded;
    private Select<TItemValue, TItem> _selectComponent;

    protected override async Task OnInitializedAsync()
    {
        await base.OnInitializedAsync();

        if (!Items.Any())
        {
            await EnsureInitialItemLoaded();
        }

        _isInitialized = true;
    }

    public override async Task SetParametersAsync(ParameterView parameters)
    {
        foreach (var parameter in parameters)
        {
            if (parameter.Name == nameof(DependsOn))
            {
                _isDepended = true;

                break;
            }
        }

        await base.SetParametersAsync(parameters);
    }

    private void OnSearch(string query)
    {
        if (!EnableSearch)
            return;

        ReloadItems(query);
    }

    /// <summary>
    /// Loads top items regarding query.
    /// </summary>
    /// <param name="query">Query text to search items for.</param>
    public async Task ReloadItems(string? query = null)
    {
        try
        {
            _depenseChanged = false;
            IsLoading = true;

            var request = new TQuery
            {
                Limit = EnableSearch
                    ? Limit
                    : null,
                Query = query
            };

            QueryModifier?.Invoke(request);

            if (typeof(TItemValue) == typeof(TItemId) && IsMultiple && Values?.Any() == true &&
                string.IsNullOrWhiteSpace(query))
            {
                request.AddSortByIds(Values.Cast<TItemId>().ToArray());
                request.Limit += Values.Length;
            }

            var response = await Mediator.Send(request);

            Items = response.Results;

            _isItemsLoaded = true;
        }
        finally
        {
            IsLoading = false;
            StateHasChanged();
        }
    }

    /// <summary>
    /// Loads only one initial selected item
    /// </summary>
    public async Task EnsureInitialItemLoaded(bool setInitialValue = true)
    {
        if (_isDepended && (DependsOn is null || DependsOn.Equals(default)))
        {
            return;
        }

        if (setInitialValue)
        {
            if (InitialValue is not null && !IsMultiple)
            {
                Items = new[] { InitialValue };

                return;
            }

            if (InitialValues is not null)
            {
                Items = InitialValues.ToArray();

                return;
            }
        }

        var withoutValue = IsMultiple
            ? Equals(Values, default)
            : Equals(Value, default(TItemValue));

        if (withoutValue && !DefaultActiveFirstOption)
        {
            return;
        }

        try
        {
            _depenseChanged = false;
            IsLoading = true;

            var request = new TQuery
            {
                Limit = 1
            };

            if (!withoutValue)
            {
                request.Ids = IsMultiple
                    ? Values!.Select(x => (TItemId)Convert.ChangeType(x, typeof(TItemId))!).ToArray()
                    : new[] { (TItemId)Convert.ChangeType(Value, typeof(TItemId))! };
                request.Limit = request.Ids.Length;
            }

            if (DefaultActiveFirstOption)
            {
                QueryModifier?.Invoke(request);
            }
            else
            {
                request.IgnoreQueryableModifiers();
            }

            var response = await Mediator.Send(request);

            Items = response.Results;

            if (Items.Any())
            {
                var getValue = PathHelper.GetDelegate<TItem, TItemValue>(ValueName);

                Value = getValue(Items.First());
            }
        }
        finally
        {
            IsLoading = false;

            StateHasChanged();
        }
    }

    private void OnFocus()
    {
        if (Disabled)
        {
            return;
        }

        var filter = new TQuery();
        QueryModifier?.Invoke(filter);

        var filterHasChanges = FilterComparer?.Equals(_previousFilter, filter) == false;

        if (_isItemsLoaded && !filterHasChanges)
            return;

        _previousFilter = filter;

#pragma warning disable S1481
        var _ = ReloadItems();
#pragma warning restore S1481
    }

    private void OnDropdownVisibleChange(bool isVisible)
    {
        if (Mode is SelectMode.Multiple && !isVisible)
        {
            OnSearch(null);
        }
    }
}