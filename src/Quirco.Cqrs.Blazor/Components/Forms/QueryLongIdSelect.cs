using Quirco.Cqrs.Domain.Queries;

namespace Quirco.Cqrs.Blazor.Components.Forms;

public class QueryLongIdSelect<TItem, TItemValue, TQuery> : QuerySelectBase<TItem, long, TItemValue, TQuery>
    where TQuery : SearchQuery<TItem, long>, new()
{
}