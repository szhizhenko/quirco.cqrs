using System.Text;
using AntDesign;
using isdayoff.Contract;
using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.DependencyInjection;
using Quirco.Cqrs.Infrastructure.DayOff;

namespace Quirco.Cqrs.Blazor.Components;

public partial class DateRangePicker
{
    public IDayOffService? DayOffService { get; set; }

    [Inject] private IServiceProvider ServiceProvider { get; set; }

    private DateTime? _endDate;
    private DateTime? _startDate;
    private DateTime?[] _value;

    [Parameter] public string[] Placeholder { get; set; } = { "С", "По" };

    [Parameter] public bool AllowClear { get; set; }

    [Parameter]
    public DateTime? StartDate
    {
        get { return _startDate; }
        set
        {
            if (_startDate == value)
            {
                return;
            }

            _startDate = value;

            _value = new[] { StartDate, EndDate };
        }
    }

    [Parameter]
    public Func<DateTime, DateTime, RenderFragment>? DateRender { get; set; }

    [Parameter] public EventCallback<DateTime?> StartDateChanged { get; set; }

    [Parameter]
    public DateTime? EndDate
    {
        get { return _endDate; }
        set
        {
            if (_endDate == value)
            {
                return;
            }

            _endDate = value;

            _value = new[] { StartDate, EndDate };
        }
    }

    [Parameter] public EventCallback<DateTime?> EndDateChanged { get; set; }

    private async Task ChangeValue(DateRangeChangedEventArgs arg)
    {
        await StartDateChanged.InvokeAsync(arg.Dates.First());
        await EndDateChanged.InvokeAsync(arg.Dates.Last());
    }

    [Parameter] public Func<DateTime, DateTime, string>? DateRenderStyle { get; set; }

    [Parameter] public bool DayOffEnabled { get; set; } = true;

    [Parameter] public string? DayOffColor { get; set; }

    protected override async Task OnInitializedAsync()
    {
        await base.OnInitializedAsync();

        if (DayOffEnabled)
        {
            DayOffService = ServiceProvider.GetService<IDayOffService>();
        }
    }
}