﻿using Blazored.SessionStorage;
using Microsoft.Extensions.DependencyInjection;
using Quirco.Cqrs.Blazor.Filters;
using Quirco.Cqrs.Blazor.MediatR;
using Quirco.Cqrs.Blazor.Navigation;
using Quirco.Cqrs.Mvc.StateProviders.Filter;

namespace Quirco.Cqrs.Blazor;

public static class Extensions
{
    public static IServiceCollection AddQuircoBlazor(this IServiceCollection serviceCollection)
    {
        serviceCollection.AddTransient<IFilterStateProvider, QueryStringStateProvider>();
        serviceCollection.AddScoped<NavigationHistoryManager>();
        serviceCollection.AddBlazoredSessionStorage();
        serviceCollection.AddScopedMediator();

        return serviceCollection;
    }
}