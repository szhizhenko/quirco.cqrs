using AntDesign;
using AntDesign.TableModels;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Authorization;
using Quirco.Cqrs.Blazor.MediatR;
using Quirco.Cqrs.Core.Extensions;
using Quirco.Cqrs.Core.Models;
using Quirco.Cqrs.DataAccess.Models;
using Quirco.Cqrs.Domain.Commands;
using Quirco.Cqrs.Domain.Queries;
using Quirco.Cqrs.Mvc.StateProviders.Filter;

namespace Quirco.Cqrs.Blazor.List;

public abstract partial class BaseListComponent<TModel, TEntity, TQuery>
    : BaseListComponent<TModel, TEntity, TQuery, int>
    where TEntity : class, IHasId<int>
    where TQuery : SearchQuery<TModel>, new()
{
}

public abstract partial class BaseListComponent<TModel, TEntity, TQuery, TId> : ComponentBase
    where TEntity : class, IHasId<TId>
    where TQuery : SearchQuery<TModel, TId>, new()
{
    [Inject]
    protected IScopedMediator Mediator { get; set; }

    [Inject]
    protected MessageService Messages { get; set; }

    [Inject]
    protected IFilterStateProvider FilterStateProvider { get; set; }

    [CascadingParameter]
    protected Task<AuthenticationState> AuthenticationStateTask { get; set; }

    [Inject]
    protected NavigationManager NavigationManager { get; set; }

    protected int PageSize { get; set; } = 10;

    protected int PageIndex { get; set; } = 1;

    protected int Total { get; set; }

    public PagedResponse<TModel>? Items { get; protected set; }

    public bool IsLoading { get; protected set; } = true;

    protected string? Query
    {
        get => Filter?.Query;
        set => Filter.Query = value;
    }

    protected TQuery Filter { get; private set; } = new();

    /// <summary>
    /// Reloads page (and resets page index)
    /// </summary>
    public virtual async Task Reload()
    {
        await ReloadInternal(true);
    }

    /// <summary>
    /// Refreshes page in current state (keeps filters, paging etc)
    /// </summary>
    public virtual async Task Refresh()
    {
        await ReloadInternal(false);
    }

    private async Task ReloadInternal(bool resetPaging)
    {
        try
        {
            IsLoading = true;

            if (resetPaging)
            {
                Filter.Offset = 0;
                Filter.Limit = PageSize;
                PageIndex = 1;
            }

            await FilterStateProvider.Persist(FilterPrefix, Filter);
            Items = await Mediator.Send(Filter);
            Total = (int)Items.Count;

            await OnAfterReload();
        }
        finally
        {
            IsLoading = false;
        }
    }

    /// <summary>
    /// Fires when list data is loaded.
    /// </summary>
    protected virtual Task OnAfterReload()
    {
        return Task.CompletedTask;
    }

    protected async Task DeleteItem(int itemId)
    {
        try
        {
            await Mediator.Send(new DeleteCommand<TEntity, int>(itemId));
            await ReloadInternal(false);
            await Messages.Success("Успешно удалено");
        }
        catch (FluentValidation.ValidationException e)
        {
            await Messages.Error(string.Join("\n", e.Errors));
        }
        catch (Exception e)
        {
            await Messages.Error("Ошибка при удалении: " + e.Message);
        }
    }

    protected override async Task OnParametersSetAsync()
    {
        try
        {
            IsLoading = true;
            await base.OnParametersSetAsync();
            Filter = await BuildDefaultQuery();

            if (!await FilterStateProvider.Restore(FilterPrefix, Filter))
            {
                await InitializeFilterWithDefaultParameters(Filter);
            }

            await OnAfterFilterRestoredAsync(Filter);
            StateHasChanged();

            if (Filter.Limit != null && Filter.Limit != 0)
            {
                PageIndex = (int)(Filter.Offset / Filter.Limit) + 1;
                PageSize = Filter.Limit ?? 10;
            }

            await ReloadInternal(false);
        }
        finally
        {
            IsLoading = false;
        }
    }

    /// <summary>
    /// Initializes filter with some defaults. This will be called on filter cleaning or when no parameters are passed from storage (query string),
    /// and filter should has some default state.
    /// </summary>
    /// <param name="filter">New instance of filter</param>
    protected virtual Task InitializeFilterWithDefaultParameters(TQuery filter) => Task.CompletedTask;

    /// <summary>
    /// Firest after filter is restored from filter storage (query etc). Should be used to set default unchangeable filter parameters.
    /// </summary>
    /// <param name="filter">Instance of filter in restored state</param>
    protected virtual Task OnAfterFilterRestoredAsync(TQuery filter) => Task.CompletedTask;

    /// <summary>
    /// Creates new query with base parametes.
    /// </summary>
    protected virtual Task<TQuery> BuildDefaultQuery() => Task.FromResult(
        new TQuery
        {
            Offset = (PageIndex - 1) * PageSize,
            Limit = PageSize
        });

    protected async Task PageChanged(PaginationEventArgs arg)
    {
        PageIndex = arg.Page;
        PageSize = arg.PageSize;

        Filter.Offset = (arg.Page - 1) * arg.PageSize;
        Filter.Limit = arg.PageSize;

        await FilterStateProvider.Persist(FilterPrefix, Filter);
        await ReloadInternal(false);
    }

    public async Task ClearFilter()
    {
        Filter = await BuildDefaultQuery();
        await InitializeFilterWithDefaultParameters(Filter);
        await OnAfterFilterRestoredAsync(Filter);
        await FilterStateProvider.Persist(FilterPrefix, Filter);
        await ReloadInternal(true);
    }

    private int _sortFieldsHashCode;

    /// <summary>
    /// Helping handler for tracking changes on table sorting or filtering
    /// </summary>
    protected virtual async Task OnTableChange(QueryModel<TModel> queryModel)
    {
        var sortableFields = queryModel.SortModel.Where(x => x.Sort != null).ToArray();

        var sortFieldsHashCode = sortableFields.Select(x => x.FieldName + x.Sort).GetOrderIndependentHashCode();

        if (_sortFieldsHashCode == sortFieldsHashCode)
        {
            return;
        }

        _sortFieldsHashCode = sortFieldsHashCode;

        Filter.ClearSort();

        foreach (var sortModel in sortableFields)
        {
            var isDescending = sortModel.Sort != "ascend";
            Filter.OrderBy(sortModel.FieldName, isDescending);
        }

        await Refresh();
    }

    /// <summary>
    /// Prefix used to persist filter state in storage. If there are many filters on same page, they should have different prefixes.
    /// </summary>
    public virtual string? FilterPrefix => null;

    protected async Task PageSizeChanged(int pageSize)
    {
        await PageChanged(new PaginationEventArgs(PageIndex,
                                                  pageSize));
    }
}