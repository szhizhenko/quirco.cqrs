﻿using System.Net;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Primitives;
using Quirco.Cqrs.Mvc.StateProviders.Filter;

namespace Quirco.Cqrs.Blazor.Filters;

public class QueryStringStateProvider : IFilterStateProvider
{
    private readonly NavigationManager _navigationManager;

    public QueryStringStateProvider(NavigationManager navigationManager)
    {
        _navigationManager = navigationManager;
    }

    public Task Persist<TState>(string prefix, TState state)
    {
        var uri = _navigationManager.ToAbsoluteUri(_navigationManager.Uri);
        var query = QueryHelpers.ParseQuery(uri.Query);
        var parameters = ObjectKvpJsonPersister.Persist(prefix, state).Select(kv => new KeyValuePair<string, StringValues>(kv.Key, WebUtility.UrlEncode(kv.Value)));
        foreach (var parameter in parameters)
        {
            query[parameter.Key] = parameter.Value;
        }

        var url = QueryHelpers.AddQueryString(uri.GetLeftPart(UriPartial.Path), query);
        if (!string.Equals(uri.ToString(), url))
        {
            _navigationManager.NavigateTo(url, false, true);
        }

        return Task.CompletedTask;
    }

    public Task<bool> Restore<TState>(string prefix, TState state)
    {
        var uri = _navigationManager.ToAbsoluteUri(_navigationManager.Uri);
        var query = QueryHelpers.ParseQuery(uri.Query);
        var res = ObjectKvpJsonPersister.Restore(prefix, state, query.Select(kv => new KeyValuePair<string, string?>(kv.Key, WebUtility.UrlDecode(kv.Value.ToString()))));
        return Task.FromResult(res);
    }
}