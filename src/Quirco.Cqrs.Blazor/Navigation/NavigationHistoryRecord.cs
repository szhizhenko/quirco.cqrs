namespace Quirco.Cqrs.Blazor.Navigation;

/// <summary>
/// Запись о навигации по приложению
/// </summary>
public record NavigationHistoryRecord
{
    /// <summary>
    /// Название страницы по маршруту
    /// </summary>
    public string PageName { get; }

    /// <summary>
    /// Адрес страницы
    /// </summary>
    public string Url { get; }

    public NavigationHistoryRecord(string pageName, string url)
    {
        PageName = pageName;
        Url = url;
    }
}