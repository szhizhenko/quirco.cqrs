﻿using Blazored.SessionStorage;

namespace Quirco.Cqrs.Blazor.Navigation;

/// <summary>
/// Отвечает за стек навигации в приложении, позволяя отслеживать историю навигации, реализует возможность навигации "назад".
/// </summary>
public class NavigationHistoryManager
{
    private const int StackSize = 10;

    private readonly ISessionStorageService _sessionStorage;
    private Stack<NavigationHistoryRecord>? _navStack;

    public NavigationHistoryManager(ISessionStorageService sessionStorage)
    {
        _sessionStorage = sessionStorage;
    }

    private async Task<Stack<NavigationHistoryRecord>> GetNavStack()
    {
        var navHistory = await _sessionStorage.GetItemAsync<NavigationHistoryRecord[]>("nav-history");

        _navStack = new Stack<NavigationHistoryRecord>(navHistory?.Reverse() ?? Array.Empty<NavigationHistoryRecord>());

        return _navStack;
    }

    /// <summary>
    /// Добавляет страницу в стек навигации
    /// </summary>
    /// <param name="page"></param>
    public async Task AddPageToHistory(NavigationHistoryRecord page)
    {
        if (string.IsNullOrWhiteSpace(page.PageName))
        {
            return;
        }

        var navigationHistory = await GetNavStack();

        if (navigationHistory.TryPeek(out var current) &&
            (current.PageName == page.PageName || current.Url == page.Url))
        {
            navigationHistory.Pop();
        }

        await SavePage(page, navigationHistory);
    }

    public async Task RemoveUrlFromHistory(string url)
    {
        var navigationHistory = await GetNavStack();

        var newNavStack = navigationHistory.Where(x => x.Url != url);

        await SaveState(newNavStack);
    }

    /// <summary>
    /// Указывает, возможна ли навигация "назад"
    /// </summary>
    /// <returns></returns>
    public async Task<bool> CanGoBack() => (await GetNavStack()).Count > 1;

    /// <summary>
    /// Возвращает параметры страницы для навигации назад
    /// </summary>
    /// <returns></returns>
    public async Task<NavigationHistoryRecord> GetPreviousPage()
    {
        var navigationHistory = await GetNavStack();

        navigationHistory.Pop();
        var prevPage = navigationHistory.Pop();

        await SaveState(navigationHistory);

        return prevPage;
    }

    /// <summary>
    /// Возвращает параметры страницы для навигации назад
    /// </summary>
    /// <returns></returns>
    public async Task<NavigationHistoryRecord> GetPreviousPageWithoutPopCurrent()
    {
        var navigationHistory = await GetNavStack();

        var prevPage = navigationHistory.Pop();

        await SaveState(navigationHistory);

        return prevPage;
    }

    private async Task SavePage(NavigationHistoryRecord page, Stack<NavigationHistoryRecord> navigationHistory)
    {
        navigationHistory.Push(page);

        if (navigationHistory.Count > StackSize)
        {
            await SaveState(navigationHistory.SkipLast(1));
        }
        else
        {
            await SaveState(navigationHistory);
        }
    }

    private async Task SaveState(IEnumerable<NavigationHistoryRecord> history)
    {
        await _sessionStorage.SetItemAsync("nav-history", history);
    }
}