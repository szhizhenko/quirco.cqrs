﻿using System.Diagnostics;

namespace Quirco.Cqrs.Core.Models;

[DebuggerDisplay("{Count} result(s)")]
public class PagedResponse<TModel>
{
    public TModel[] Results { get; }

    public long Count { get; }

    public PagedResponse(TModel[] results, long count)
    {
        Results = results;
        Count = count;
    }
}