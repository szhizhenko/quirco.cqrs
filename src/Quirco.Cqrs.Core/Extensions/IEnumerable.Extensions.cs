namespace Quirco.Cqrs.Core.Extensions;

public static class EnumerableExtensions
{
    /// <summary>
    /// Calculate order independent hash code via EqualityComparer.Default
    /// </summary>
    /// <param name="source">Enumerable of T</param>
    /// <typeparam name="T">Type for hashCode</typeparam>
    /// <returns>HashCode</returns>
    public static int GetOrderIndependentHashCode<T>(this IEnumerable<T> source)
        where T : notnull
    {
        var hash = 0;

        foreach (var element in source)
        {
            hash ^= EqualityComparer<T>.Default.GetHashCode(element);
        }

        return hash;
    }
}