﻿using Quirco.Cqrs.Core.Mapping;

namespace Quirco.Cqrs.Core.Extensions;

public static class MapperExtensions
{
    /// <summary>
    /// Extension method to project from a queryable using the provided mapping engine
    /// </summary>
    /// <remarks>Projections are only calculated once and cached</remarks>
    /// <typeparam name="TDestination">Destination type</typeparam>
    /// <param name="source">Queryable source</param>
    /// <param name="mapper">Mapper</param>
    /// <returns>Expression to project into</returns>
    public static IQueryable<TDestination> ProjectTo<TDestination>(this IQueryable source, IMapper mapper) =>
        mapper.ProjectTo<TDestination>(source);

    /// <summary>
    /// Extension method to project from a queryable using the provided mapping engine
    /// </summary>
    /// <remarks>Projections are only calculated once and cached</remarks>
    /// <typeparam name="TDestination">Destination type</typeparam>
    /// <param name="source">Queryable source</param>
    /// <param name="mapper">Mapper</param>
    /// <param name="parameters">Optional parameter object for parameterized mapping expressions</param>
    /// <returns>Expression to project into</returns>
    public static IQueryable<TDestination> ProjectTo<TDestination>(this IQueryable source, IMapper mapper, object parameters) =>
        mapper.ProjectTo<TDestination>(source, parameters);
}