using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Quirco.Cqrs.Infrastructure.Jobs;
using Quirco.Cqrs.Tests.Jobs;

namespace Quirco.Cqrs.Tests.Startups;

public static class InMemoryTestStartup
{
    /// <summary>
    /// Configures service collection for test environment. Adds in-memory context provider, test jobs service etc.
    /// </summary>
    /// <param name="services">Services collection</param>
    /// <typeparam name="TContext">Type of DB context to be configured with in-memory provider</typeparam>
    public static IServiceCollection AddTestServices<TContext>(this IServiceCollection services)
        where TContext : DbContext
    {
        var descriptor = services.SingleOrDefault(
            d => d.ServiceType ==
                 typeof(DbContextOptions<TContext>));

        if (descriptor != null)
        {
            services.Remove(descriptor);
        }

        services.AddEntityFrameworkInMemoryDatabase();

        // Add ApplicationDbContext using an in-memory database for testing.
        services.AddDbContextFactory<TContext>(
            options =>
            {
                options.UseInMemoryDatabase(Guid.NewGuid().ToString());
                options.EnableDetailedErrors();
                options.EnableSensitiveDataLogging();
            },
            ServiceLifetime.Scoped);
        services.AddScoped(p => p.GetRequiredService<IDbContextFactory<TContext>>().CreateDbContext());

        services.AddScoped<IJobsService, TestJobsService>();

        return services;
    }
}