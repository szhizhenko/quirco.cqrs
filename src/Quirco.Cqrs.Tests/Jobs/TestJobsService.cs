﻿using System.Linq.Expressions;
using Microsoft.Extensions.DependencyInjection;
using Quirco.Cqrs.Infrastructure.Jobs;

namespace Quirco.Cqrs.Tests.Jobs;

/// <summary>
/// Synchronous jobs implementation for tests
/// </summary>
public class TestJobsService : IJobsService
{
    private readonly IServiceProvider _services;

    public TestJobsService(IServiceProvider services)
    {
        _services = services;
    }

    public async Task<string> Enqueue<T>(Expression<Func<T, Task>> methodCall)
    {
        await methodCall.Compile()(_services.GetRequiredService<T>());
        return "new-job";
    }

    public async Task<string> Enqueue<T>(Expression<Func<T, Task>> methodCall, TimeSpan delay)
    {
        await methodCall.Compile()(_services.GetRequiredService<T>());
        return "new-job";
    }

    public Task Schedule<T>(string id, Expression<Func<T, Task>> methodCall, string cronExpression)
    {
        return Task.CompletedTask;
    }

    public Task RemoveIfExists(string recurringJobId)
    {
        return Task.CompletedTask;
    }
}