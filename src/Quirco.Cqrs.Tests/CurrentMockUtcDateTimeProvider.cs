using Quirco.Cqrs.Infrastructure.DateTime;

namespace Quirco.Cqrs.Tests;

public class CurrentMockUtcDateTimeProvider : ICurrentDateTimeProvider
{
    public DateTime Now { get; set; }

    public CurrentMockUtcDateTimeProvider()
    {
        Now = DateTime.UtcNow;
    }
}