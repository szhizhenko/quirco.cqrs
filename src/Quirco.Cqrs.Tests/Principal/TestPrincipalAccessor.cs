﻿using System.Security.Principal;
using Quirco.Security.Principal;

namespace Quirco.Cqrs.Tests.Principal;

/// <summary>
/// Fixed principal accessor used in tests
/// </summary>
public class TestPrincipalAccessor : IPrincipalAccessor
{
    private IPrincipal _testPrincipal;

    public TestPrincipalAccessor(IPrincipal testPrincipal)
    {
        _testPrincipal = testPrincipal;
    }

    /// <summary>
    /// Set new current principal
    /// </summary>
    public void SetPrincipal(IPrincipal principal)
    {
        _testPrincipal = principal;
    }

    public Task<IPrincipal> GetPrincipal()
    {
        return Task.FromResult(_testPrincipal);
    }
}