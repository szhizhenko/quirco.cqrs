using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Quirco.Cqrs.Tests;

public abstract class BaseIntegrationTest<TStartup, TContext> : IDisposable
    where TStartup : class
    where TContext : DbContext
{
    protected readonly IWebHost WebHost;

    protected BaseIntegrationTest()
    {
        WebHost = new WebHostBuilder()
            .UseEnvironment("Test")
            .ConfigureAppConfiguration((_, e) =>
            {
                e.AddJsonFile("appsettings.json", optional: true)
                    .AddEnvironmentVariables();
            })
            .ConfigureServices(ConvifureServices)
            .UseStartup<TStartup>()
            .Build();
#pragma warning disable S1481
        var _ = Context;
#pragma warning restore S1481
    }

    protected virtual void ConvifureServices(IServiceCollection services)
    {
    }

    private TContext? _context;
    protected TContext Context => _context ??= BuildContext();

    private TContext BuildContext()
    {
        var context = WebHost.Services.GetRequiredService<TContext>();
        InitializeDbContext(context);
        context.SaveChanges();
        return context;
    }

    private IDbContextFactory<TContext>? _contextFactory;
    protected IDbContextFactory<TContext> ContextFactory => _contextFactory ??= BuildContextFactory();

    private IDbContextFactory<TContext> BuildContextFactory()
    {
        Context.SaveChanges();
        return WebHost.Services.GetRequiredService<IDbContextFactory<TContext>>();
    }

    public TService GetService<TService>()
    {
        return WebHost.Services.GetRequiredService<TService>();
    }

    protected abstract void InitializeDbContext(TContext context);

    public void Dispose()
    {
        Context?.Dispose();
        WebHost?.Dispose();
        GC.SuppressFinalize(this);
    }
}