﻿using Quirco.Cqrs.DataAccess.Models;

namespace Quirco.Cqrs.Domain.Extensions;

public static class CollectionExtensions
{
    /// <summary>
    /// Synchronize two collections and make target containing the same elements as source.
    /// Extra elements in target collection will be deleted. Missing elements will be inserted. Existing will be updated via mapper method.
    /// </summary>
    /// <param name="source">Source collection</param>
    /// <param name="target">Target collection</param>
    /// <param name="mapper">Function to map source item to target</param>
    /// <param name="deleter">Optional action, will be called for each deleted item in target collection.</param>
    /// <typeparam name="TSource"></typeparam>
    /// <typeparam name="TTarget"></typeparam>
    public static (int, int, int) Sync<TSource, TTarget>(
        this ICollection<TSource>? source,
        IList<TTarget> target,
        Func<TSource, TTarget, TTarget> mapper,
        Action<TTarget>? deleter = null)
        where TTarget : class, IHasId, new()
        where TSource : IHasId
    {
        var (inserted, updated, deleted) = (0, 0, 0);

        if (source != null)
        {
            target.SortDescending();

            var index = 0;
            foreach (var src in source.OrderByDescending(x => x.Id)) // Update existing items or insert new
            {
                if (index >= target.Count)
                {
                    var created = mapper(src, new TTarget());
                    created.Id = 0;
                    target.Add(created);
                    inserted++;
                }

                else
                {
                    var id = target[index].Id;
                    target[index] = mapper(src, target[index]);
                    target[index].Id = id;
                    updated++;
                }

                index++;
            }

            var targetCount = target.Count;
            for (var i = index; i < targetCount; i++) // Remove tail
            {
                deleter?.Invoke(target[i]);
                target.RemoveAt(index);
                deleted++;
            }
        }

        return (inserted, updated, deleted);
    }

    /// <summary>
    /// Synchronize two collections and make target containing the same elements (by Id) as source in the original order
    /// </summary>
    /// <param name="source">Source collection</param>
    /// <param name="target">Target collection</param>
    /// <param name="mapExisting">Function to map source item to target</param>
    /// <param name="deleter">Optional action, will be called for each deleted item in target collection.</param>
    /// <typeparam name="TSource"></typeparam>
    /// <typeparam name="TTarget"></typeparam>
    public static (int, int, int) SyncByCompare<TSource, TTarget>(
        this ICollection<TSource>? source,
        IList<TTarget> target,
        Func<TSource, TTarget, TTarget> mapExisting,
        Action<TTarget>? deleter = null)
        where TTarget : IHasId, new()
        where TSource : IHasId
    {
        return source.SyncByCompare(target, mapExisting, (s, t) => s.Id != 0 && s.Id == t.Id, deleter);
    }

    /// <summary>
    /// Synchronize two collections and make target containing the same elements as source in the original order
    /// It requires to specify comparer for "same" elements criteria in collections.
    /// </summary>
    /// <param name="source">Source collection</param>
    /// <param name="target">Target collection</param>
    /// <param name="mapper">Function to map source item to target</param>
    /// <param name="deleter">Optional action, will be called for each deleted item in target collection.</param>
    /// <param name="lookup">Optional comparer for searching same elements in collections.</param>
    /// <param name="createNewItems">If no item in target collection found, new one will be created.</param>
    public static (int, int, int) SyncByCompare<TSource, TTarget>(
        this ICollection<TSource>? source,
        ICollection<TTarget> target,
        Func<TSource, TTarget, TTarget> mapper,
        Func<TSource, TTarget, bool> lookup,
        Action<TTarget>? deleter = null,
        bool createNewItems = true)
        where TTarget : new()
    {
        var (inserted, updated, deleted) = (0, 0, 0);

        if (source != null)
        {
            foreach (var trg in target.ToList())
            {
                if (!source.Any(s => lookup(s, trg)))
                {
                    deleter?.Invoke(trg);
                    target.Remove(trg);
                    deleted++;
                }
            }

            foreach (var src in source)
            {
                var trg = target.FirstOrDefault(t => lookup(src, t));

                if (trg == null)
                {
                    var created = mapper(src, new TTarget());
                    if (createNewItems && created is IHasId hasId)
                    {
                        hasId.Id = 0;
                    }

                    target.Add(created);
                    inserted++;
                }
                else
                {
                    if (trg is IHasId hasId)
                    {
                        var id = hasId.Id;
                        mapper(src, trg);
                        hasId.Id = id;
                    }
                    else
                    {
                        mapper(src, trg);
                    }

                    updated++;
                }
            }
        }

        return (inserted, updated, deleted);
    }

    private static void SortDescending<T>(this IList<T> source)
        where T : IHasId
    {
        bool itemMoved;
        do
        {
            itemMoved = false;
            for (int i = 0; i < source.Count - 1; i++)
            {
                if (source[i].Id < source[i + 1].Id)
                {
                    (source[i + 1], source[i]) = (source[i], source[i + 1]);
                    itemMoved = true;
                }
            }
        } while (itemMoved);
    }
}