﻿using Quirco.Cqrs.Domain.Filters;
using Quirco.Cqrs.Domain.Models;

namespace Quirco.Cqrs.Domain.Extensions;

public static class QueryableExtensions
{
    public static async Task<IQueryable<TEntity>> ApplyModifiers<TEntity>(
        this IQueryable<TEntity> query,
        IEnumerable<IQueryableModifier<TEntity>>? modifiers,
        CancellationToken cancellationToken)
        where TEntity : class
    {
        if (modifiers == null) return query;

        foreach (var modifier in modifiers)
            query = await modifier.Modify(query, cancellationToken);

        return query;
    }

    public static async Task<IQueryable<TEntity>> ApplyModifiers<TEntity, TId, TModel, TRequest>(
        this IQueryable<TEntity> query,
        IEnumerable<IQueryableModifier<TEntity>>? modifiers,
        TRequest? request,
        CancellationToken cancellationToken)
        where TEntity : class
        where TRequest : ListQuery<TModel, TId?>
    {
        if (modifiers == null || request?.IgnoreAllQueryableModifiers == true) return query;

        var availModifiers = modifiers.Where(m => request?.IgnoreQueryableModifiers == null ||
                                                  !request.IgnoreQueryableModifiers.Contains(m.GetType()));

        foreach (var modifier in availModifiers)
            query = await modifier.Modify(query, cancellationToken);

        return query;
    }
}