using MediatR;
using Microsoft.EntityFrameworkCore;
using Quirco.Cqrs.DataAccess.Models;
using Quirco.Cqrs.Domain.Exceptions;
using Quirco.Cqrs.Domain.Security;
using Quirco.Cqrs.Infrastructure.Security;
using Quirco.Security.Principal;

namespace Quirco.Cqrs.Domain.Commands;

/// <inheritdoc />
public abstract class DeleteCommandHandler<TContext, TEntity> : DeleteCommandHandler<TContext, TEntity, int>
    where TContext : DbContext
    where TEntity : class, IHasId<int>, new()
{
    protected DeleteCommandHandler(
        TContext context,
        IEnumerable<IPermissionEvaluator<TEntity>>? evaluators,
        IPrincipalAccessor principal)
        : base(context, evaluators, principal)
    {
    }
}

/// <inheritdoc />
public abstract class DeleteCommandHandler<TContext, TEntity, TId> : DeleteCommandHandler<DeleteCommand<TEntity, TId>,
    TContext, TEntity, TId>
    where TEntity : class, IHasId<TId>, new()
    where TContext : DbContext
    where TId : struct
{
    protected DeleteCommandHandler(
        TContext context,
        IEnumerable<IPermissionEvaluator<TEntity>>? evaluators,
        IPrincipalAccessor? principal = null)
        : base(context, evaluators, principal)
    {
    }
}

/// <summary>
/// Удаляет запись без её загрузки из базы данных, если не заданы валидаторы (evaluators)
/// </summary>
public abstract class DeleteCommandHandler<TCommand, TContext, TEntity, TId> : IRequestHandler<TCommand>
    where TCommand : DeleteCommand<TEntity, TId>, IRequest
    where TEntity : class, IHasId<TId>, new()
    where TContext : DbContext
    where TId : struct
{
    protected TContext Context { get; }

    protected readonly IEnumerable<IPermissionEvaluator<TEntity>>? Evaluators;
    protected readonly IPrincipalAccessor? PrincipalAccessor;

    /// <summary>
    /// Инициализирует новый экземпляр класса <see cref="DeleteCommandHandler{TCommand, TContext, TEntity, TId}"/>.
    /// </summary>
    protected DeleteCommandHandler(
        TContext context,
        IEnumerable<IPermissionEvaluator<TEntity>>? evaluators,
        IPrincipalAccessor? principal = null)
    {
        Context = context;
        Evaluators = evaluators;
        PrincipalAccessor = principal;
    }

    public virtual async Task Handle(TCommand request, CancellationToken cancellationToken)
    {
        TEntity[] entities;

        if (request.Ids.Length == 1)
        {
            var id = request.Ids[0];

            entities = new[]
            {
                await Context
                    .Set<TEntity>()
                    .IgnoreQueryFilters()
                    .FirstOrDefaultAsync(e => e.Id.Equals(id), cancellationToken) ??
                throw new ObjectNotFoundException<TId>(typeof(TEntity), id)
            };
        }
        else
        {
            entities = await Context
                .Set<TEntity>()
                .IgnoreQueryFilters()
                .Where(e => request.Ids.Contains(e.Id))
                .ToArrayAsync(cancellationToken);

            if (entities.Length != request.Ids.Length)
            {
                throw new ObjectNotFoundException<TId>(
                    typeof(TEntity),
                    $"Object with one of ids not found: " +
                    string.Join(",", request.Ids));
            }
        }

        foreach (var entity in entities)
        {
            await ValidateAccessAsync(entity, cancellationToken);

            if (entity is IHasDateDeleted hasDateDeleted)
            {
                hasDateDeleted.DateDeleted ??= DateTime.UtcNow;

                if (hasDateDeleted is IHasUserDeleted hasUserDeleted)
                {
                    if (PrincipalAccessor == null)
                    {
                        throw new InvalidOperationException(
                            "Can't process deleted user because no PrincipalAccessor instance specified");
                    }

                    var principal = await PrincipalAccessor.GetPrincipal();
                    hasUserDeleted.UserDeletedId = principal.GetId();
                }
            }
            else
            {
                Context.Entry(entity).State = EntityState.Deleted;
            }
        }

        await Context.SaveChangesAsync(cancellationToken);
    }

    private async Task ValidateAccessAsync(TEntity entity, CancellationToken cancellationToken)
    {
        if (Evaluators != null)
        {
            foreach (var evaluator in Evaluators)
            {
                cancellationToken.ThrowIfCancellationRequested();

                if (!await evaluator.CanDelete(entity, cancellationToken))
                {
                    throw new AccessDeniedException();
                }
            }
        }
    }
}