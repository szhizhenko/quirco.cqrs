namespace Quirco.Cqrs.Domain.Commands;

public abstract class CreateUpdateEntityCommand<TModel, TEntity> : CreateUpdateEntityCommand<int, TModel, TEntity>
{
    protected CreateUpdateEntityCommand() : base() // Required for proper deserialization
    {
    }

    public CreateUpdateEntityCommand(TModel model) : base(model)
    {
    }

    public CreateUpdateEntityCommand(int? id, TModel model) : base(id, model)
    {
    }
}

/// <summary>
/// Command to create or update entity from model TModel
/// </summary>
/// <typeparam name="TId">Type of ID</typeparam>
/// <typeparam name="TModel">Model type</typeparam>
/// <typeparam name="TEntity">Entity type</typeparam>
public abstract class CreateUpdateEntityCommand<TId, TModel, TEntity> : CreateUpdateCommand<TId, TModel>, IAffectEntityCommand<TId?>
    where TId : struct
{
    protected CreateUpdateEntityCommand()
    {
    }

    protected CreateUpdateEntityCommand(TModel model) : base(model)
    {
    }

    protected CreateUpdateEntityCommand(TId? id, TModel model) : base(id, model)
    {
    }

    public Type EntityType => typeof(TEntity);
}