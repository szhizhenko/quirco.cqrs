﻿using MediatR;

namespace Quirco.Cqrs.Domain.Commands;

/// <summary>
/// <inheritdoc />
/// </summary>
public abstract class DeleteCommand : DeleteCommand<int>
{
    protected DeleteCommand() : base() // Required for proper deserialization
    {
    }

    public DeleteCommand(int id) : base(id)
    {
    }
}

/// <summary>
/// Запрос на удаление сущности.
/// </summary>
public abstract class DeleteCommand<TId> : IRequest
    where TId : struct
{
    public TId[] Ids { get; set; }

    protected DeleteCommand() // Required for proper deserialization
    {
    }

    /// <summary>
    /// Инициализирует новый экземпляр класса
    /// </summary>
    /// <param name="id">Идентификатор сущностей к удалению</param>
    public DeleteCommand(TId id)
    {
        Ids = new[] { id };
    }

    /// <summary>
    /// Инициализирует новый экземпляр класса
    /// </summary>
    /// <param name="ids">Список идентификаторов сущностей к удалению</param>
    public DeleteCommand(TId[] ids)
    {
        Ids = ids;
    }
}

/// <summary>
/// Запрос на удаление сущности.
/// </summary>
/// <typeparam name="TId">Тип идентификатора сещности</typeparam>
/// <typeparam name="TEntity">Тип сущности</typeparam>
public class DeleteCommand<TEntity, TId> : IRequest
    where TId : struct
{
    public TId[] Ids { get; set; }

    protected DeleteCommand() // Required for proper deserialization
    {
    }

    /// <summary>
    /// Инициализирует новый экземпляр класса
    /// </summary>
    /// <param name="id">Идентификатор сущностей к удалению</param>
    public DeleteCommand(TId id)
    {
        Ids = new[] { id };
    }

    /// <summary>
    /// Инициализирует новый экземпляр класса
    /// </summary>
    /// <param name="ids">Список идентификаторов сущностей к удалению</param>
    public DeleteCommand(TId[] ids)
    {
        Ids = ids;
    }
}