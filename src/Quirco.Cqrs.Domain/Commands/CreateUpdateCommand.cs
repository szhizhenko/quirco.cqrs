using MediatR;
using Quirco.Cqrs.DataAccess.Models;

namespace Quirco.Cqrs.Domain.Commands;

public abstract class CreateUpdateCommand<TModel> : CreateUpdateCommand<int, TModel>
{
    protected CreateUpdateCommand() : base() // Required for proper deserialization
    {
    }

    public CreateUpdateCommand(TModel model) : base(model)
    {
    }

    public CreateUpdateCommand(int? id, TModel model) : base(id, model)
    {
    }
}

/// <summary>
/// Command to create or update entity from model TModel
/// </summary>
public abstract class CreateUpdateCommand<TId, TModel> : IRequest<TId>
    where TId : struct
{
    public TId? Id { get; }

    public TModel Model { get; }

    protected CreateUpdateCommand() // Required for proper deserialization
    {
    }

    protected CreateUpdateCommand(TModel model) : this(null, model)
    {
    }

    protected CreateUpdateCommand(TId? id, TModel model)
    {
        Id = Equals(id, default(TId)) ? null : id;
        Model = model;

        if (Id != null && model is IHasId<TId> modelWithId && !modelWithId.Id.Equals(id))
            modelWithId.Id = Id.Value;
    }
}