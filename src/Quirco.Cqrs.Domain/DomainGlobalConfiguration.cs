using Quirco.Cqrs.Domain.Queries;

namespace Quirco.Cqrs.Domain;

/// <summary>
/// Config for domain
/// </summary>
public static class DomainGlobalConfiguration
{
    /// <summary>
    /// Config for <see cref="GetEntityByIdQuery{T}"/>
    /// </summary>
    public static class GetEntityById
    {
        /// <summary>
        /// Ignore query filters for all requests by default
        /// </summary>
        public static bool IgnoreQueryFilters = true;
    }
}