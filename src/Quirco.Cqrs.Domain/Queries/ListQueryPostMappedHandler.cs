using System.Linq.Expressions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Quirco.Cqrs.Core.Mapping;
using Quirco.Cqrs.Core.Models;
using Quirco.Cqrs.DataAccess.Models;
using Quirco.Cqrs.Domain.DataContextExtensions;
using Quirco.Cqrs.Domain.Extensions;
using Quirco.Cqrs.Domain.Filters;
using Quirco.Cqrs.Domain.Models;

namespace Quirco.Cqrs.Domain.Queries;

public abstract class ListQueryPostMappedHandler<TModel, TEntity, TContext, TListQuery>
    : ListQueryPostMappedHandler<TModel, TEntity, TContext, TListQuery, int>
    where TEntity : class, IHasId
    where TContext : DbContext
    where TListQuery : ListQuery<TModel, int>
{
    protected ListQueryPostMappedHandler(
        IMapper mapper,
        IDbContextFactory<TContext> context,
        IEnumerable<IQueryableModifier<TEntity>>? modifiers)
        : base(mapper, context, modifiers)
    {
    }
}

/// <summary>
/// Обработчик запроса на поиск элементов. Отличается от <see cref="ListQueryHandler{TModel,TEntity,TContext,TListQuery}"/> особым способом
/// материализации моделей - из хранилища сначала материализуются сущности, затем происходит маппинг в модели.
/// </summary>
/// <typeparam name="TModel">Тип модели.</typeparam>
/// <typeparam name="TEntity">Тип сущности в БД.</typeparam>
/// <typeparam name="TContext">Тип контекста БД.</typeparam>
/// <typeparam name="TListQuery">Тип поискового запроса.</typeparam>
public abstract class ListQueryPostMappedHandler<TModel, TEntity, TContext, TListQuery, TId>
    : IRequestHandler<TListQuery, PagedResponse<TModel>>
    where TEntity : class, IHasId<TId>
    where TContext : DbContext
    where TListQuery : ListQuery<TModel, TId>
{
    /// <summary>
    /// Initializes a new instance of the <see cref="ListQueryHandler{TModel, TEntity, TContext, TListQuery}"/>.
    /// </summary>
    protected ListQueryPostMappedHandler(
        IMapper mapper,
        IDbContextFactory<TContext> contextFactory,
        IEnumerable<IQueryableModifier<TEntity>>? modifiers)
    {
        Mapper = mapper;
        ContextFactory = contextFactory;
        Modifiers = modifiers;
    }

    /// <summary>
    /// Маппер сущностей.
    /// </summary>
    protected IMapper Mapper { get; }

    /// <summary>
    /// Контекст работы с БД.
    /// </summary>
    protected IDbContextFactory<TContext> ContextFactory { get; }

    /// <summary>
    /// Модификаторы запроса.
    /// </summary>
    protected IEnumerable<IQueryableModifier<TEntity>>? Modifiers { get; }

    /// <inheritdoc />
    public virtual async Task<PagedResponse<TModel>> Handle(TListQuery request, CancellationToken cancellationToken)
    {
        await using var context = await ContextFactory.CreateDbContextAsync(cancellationToken);

        var query = await BuildQuery(context, request, cancellationToken);
        query = Sort(request, query);
        var list = await MapQueryableToModel(request, query.Paging(request), cancellationToken);

        var response = new PagedResponse<TModel>(list, await query.CountAsync(cancellationToken));

        return response;
    }

    protected virtual async Task<TModel[]> MapQueryableToModel(
        TListQuery request,
        IQueryable<TEntity> query,
        CancellationToken cancellationToken)
    {
        var materialized = await query.ToArrayAsync(cancellationToken);

        return Mapper.Map<TModel[]>(materialized);
    }

    /// <summary>
    /// Добавить специфичные запросу фильтры.
    /// </summary>
    /// <returns></returns>
    protected virtual async Task<IQueryable<TEntity>> BuildQuery(
        TContext context,
        TListQuery request,
        CancellationToken cancellationToken)
    {
        var query = context
            .Set<TEntity>()
            .AsQueryable()
            .AsNoTracking();

        if (request.Ids is not null)
            query = query.Where(e => request.Ids.Contains(e.Id));

        if (request.ExcludeIds is not null)
            query = query.Where(e => !request.ExcludeIds.Contains(e.Id));

        query = await query.ApplyModifiers<TEntity, TId, TModel, TListQuery>(Modifiers, request, cancellationToken);

        return query;
    }

    /// <summary>
    /// Добавляет сортировку к запросу по указанным полям (SortableFields); применяеться в соответствующем порядке
    /// </summary>
    protected virtual IQueryable<TEntity> Sort(TListQuery request, IQueryable<TEntity> query)
    {
        var isNestedSort = false;

        if (request.SortByIds?.Any() == true)
        {
            query = query.OrderByDescending(x => request.SortByIds.Contains(x.Id));

            isNestedSort = true;
        }

        if (request.SortableProperties?.Any() == true)
        {
            foreach (var sortableField in request.SortableProperties)
            {
                query = Sort(query,
                     sortableField.PropertySelector,
                     sortableField.IsDescending,
                     isNestedSort);

                isNestedSort = true;
            }

            return query;
        }

        if (isNestedSort)
        {
            return query;
        }

        return DefaultSort(request, query);
    }

    /// <summary>
    /// Default sorting when sort fields are empty
    /// </summary>
    /// <param name="request"></param>
    /// <param name="query"></param>
    /// <returns></returns>
    protected virtual IQueryable<TEntity> DefaultSort(TListQuery request, IQueryable<TEntity> query)
    {
        if (typeof(TEntity).IsAssignableTo(typeof(IHasDateCreated)))
        {
            return SortByCreatedDate(query);
        }

        return query.OrderBy(x => x.Id);
    }

    private static LambdaExpression? _sortByCreatedDateExpression;

    private static IQueryable<TEntity> SortByCreatedDate(IQueryable<TEntity> query)
    {
        if (_sortByCreatedDateExpression == null)
        {
            var propertyInfo = typeof(TEntity).GetProperty(nameof(IHasDateCreated.DateCreated));
            var parameterExpression = Expression.Parameter(typeof(TEntity), "p");
            var propertyAccess = Expression.MakeMemberAccess(parameterExpression, propertyInfo);

            _sortByCreatedDateExpression = Expression.Lambda(
                propertyAccess,
                parameterExpression);
        }

        return Sort(
            query,
            _sortByCreatedDateExpression,
            isDescending: true,
            isNestedSort: false);
    }

    private static IOrderedQueryable<TSource> Sort<TSource>(
        IQueryable<TSource> source,
        LambdaExpression propertySelector,
        bool isDescending = false,
        bool isNestedSort = false)
    {
        var propertyInfo = typeof(TSource).GetProperty(propertySelector.GetMemberAccess().Name);
        var parameterExpression = Expression.Parameter(typeof(TSource), "p");
        var propertyAccess = Expression.MakeMemberAccess(parameterExpression, propertyInfo!);

        var propertySelectorForEntity = Expression.Lambda(
            propertyAccess,
            parameterExpression);

        var resultExp2 = Expression.Call(
            typeof(Queryable),
            isNestedSort
                ? isDescending
                    ? "ThenByDescending"
                    : "ThenBy"
                : isDescending
                    ? "OrderByDescending"
                    : "OrderBy",
            new[] { typeof(TSource), propertySelectorForEntity.ReturnType },
            source.Expression,
            Expression.Quote(propertySelectorForEntity));

        return (IOrderedQueryable<TSource>)source.Provider.CreateQuery<TSource>(resultExp2);
    }
}