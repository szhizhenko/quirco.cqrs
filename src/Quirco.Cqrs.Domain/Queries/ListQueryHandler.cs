using System.Linq.Expressions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Quirco.Cqrs.Core.Mapping;
using Quirco.Cqrs.Core.Models;
using Quirco.Cqrs.DataAccess.Models;
using Quirco.Cqrs.Domain.DataContextExtensions;
using Quirco.Cqrs.Domain.Extensions;
using Quirco.Cqrs.Domain.Filters;
using Quirco.Cqrs.Domain.Models;
using Quirco.Cqrs.Infrastructure.Extensions;

namespace Quirco.Cqrs.Domain.Queries;

public abstract class ListQueryHandler<TModel, TEntity, TContext, TListQuery>
    : ListQueryHandler<TModel, TEntity, TContext, TListQuery, int>
    where TEntity : class, IHasId
    where TContext : DbContext
    where TListQuery : ListQuery<TModel, int>
{
    protected ListQueryHandler(
        IMapper mapper,
        IDbContextFactory<TContext> context,
        IEnumerable<IQueryableModifier<TEntity>>? modifiers) : base(mapper, context, modifiers)
    {
    }
}

/// <summary>
/// Обработчик запроса на поиск элементов.
/// </summary>
/// <typeparam name="TModel">Тип модели.</typeparam>
/// <typeparam name="TEntity">Тип сущности в БД.</typeparam>
/// <typeparam name="TContext">Тип контекста БД.</typeparam>
/// <typeparam name="TListQuery">Тип поискового запроса.</typeparam>
/// <typeparam name="TId">Type of entity id.</typeparam>
public abstract class ListQueryHandler<TModel, TEntity, TContext, TListQuery, TId>
    : IRequestHandler<TListQuery, PagedResponse<TModel>>
    where TEntity : class, IHasId<TId>
    where TContext : DbContext
    where TListQuery : ListQuery<TModel, TId?>
{
    /// <summary>
    /// Initializes a new instance of the <see cref="ListQueryHandler{TModel, TEntity, TContext, TListQuery}"/>.
    /// </summary>
    protected ListQueryHandler(
        IMapper mapper,
        IDbContextFactory<TContext> contextFactory,
        IEnumerable<IQueryableModifier<TEntity>>? modifiers)
    {
        Mapper = mapper;
        ContextFactory = contextFactory;
        Modifiers = modifiers;
    }

    /// <summary>
    /// Маппер сущностей.
    /// </summary>
    protected IMapper Mapper { get; }

    /// <summary>
    /// Контекст работы с БД.
    /// </summary>
    protected IDbContextFactory<TContext> ContextFactory { get; }

    /// <summary>
    /// Модификаторы запроса.
    /// </summary>
    protected IEnumerable<IQueryableModifier<TEntity>>? Modifiers { get; }

    /// <inheritdoc />
    public virtual async Task<PagedResponse<TModel>> Handle(TListQuery request, CancellationToken cancellationToken)
    {
        await using var context = await ContextFactory.CreateDbContextAsync(cancellationToken);

        var query = await BuildQuery(context, request, cancellationToken);
        var modelQuery = await MapQueryableToModel(request, query, cancellationToken);
        var sortedQueryModel = Sort(request, modelQuery);
        var list = await sortedQueryModel.Paging(request).ToArrayAsync(cancellationToken);
        var response = new PagedResponse<TModel>(list, await query.CountAsync(cancellationToken));

        return response;
    }

    protected virtual Task<IQueryable<TModel>> MapQueryableToModel(
        TListQuery request,
        IQueryable<TEntity> query,
        CancellationToken cancellationToken)
    {
        return Task.FromResult(Mapper.ProjectTo<TModel>(query));
    }

    /// <summary>
    /// Добавить специфичные запросу фильтры.
    /// </summary>
    protected virtual async Task<IQueryable<TEntity>> BuildQuery(
        TContext context,
        TListQuery request,
        CancellationToken cancellationToken)
    {
        var query = context
            .Set<TEntity>()
            .AsQueryable()
            .AsNoTracking();

        if (request.Ids is not null)
            query = query.IgnoreQueryFilters().Where(e => request.Ids.Contains(e.Id));

        if (request.ExcludeIds is not null)
            query = query.Where(e => !request.ExcludeIds.Contains(e.Id));

        if (request.IgnoreQueryFilters)
        {
            query = query.IgnoreQueryFilters();
        }

        query = await query.ApplyModifiers<TEntity, TId, TModel, TListQuery>(Modifiers, request, cancellationToken);

        return query.OrderBy(e => e.Id);
    }

    /// <summary>
    /// Добавляет сортировку к запросу по указанным полям (SortableFields); применяеться в соответствующем порядке
    /// </summary>
    protected virtual IQueryable<TModel> Sort(TListQuery request, IQueryable<TModel> query)
    {
        var isNestedSort = request.SortByIds?.Any() == true && TrySortByIds(ref query, request.SortByIds);

        if (request.SortableProperties?.Any() == true)
        {
            foreach (var sortableField in request.SortableProperties)
            {
                query = Sort(query,
                     sortableField.PropertySelector,
                     sortableField.IsDescending,
                     isNestedSort);

                isNestedSort = true;
            }

            return query;
        }

        if (isNestedSort)
        {
            return query;
        }

        return DefaultSort(request, query);
    }

    private static bool TrySortByIds(ref IQueryable<TModel> source, IEnumerable<TId> ids)
    {
        var propertyInfo = typeof(TModel).GetProperty("Id");

        if (propertyInfo == null)
            return false;

        var parameterExpression = Expression.Parameter(typeof(TModel), "p");
        var propertyAccess = Expression.MakeMemberAccess(parameterExpression, propertyInfo);
        Expression<Func<IEnumerable<int>, bool>> containsExpr = q => q.Contains(default);

        var containsMethod = ((MethodCallExpression)containsExpr.Body).Method;

        var containsCall = Expression.Call(
            containsMethod,
            Expression.Constant(ids),
            propertyAccess
        );

        var whereExpr = (Expression<Func<TModel, bool>>)Expression.Lambda(containsCall, parameterExpression);

        source = source.OrderByDescending(whereExpr);

        return true;
    }

    /// <summary>
    /// Default sorting when sort fields are empty
    /// </summary>
    /// <param name="request"></param>
    /// <param name="query"></param>
    /// <returns></returns>
    protected virtual IQueryable<TModel> DefaultSort(TListQuery request, IQueryable<TModel> query)
    {
        if (typeof(TModel).IsAssignableTo(typeof(IHasDateCreated)))
        {
            return SortByCreatedDate(query);
        }

        return query;
    }

    private static LambdaExpression? _sortByCreatedDateExpression;

    private static IQueryable<TModel> SortByCreatedDate(IQueryable<TModel> query)
    {
        if (_sortByCreatedDateExpression == null)
        {
            var propertyInfo = typeof(TModel).GetProperty(nameof(IHasDateCreated.DateCreated));

            if (propertyInfo == null)
                throw new InvalidOperationException(
                    $"Model of type '{typeof(TModel)}' should implement IHasDateCreated interface");

            var parameterExpression = Expression.Parameter(typeof(TModel), "p");
            var propertyAccess = Expression.MakeMemberAccess(parameterExpression, propertyInfo);

            _sortByCreatedDateExpression = Expression.Lambda(
                propertyAccess,
                parameterExpression);
        }

        return Sort(
            query,
            _sortByCreatedDateExpression,
            isDescending: true,
            isNestedSort: false);
    }

    private static IOrderedQueryable<TSource> Sort<TSource>(
        IQueryable<TSource> source,
        LambdaExpression propertySelector,
        bool isDescending = false,
        bool isNestedSort = false)
    {
        var propertyInfo = propertySelector.GetPropertyInfo();

        var resultExp2 = Expression.Call(
            typeof(Queryable),
            isNestedSort
                ? isDescending
                    ? "ThenByDescending"
                    : "ThenBy"
                : isDescending
                    ? "OrderByDescending"
                    : "OrderBy",
            new[] { typeof(TSource), propertyInfo.PropertyType },
            source.Expression,
            Expression.Quote(propertySelector));

        return (IOrderedQueryable<TSource>)source.Provider.CreateQuery<TSource>(resultExp2);
    }
}