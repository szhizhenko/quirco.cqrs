using Quirco.Cqrs.Domain.Models;

namespace Quirco.Cqrs.Domain.Queries;

public static partial class ListQueryExtensions
{
    /// <summary>
    /// Ignore queryable modifier of certain type when querying
    /// </summary>
    /// <param name="source">Source query</param>
    /// <typeparam name="TModel">Type of model</typeparam>
    /// <typeparam name="TId">Type of model's ID</typeparam>
    /// <typeparam name="TModifier">Type of modifier to be ignored</typeparam>
    /// <returns>Modified list query</returns>
    public static ListQuery<TModel, TId> IgnoreQueryableModifier<TModel, TId, TModifier>(this ListQuery<TModel, TId> source)
    {
        source.IgnoreQueryableModifiers ??= new HashSet<Type>();
        source.IgnoreQueryableModifiers.Add(typeof(TModifier));
        return source;
    }

    /// <summary>
    /// Ignores all queryable modifiers on select
    /// </summary>
    public static ListQuery<TModel, TId> IgnoreQueryableModifiers<TModel, TId>(this ListQuery<TModel, TId> source)
    {
        source.IgnoreAllQueryableModifiers = true;
        return source;
    }

    /// <summary>
    /// Forces query to ignore any applied query filter
    /// </summary>
    public static ListQuery<TModel, TId> IgnoreQueryFilters<TModel, TId>(this ListQuery<TModel, TId> source)
    {
        source.IgnoreQueryFilters = true;
        return source;
    }
}