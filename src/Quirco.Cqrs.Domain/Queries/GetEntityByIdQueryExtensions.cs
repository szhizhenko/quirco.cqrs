namespace Quirco.Cqrs.Domain.Queries;

public static partial class ListQueryExtensions
{
    /// <summary>
    /// Ignore all queryable modifiers when querying
    /// </summary>
    /// <param name="source">Source query</param>
    /// <typeparam name="TModel">Type of model</typeparam>
    /// <typeparam name="TId">Type of model's ID</typeparam>
    /// <returns>Modified list query</returns>
    public static GetEntityByIdQuery<TModel, TId> IgnoreQueryableModifiers<TModel, TId>(this GetEntityByIdQuery<TModel, TId> source)
    {
        source.IgnoreAllQueryableModifiers = true;
        return source;
    }

    /// <summary>
    /// Включает query-фильтрацию при получении элемента по идентификатору
    /// </summary>
    /// <param name="source">Source query</param>
    /// <typeparam name="TModel">Type of model</typeparam>
    /// <typeparam name="TId">Type of model's ID</typeparam>
    /// <returns>Modified list query</returns>
    public static GetEntityByIdQuery<TModel, TId> EnableQueryFilters<TModel, TId>(this GetEntityByIdQuery<TModel, TId> source)
    {
        source.IgnoreQueryFilters = false;
        return source;
    }
}