﻿using System.ComponentModel;
using MediatR;

namespace Quirco.Cqrs.Domain.Queries;

public abstract class GetEntityByIdQuery<T> : GetEntityByIdQuery<T, int>
{
    protected GetEntityByIdQuery(int id) : base(id)
    {
    }
}

/// <summary>
/// Get entity model by id
/// </summary>
/// <typeparam name="T">Type of model</typeparam>
/// <typeparam name="TId">Type of identifier</typeparam>
public abstract class GetEntityByIdQuery<T, TId> : IRequest<T>
{
    /// <summary>
    /// Инициализирует новый экземпляр класса <see cref="GetEntityByIdQuery{T}"/>.
    /// </summary>
    public GetEntityByIdQuery(TId id)
    {
        Id = id;
    }

    /// <summary>
    /// Идентификатор сущности.
    /// </summary>
    [DisplayName("Идентификатор сущности")]
    public TId Id { get; set; }

    /// <summary>
    /// Определяет, следует ли игнорировать вообще все модификаторы при выборке
    /// </summary>
    internal bool IgnoreAllQueryableModifiers { get; set; }

    /// <summary>
    /// Игнорировать ли query-фильтры. По умолчанию фильтры отключены.
    /// </summary>
    internal bool IgnoreQueryFilters { get; set; } = DomainGlobalConfiguration.GetEntityById.IgnoreQueryFilters;
}