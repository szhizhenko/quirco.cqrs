using System.Linq.Expressions;
using Quirco.Cqrs.Domain.Models;

namespace Quirco.Cqrs.Domain.Queries;

public static partial class ListQueryExtensions
{
    public static ListQuery<TModel, TId> OrderBy<TModel, TKey, TId>(this ListQuery<TModel, TId> source, Expression<Func<TModel, TKey>> keySelector, bool isDescending)
    {
        source.SortableProperties ??= new List<SortableProperty>();
        source.SortableProperties.Add(new SortableProperty
        {
            PropertySelector = keySelector,
            IsDescending = isDescending
        });

        return source;
    }

    public static ListQuery<TModel, TId> OrderBy<TModel, TKey, TId>(this ListQuery<TModel, TId> source, Expression<Func<TModel, TKey>> keySelector)
    {
        return source.OrderBy(keySelector, false);
    }

    public static ListQuery<TModel, TId> OrderByDescending<TModel, TKey, TId>(this ListQuery<TModel, TId> source, Expression<Func<TModel, TKey>> keySelector)
    {
        return source.OrderBy(keySelector, true);
    }

    public static ListQuery<TModel, TId> OrderBy<TModel, TId>(this ListQuery<TModel, TId> source, string propertyName, bool isDescending)
    {
        var parameterExpression = Expression.Parameter(typeof(TModel), "p");

        Expression propertyAccess = parameterExpression;

        foreach (var member in propertyName.Split('.'))
        {
            propertyAccess = Expression.PropertyOrField(propertyAccess, member);
        }

        var orderByPropertyLambda = Expression.Lambda(
            propertyAccess,
            parameterExpression);

        source.SortableProperties ??= new List<SortableProperty>();
        source.SortableProperties.Add(new SortableProperty
        {
            PropertySelector = orderByPropertyLambda,
            IsDescending = isDescending
        });

        return source;
    }

    public static ListQuery<TModel, TId> OrderBy<TModel, TId>(this ListQuery<TModel, TId> source, string propertyName)
    {
        return source.OrderBy(propertyName, false);
    }

    public static ListQuery<TModel, TId> OrderByDescending<TModel, TId>(this ListQuery<TModel, TId> source, string propertyName)
    {
        return source.OrderBy(propertyName, true);
    }

    public static ListQuery<TModel, TId> ClearSort<TModel, TId>(this ListQuery<TModel, TId> source)
    {
        source.SortableProperties = null;
        return source;
    }
}