using Quirco.Cqrs.Domain.Models;

namespace Quirco.Cqrs.Domain.Queries;

/// <summary>
/// Search by query text.
/// </summary>
public abstract class SearchQuery<TModel> : SearchQuery<TModel, int>
{
}

/// <summary>
/// Search by query text.
/// </summary>
public abstract class SearchQuery<TModel, TId> : ListQuery<TModel, TId>
{
    /// <summary>
    /// Search query (term)
    /// </summary>
    public string? Query { get; set; }
}