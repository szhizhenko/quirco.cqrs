namespace Quirco.Cqrs.Domain.Exceptions;

public class ConcurrencyException : ConcurrencyException<int>
{
    public ConcurrencyException(Type objectType, int entityId) : base(objectType, entityId)
    {
    }
}

public class ConcurrencyException<TId> : ApplicationException
{
    /// <summary>
    /// Инициализирует новый экземпляр класса <see cref="ConcurrencyException&lt;T&gt;"/>.
    /// </summary>
    public ConcurrencyException(Type objectType, TId entityId)
        : base($"Entity '{objectType}' with Id={entityId} already changed")
    {
        ObjectType = objectType;
        EntityId = entityId;
    }

    public Type ObjectType { get; set; }

    public TId EntityId { get; set; }
}