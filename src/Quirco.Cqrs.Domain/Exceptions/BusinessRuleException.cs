using System.Runtime.Serialization;
using FluentValidation;
using FluentValidation.Results;

namespace Quirco.Cqrs.Domain.Exceptions;

/// <summary>
/// Исключение генерируемое при нарушении бизнес-логики.
/// </summary>
public class BusinessRuleException : ValidationException
{
    /// <summary>
    /// Инициализирует новый экземпляр класса <see cref="BusinessRuleException"/>.
    /// </summary>
    /// <param name="message"></param>
    public BusinessRuleException(string message) : base(message)
    {
    }

    /// <summary>
    /// Инициализирует новый экземпляр класса <see cref="BusinessRuleException"/>.
    /// </summary>
    /// <param name="validationFailure"></param>
    public BusinessRuleException(ValidationFailure validationFailure)
        : base(new List<ValidationFailure>(new[] { validationFailure }))
    {
    }

    /// <summary>
    /// Инициализирует новый экземпляр класса <see cref="BusinessRuleException"/>.
    /// </summary>
    /// <param name="message"></param>
    /// <param name="errors"></param>
    public BusinessRuleException(string message, IEnumerable<ValidationFailure> errors)
        : base(message, errors)
    {
    }

    /// <summary>
    /// Инициализирует новый экземпляр класса <see cref="BusinessRuleException"/>.
    /// </summary>
    /// <param name="errors"></param>
    public BusinessRuleException(IEnumerable<ValidationFailure> errors)
        : base(errors)
    {
    }

    /// <summary>
    /// Инициализирует новый экземпляр класса <see cref="BusinessRuleException"/>.
    /// </summary>
    /// <param name="info"></param>
    /// <param name="context"></param>
    public BusinessRuleException(SerializationInfo info, StreamingContext context)
        : base(info, context)
    {
    }
}