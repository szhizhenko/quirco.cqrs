﻿namespace Quirco.Cqrs.Domain.Exceptions;

/// <summary>
/// Доступ к сущности запрещен.
/// </summary>
public class AccessDeniedException : ApplicationException
{
    public AccessDeniedException() : this("Access denied")
    {
    }

    public AccessDeniedException(string? message) : base(message)
    {
    }

    public AccessDeniedException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}