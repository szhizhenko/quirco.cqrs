﻿using FluentValidation;

namespace Quirco.Cqrs.Domain.Models;

public class OpenDateRangeValidator : AbstractValidator<OpenDateRange>
{
    public OpenDateRangeValidator()
    {
        RuleFor(q => q.To)
            .GreaterThanOrEqualTo(q => q.From)
            .When(e => e.From != null && e.To != null);
    }
}