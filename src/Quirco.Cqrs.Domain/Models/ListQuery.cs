using MediatR;
using Quirco.Cqrs.Core.Models;

namespace Quirco.Cqrs.Domain.Models;

/// <summary>
/// Поисковый запрос для получения списка сущностей.
/// </summary>
/// <typeparam name="TModel">Тип модели</typeparam>
public abstract class ListQuery<TModel> : ListQuery<TModel, int>
{
}

/// <summary>
/// Поисковый запрос для получения списка сущностей.
/// </summary>
/// <typeparam name="TModel">Тип модели</typeparam>
/// <typeparam name="TId">Тип идентификатора сущностей</typeparam>
public abstract class ListQuery<TModel, TId> : LimitQuery, IRequest<PagedResponse<TModel>>
{
    /// <summary>
    /// Перечень идентификаторов сущностей для фильтрации по айди
    /// </summary>
    public TId[]? Ids { get; set; }

    /// <summary>
    /// Перечень идентификаторов сущностей, которыйх нужно исключить из выдачи
    /// </summary>
    public TId[]? ExcludeIds { get; set; }

    /// <summary>
    /// Показывать в начале
    /// </summary>
    public TId[]? SortByIds { get; set; }

    /// <summary>
    /// Игнорирует глобальный фильтр ef
    /// </summary>
    internal bool IgnoreQueryFilters { get; set; }

    /// <summary>
    /// Массив с полями для сортировки
    /// </summary>
    internal List<SortableProperty>? SortableProperties { get; set; }

    /// <summary>
    /// Список модификаторов, которые следует игнорировать при выборке данных
    /// </summary>
    internal HashSet<Type>? IgnoreQueryableModifiers { get; set; }

    /// <summary>
    /// Определяет, следует ли игнорировать вообще все модификаторы при выборке
    /// </summary>
    internal bool IgnoreAllQueryableModifiers { get; set; }
}