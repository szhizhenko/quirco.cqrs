using System.Diagnostics;

namespace Quirco.Cqrs.Domain.Models;

/// <summary>
/// Открытый интервал дат
/// </summary>
[DebuggerDisplay("{From}-{To}")]
public class OpenDateRange
{
    /// <summary>
    /// Инициализирует новый экземпляр класса <see cref="OpenDateRange"/>.
    /// </summary>
    public OpenDateRange()
    {
    }

    /// <summary>
    /// Инициализирует новый экземпляр класса <see cref="OpenDateRange"/>.
    /// </summary>
    public OpenDateRange(DateTime? @from, DateTime? to)
    {
        From = @from;
        To = to;
    }

    /// <summary>
    /// Начало отрезка включительно.
    /// </summary>
    public DateTime? From { get; set; }

    /// <summary>
    /// Конец отрезка включительно.
    /// </summary>
    public DateTime? To { get; set; }
}