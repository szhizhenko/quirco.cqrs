using System.Linq.Expressions;

namespace Quirco.Cqrs.Domain.Models;

/// <summary>
/// Модель для сортировки
/// </summary>
internal class SortableProperty
{
    /// <summary>
    /// Лямбда для выбора свойства
    /// </summary>
    public LambdaExpression PropertySelector { get; set; }

    /// <summary>
    /// True - от меньшего к большему, False - от большего к меньшему
    /// </summary>
    public bool IsDescending { get; set; }
}