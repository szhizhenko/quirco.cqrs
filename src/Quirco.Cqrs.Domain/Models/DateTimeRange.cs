﻿using System.Diagnostics;

namespace Quirco.Cqrs.Domain.Models;

[DebuggerDisplay($"{nameof(DateTimeFrom)}-{nameof(DateTimeTo)}")]
public struct DateTimeRange
{
    /// <summary>
    /// Дата и время начала
    /// </summary>
    public DateTime DateTimeFrom { get; }

    /// <summary>
    /// Дата и время окончания
    /// </summary>
    public DateTime DateTimeTo { get; }

    public TimeSpan Span => DateTimeTo - DateTimeFrom;

    public int SpanDays => (int)(DateTimeTo.Date - DateTimeFrom.Date).TotalDays;

    /// <summary>
    /// Коллекция дат в интервале
    /// </summary>
    public IEnumerable<DateTime> Dates
    {
        get
        {
            for (var i = 0; i < SpanDays; i++)
            {
                yield return DateTimeFrom.Date.AddDays(i);
            }
        }
    }

    public DateTimeRange() : this(DateTime.Now, DateTime.Now.AddDays(1))
    {
    }

    public DateTimeRange(DateTime dateTimeFrom, DateTime dateTimeTo) : this(dateTimeFrom, dateTimeTo, false)
    {
    }

    public DateTimeRange(DateTime dateTimeFrom, DateTime dateTimeTo, bool correctDates)
    {
        if (dateTimeFrom > dateTimeTo)
        {
            if (correctDates)
                dateTimeTo = dateTimeFrom;
            else
                throw new ArgumentException($"Unable to create time interval of negative length ({dateTimeFrom} - {dateTimeTo})");
        }

        DateTimeFrom = dateTimeFrom;
        DateTimeTo = dateTimeTo;
    }

    public DateTimeRange(string dateTimeFrom, string dateTimeTo) : this(DateTime.Parse(dateTimeFrom), DateTime.Parse(dateTimeTo))
    {
    }

    public bool OverlapsWith(DateTimeRange otherTimeRange)
    {
        return OverlapsWith(otherTimeRange.DateTimeFrom, otherTimeRange.DateTimeTo);
    }

    public bool OverlapsWith(DateTime dateFrom, DateTime dateTo)
    {
        return DateTimeFrom < dateTo && dateFrom < DateTimeTo;
    }

    public bool Equals(DateTimeRange other)
    {
        return DateTimeFrom.Equals(other.DateTimeFrom) && DateTimeTo.Equals(other.DateTimeTo);
    }

    public override bool Equals(object? obj)
    {
        return obj is DateTimeRange other && Equals(other);
    }

    public override int GetHashCode()
    {
        return HashCode.Combine(DateTimeFrom, DateTimeTo);
    }

    public DateTimeRange Clone()
    {
        return new DateTimeRange(DateTimeFrom, DateTimeTo);
    }

    /// <summary>
    /// Осуществляет оптимизированную проверку на пересечение временного интервала с массивом упорядоченных по времени отрезков.
    /// </summary>
    /// <param name="timeRange">Интервал, который будем проверять на пересечение</param>
    /// <param name="orderedRangesToCheck">Массив упорядоченных интервалов, с которыми будем проверять пересечение</param>
    /// <returns>Перечисление пересекающихся отрезков с их порядковыми в исходном списке индексами</returns>
    public static IEnumerable<OverlapResult> GetOverlappedItems(DateTimeRange timeRange, IList<DateTimeRange> orderedRangesToCheck)
    {
        if (timeRange.DateTimeTo < orderedRangesToCheck.First().DateTimeFrom)
            yield break;

        if (timeRange.DateTimeFrom > orderedRangesToCheck.Last().DateTimeTo)
            yield break;
        var overlapped = false;
        for (var i = 0; i < orderedRangesToCheck.Count; i++)
        {
            var item = orderedRangesToCheck[i];
            if (timeRange.OverlapsWith(item))
            {
                yield return new OverlapResult(i, item);
                overlapped = true;
            }
            else if (overlapped)
            {
                yield break; // в прошлом отрезки пересекались, а теперь нет, выходим
            }
        }
    }

    /// <summary>
    /// Does this interval contains date?
    /// </summary>
    /// <param name="date">Date to check</param>
    /// <param name="inclusive">If right side should be included in check</param>
    /// <returns></returns>
    public bool Contains(DateTime date, bool inclusive = false)
    {
        if (inclusive)
            return DateTimeFrom <= date && date <= DateTimeTo;
        return DateTimeFrom <= date && date < DateTimeTo;
    }

    public bool Contains(IEnumerable<DateTimeRange> ranges)
    {
        var dateTimeFrom = DateTimeFrom;
        var dateTimeTo = DateTimeTo;
        return ranges.Aggregate(true, (current, range) => current && dateTimeFrom <= range.DateTimeFrom && range.DateTimeTo <= dateTimeTo);
    }

    public bool Contains(OpenDateRange range)
    {
        if (range.From == null || range.To == null)
            return false;
        return DateTimeFrom <= range.From && range.To <= DateTimeTo;
    }

    public DateTimeRange ExtendEnd(TimeSpan timeSpan)
    {
        return new DateTimeRange(DateTimeFrom, DateTimeTo + timeSpan);
    }

    public DateTimeRange ExtendToBothSides(TimeSpan timeSpan)
    {
        return new DateTimeRange(DateTimeFrom - timeSpan, DateTimeTo + timeSpan);
    }

    public DateTimeRange Shift(TimeSpan timeSpan)
    {
        if (timeSpan == TimeSpan.Zero)
            return this;
        return new DateTimeRange(DateTimeFrom + timeSpan, DateTimeTo + timeSpan);
    }

    public DateTimeRange Union(DateTimeRange dateTimeRange)
    {
        var dateTimeFrom = DateTimeFrom;
        var dateTimeTo = DateTimeTo;
        if (dateTimeRange.DateTimeFrom < DateTimeFrom)
            dateTimeFrom = dateTimeRange.DateTimeFrom;
        if (dateTimeRange.DateTimeTo > DateTimeTo)
            dateTimeTo = dateTimeRange.DateTimeTo;
        return new DateTimeRange(dateTimeFrom, dateTimeTo);
    }

    public DateTimeRange LimitTo(DateTimeRange limit)
    {
        var dateTimeFrom = DateTimeFrom;
        var dateTimeTo = DateTimeTo;

        if (dateTimeTo < limit.DateTimeFrom)
        {
            dateTimeTo = limit.DateTimeFrom;
        }

        if (dateTimeFrom > limit.DateTimeTo)
        {
            dateTimeFrom = limit.DateTimeTo;
        }

        if (limit.DateTimeFrom > dateTimeFrom)
            dateTimeFrom = limit.DateTimeFrom;

        if (limit.DateTimeTo < dateTimeTo)
            dateTimeTo = limit.DateTimeTo;

        if (dateTimeFrom > dateTimeTo)
        {
            dateTimeTo = dateTimeFrom;
        }

        return new DateTimeRange(dateTimeFrom, dateTimeTo);
    }

    public static DateTimeRange? Range(ICollection<DateTimeRange> dateRanges)
    {
        if (!dateRanges.Any())
            return null;
        return new DateTimeRange(dateRanges.Min(dr => dr.DateTimeFrom), dateRanges.Max(dr => dr.DateTimeTo));
    }

    public int GetTotalMonths()
    {
        return (DateTimeTo.Year - DateTimeFrom.Year) * 12 + (DateTimeTo.Month - DateTimeFrom.Month) + (DateTimeTo.Day >= DateTimeFrom.Day ? 0 : -1);
    }

    public static bool operator ==(DateTimeRange left, DateTimeRange right)
    {
        return left.Equals(right);
    }

    public static bool operator !=(DateTimeRange left, DateTimeRange right)
    {
        return !(left == right);
    }
}

public class OverlapResult
{
    public int Index { get; set; }
    public DateTimeRange TimeRange { get; set; }

    public OverlapResult(int index, DateTimeRange timeRange)
    {
        Index = index;
        TimeRange = timeRange;
    }
}