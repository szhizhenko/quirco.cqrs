﻿using System.Diagnostics;

namespace Quirco.Cqrs.Domain.Models;

[DebuggerDisplay("{DateFrom}-{DateTo}")]
public struct DateOnlyRange
{
    /// <summary>
    /// Дата и время начала
    /// </summary>
    public DateOnly DateFrom { get; }

    /// <summary>
    /// Дата и время окончания
    /// </summary>
    public DateOnly DateTo { get; }

    public int SpanDays => DateTo.DayNumber - DateFrom.DayNumber;

    /// <summary>
    /// Коллекция дат в интервале
    /// </summary>
    public IEnumerable<DateOnly> Dates
    {
        get
        {
            for (var i = 0; i < SpanDays; i++)
            {
                yield return DateFrom.AddDays(i);
            }
        }
    }

    public DateOnlyRange(DateOnly dateFrom, DateOnly dateTo) : this(dateFrom, dateTo, false)
    {
    }

    public DateOnlyRange(DateOnly dateFrom, DateOnly dateTo, bool correctDates)
    {
        if (dateFrom > dateTo)
        {
            if (correctDates)
                dateTo = dateFrom;
            else
                throw new ArgumentException($"Unable to create time interval of negative length ({dateFrom} - {dateTo})");
        }

        DateFrom = dateFrom;
        DateTo = dateTo;
    }

    public DateOnlyRange(string dateTimeFrom, string dateTimeTo) : this(DateOnly.Parse(dateTimeFrom), DateOnly.Parse(dateTimeTo))
    {
    }

    public bool OverlapsWith(DateOnlyRange otherTimeRange)
    {
        return OverlapsWith(otherTimeRange.DateFrom, otherTimeRange.DateTo);
    }

    public bool OverlapsWith(DateOnly dateFrom, DateOnly dateTo)
    {
        return DateFrom < dateTo && dateFrom < DateTo;
    }

    public bool Equals(DateOnlyRange other)
    {
        return DateFrom.Equals(other.DateFrom) && DateTo.Equals(other.DateTo);
    }

    public override bool Equals(object? obj)
    {
        return obj is DateOnlyRange other && Equals(other);
    }

    public override int GetHashCode()
    {
        return HashCode.Combine(DateFrom, DateTo);
    }

    public DateOnlyRange Clone()
    {
        return new DateOnlyRange(DateFrom, DateTo);
    }

    /// <summary>
    /// Does this interval contains date?
    /// </summary>
    /// <param name="date">Date to check</param>
    /// <param name="inclusive">If right side should be included in check</param>
    /// <returns></returns>
    public bool Contains(DateOnly date, bool inclusive = false)
    {
        if (inclusive)
            return DateFrom <= date && date <= DateTo;
        return DateFrom <= date && date < DateTo;
    }

    public bool Contains(IEnumerable<DateOnlyRange> ranges)
    {
        var dateTimeFrom = DateFrom;
        var dateTimeTo = DateTo;
        return ranges.Aggregate(true, (current, range) => current && dateTimeFrom <= range.DateFrom && range.DateTo <= dateTimeTo);
    }

    public DateOnlyRange Union(DateOnlyRange dateTimeRange)
    {
        var dateTimeFrom = DateFrom;
        var dateTimeTo = DateTo;
        if (dateTimeRange.DateFrom < DateFrom)
            dateTimeFrom = dateTimeRange.DateFrom;
        if (dateTimeRange.DateTo > DateTo)
            dateTimeTo = dateTimeRange.DateTo;
        return new DateOnlyRange(dateTimeFrom, dateTimeTo);
    }

    public static DateOnlyRange? Range(ICollection<DateOnlyRange> dateRanges)
    {
        if (!dateRanges.Any())
            return null;
        return new DateOnlyRange(dateRanges.Min(dr => dr.DateFrom), dateRanges.Max(dr => dr.DateTo));
    }

    public int GetTotalMonths()
    {
        return (DateTo.Year - DateFrom.Year) * 12 + (DateTo.Month - DateFrom.Month) + (DateTo.Day >= DateFrom.Day ? 0 : -1);
    }

    public static bool operator ==(DateOnlyRange left, DateOnlyRange right)
    {
        return left.Equals(right);
    }

    public static bool operator !=(DateOnlyRange left, DateOnlyRange right)
    {
        return !(left == right);
    }
}