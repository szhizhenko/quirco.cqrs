﻿using FluentValidation;

namespace Quirco.Cqrs.Domain.Validators;

/// <summary>
/// Extensions for the <see cref="IRuleBuilder{T, TProperty}"/>.
/// </summary>
public static class RuleBuilderExtensions
{
    /// <summary>
    /// URL валидатор.
    /// </summary>
    /// <param name="builder"></param>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TProperty"></typeparam>
    /// <returns></returns>
    /// <exception cref="ArgumentNullException"></exception>
    public static IRuleBuilderOptions<T, TProperty> Url<T, TProperty>(this IRuleBuilder<T, TProperty> builder)
    {
        if (builder == null)
        {
            throw new ArgumentNullException(nameof(builder));
        }

        return builder.SetValidator(new UrlValidator<T, TProperty>());
    }

    /// <summary>
    /// Указывает, что свойство содержит только дату.
    /// </summary>
    /// <param name="builder"></param>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    /// <exception cref="ArgumentNullException"></exception>
    public static IRuleBuilderOptions<T, DateTime?> Date<T>(this IRuleBuilder<T, DateTime?> builder)
    {
        if (builder == null)
        {
            throw new ArgumentNullException(nameof(builder));
        }

        return builder.SetValidator(new NullableDateValidator<T>());
    }

    /// <summary>
    /// Указывает, что свойство содержит только дату.
    /// </summary>
    /// <param name="builder"></param>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    /// <exception cref="ArgumentNullException"></exception>
    public static IRuleBuilderOptions<T, DateTime> Date<T>(this IRuleBuilder<T, DateTime> builder)
    {
        if (builder == null)
        {
            throw new ArgumentNullException(nameof(builder));
        }

        return builder.SetValidator(new DateValidator<T>());
    }

    /// <summary>
    /// Устанавливает валидатор, который проверяет что элементы в коллекция являются уникальными.
    /// </summary>
    /// <param name="builder"></param>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TCollection"></typeparam>
    /// <typeparam name="TItem"></typeparam>
    /// <returns></returns>
    public static IRuleBuilder<T, TCollection> Unique<T, TCollection, TItem>(
        this IRuleBuilder<T, TCollection> builder)
        where TCollection : IEnumerable<TItem>
        => builder.Unique(EqualityComparer<TItem>.Default);

    /// <summary>
    /// Устанавливает валидатор, который проверяет что элементы в коллекции являются уникальными.
    /// </summary>
    public static IRuleBuilder<T, TCollection> Unique<T, TCollection, TItem>(
        this IRuleBuilder<T, TCollection> builder,
        IEqualityComparer<TItem> comparer)
        where TCollection : IEnumerable<TItem>
    {
        return builder.SetValidator(new UniqueCollectionValidator<T, TCollection, TItem>("{PropertyName} - значения должны быть уникальными.", comparer));
    }
}