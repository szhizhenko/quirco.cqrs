using System.Text.RegularExpressions;
using FluentValidation;
using FluentValidation.Validators;

namespace Quirco.Cqrs.Domain.Validators;

/// <summary>
/// Валидатор БИК.
/// </summary>
public class BicValidator<T> : PropertyValidator<T, string>
{
    private readonly string _errorMessage;

    /// <summary>
    /// Initializes a new instance
    /// </summary>
    public BicValidator() : this("Невалидный БИК.")
    {
    }

    /// <summary>
    /// Initializes a new instance
    /// </summary>
    /// <param name="errorMessage"></param>
    public BicValidator(string errorMessage)
    {
        _errorMessage = errorMessage;
    }

    protected override string GetDefaultMessageTemplate(string errorCode)
    {
        return _errorMessage;
    }

    public override string Name => "BicValidator";

    /// <inheritdoc />
    public override bool IsValid(ValidationContext<T> context, string bic)
    {
        return !string.IsNullOrWhiteSpace(bic) && Regex.IsMatch(bic, @"^\d{9}$");
    }
}