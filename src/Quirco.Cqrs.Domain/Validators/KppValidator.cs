using System.Text.RegularExpressions;
using FluentValidation;
using FluentValidation.Validators;

namespace Quirco.Cqrs.Domain.Validators;

/// <summary>
/// Валидатор КПП.
/// </summary>
public class KppValidator<T> : PropertyValidator<T, string>
{
    private readonly string _errorMessage;

    /// <summary>
    /// Initializes a new instance
    /// </summary>
    public KppValidator() : this("Невалидный КПП.")
    {
    }

    /// <summary>
    /// Initializes a new instance
    /// </summary>
    /// <param name="errorMessage"></param>
    public KppValidator(string errorMessage)
    {
        _errorMessage = errorMessage;
    }

    protected override string GetDefaultMessageTemplate(string errorCode)
    {
        return _errorMessage;
    }

    public override string Name => "KppValidator";

    /// <inheritdoc />
    public override bool IsValid(ValidationContext<T> context, string kpp)
    {
        return !string.IsNullOrWhiteSpace(kpp) && Regex.IsMatch(kpp, @"^\d{9}$");
    }
}