using System.Text.RegularExpressions;
using FluentValidation;
using FluentValidation.Validators;

namespace Quirco.Cqrs.Domain.Validators;

/// <summary>
/// Валидатор расчетного счета.
/// </summary>
public class CheckingAccountNumberValidator<T> : PropertyValidator<T, string>
{
    private readonly string _errorMessage;

    /// <summary>
    /// Initializes a new instance
    /// </summary>
    public CheckingAccountNumberValidator() : this("Невалидный расчетный счет.")
    {
    }

    /// <summary>
    /// Initializes a new instance
    /// </summary>
    /// <param name="errorMessage"></param>
    public CheckingAccountNumberValidator(string errorMessage)
    {
        _errorMessage = errorMessage;
    }

    protected override string GetDefaultMessageTemplate(string errorCode)
    {
        return _errorMessage;
    }

    public override string Name => "CheckingAccountNumberValidator";

    /// <inheritdoc />
    public override bool IsValid(ValidationContext<T> context, string checkingAccountNumber)
    {
        return !string.IsNullOrWhiteSpace(checkingAccountNumber)
               && Regex.IsMatch(checkingAccountNumber, @"^\d{20}$");
    }
}