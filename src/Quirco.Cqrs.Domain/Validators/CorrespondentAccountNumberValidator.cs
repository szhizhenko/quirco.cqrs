using System.Text.RegularExpressions;
using FluentValidation;
using FluentValidation.Validators;

namespace Quirco.Cqrs.Domain.Validators;

/// <summary>
/// Валидатор корреспондентского счета.
/// </summary>
public class CorrespondentAccountNumberValidator<T> : PropertyValidator<T, string>
{
    private readonly string _errorMessage;

    /// <summary>
    /// Initializes a new instance
    /// </summary>
    public CorrespondentAccountNumberValidator() : this("Невалидный корреспондентский счет.")
    {
    }

    /// <summary>
    /// Initializes a new instance
    /// </summary>
    /// <param name="errorMessage"></param>
    public CorrespondentAccountNumberValidator(string errorMessage)
    {
        _errorMessage = errorMessage;
    }

    protected override string GetDefaultMessageTemplate(string errorCode)
    {
        return _errorMessage;
    }

    public override string Name => "CorrespondentAccountNumberValidator";

    /// <inheritdoc />
    public override bool IsValid(ValidationContext<T> context, string correspondingAccountNumber)
    {
        return !string.IsNullOrWhiteSpace(correspondingAccountNumber)
               && Regex.IsMatch(correspondingAccountNumber, @"^\d{20}$");
    }
}