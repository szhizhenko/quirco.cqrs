﻿using FluentValidation;
using FluentValidation.Validators;

namespace Quirco.Cqrs.Domain.Validators;

/// <summary>
/// Валидатор ОГРН.
/// </summary>
public class OgrnValidator<T> : PropertyValidator<T, string>
{
    private readonly string _errorMessage;
    private const int OrganizationOgrnLength = 13;

    private const int BusinessmanOgrnLength = 15;

    /// <summary>
    /// Initializes a new instance
    /// </summary>
    public OgrnValidator() : this("Невалидный ОГРН.")
    {
    }

    /// <summary>
    /// Initializes a new instance
    /// </summary>
    /// <param name="errorMessage"></param>
    public OgrnValidator(string errorMessage)
    {
        _errorMessage = errorMessage;
    }

    protected override string GetDefaultMessageTemplate(string errorCode)
    {
        return _errorMessage;
    }

    public override string Name => "OgrnValidator";

    private static bool IsOrganizationOgrnValid(string ogrn)
    {
        return ulong.TryParse(ogrn, out var n) && n / 10 % 11 % 10 == n % 10;
    }

    private static bool IsBusinessmanOgrnValid(string ogrn)
    {
        return ulong.TryParse(ogrn, out var n) && n / 10 % 13 % 10 == n % 10;
    }

    /// <inheritdoc />
    public override bool IsValid(ValidationContext<T> context, string ogrn)
    {
        if (string.IsNullOrWhiteSpace(ogrn))
        {
            return false;
        }

        return ogrn.Length switch
        {
            OrganizationOgrnLength => IsOrganizationOgrnValid(ogrn),
            BusinessmanOgrnLength => IsBusinessmanOgrnValid(ogrn),
            _ => false
        };
    }
}