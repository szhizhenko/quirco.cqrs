﻿using FluentValidation;
using FluentValidation.Validators;

namespace Quirco.Cqrs.Domain.Validators;

/// <summary>
/// Валидатор ИНН.
/// </summary>
public class InnValidator<T> : PropertyValidator<T, string>
{
    private readonly string _errorMessage;
    private const int OrganizationInnLength = 10;

    private const int PersonInnLength = 12;

    /// <summary>
    /// Initializes a new instance
    /// </summary>
    public InnValidator() : this("Неверный ИНН.")
    {
    }

    /// <summary>
    /// Initializes a new instance
    /// </summary>
    /// <param name="errorMessage"></param>
    public InnValidator(string errorMessage)
    {
        _errorMessage = errorMessage;
    }

    protected override string GetDefaultMessageTemplate(string errorCode)
    {
        return _errorMessage;
    }

    public override string Name => "InnValidator";

    private static bool IsOrganizationInnValid(string innString)
    {
        if (long.TryParse(innString, out var inn))
        {
            return (2 * (inn / 1000000000 % 10) +
                    4 * (inn / 100000000 % 10) +
                    10 * (inn / 10000000 % 10) +
                    3 * (inn / 1000000 % 10) +
                    5 * (inn / 100000 % 10) +
                    9 * (inn / 10000 % 10) +
                    4 * (inn / 1000 % 10) +
                    6 * (inn / 100 % 10) +
                    8 * (inn / 10 % 10)) % 11 % 10 == inn % 10;
        }

        return false;
    }

    private static bool IsPersonInnValid(string innString)
    {
        if (ulong.TryParse(innString, out var inn))
        {
            return (7 * (inn / 100000000000 % 10) +
                    2 * (inn / 10000000000 % 10) +
                    4 * (inn / 1000000000 % 10) +
                    10 * (inn / 100000000 % 10) +
                    3 * (inn / 10000000 % 10) +
                    5 * (inn / 1000000 % 10) +
                    9 * (inn / 100000 % 10) +
                    4 * (inn / 10000 % 10) +
                    6 * (inn / 1000 % 10) +
                    8 * (inn / 100 % 10)) % 11 % 10 == inn % 100 / 10
                   &&
                   (3 * (inn / 100000000000 % 10) +
                    7 * (inn / 10000000000 % 10) +
                    2 * (inn / 1000000000 % 10) +
                    4 * (inn / 100000000 % 10) +
                    10 * (inn / 10000000 % 10) +
                    3 * (inn / 1000000 % 10) +
                    5 * (inn / 100000 % 10) +
                    9 * (inn / 10000 % 10) +
                    4 * (inn / 1000 % 10) +
                    6 * (inn / 100 % 10) +
                    8 * (inn / 10 % 10)) % 11 % 10 == inn % 10;
        }

        return false;
    }

    /// <inheritdoc />
    public override bool IsValid(ValidationContext<T> context, string inn)
    {
        if (string.IsNullOrWhiteSpace(inn))
        {
            return false;
        }

        return inn.Length switch
        {
            OrganizationInnLength => IsOrganizationInnValid(inn),
            PersonInnLength => IsPersonInnValid(inn),
            _ => false
        };
    }
}