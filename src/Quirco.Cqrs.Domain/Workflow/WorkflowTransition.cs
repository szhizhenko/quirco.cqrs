namespace Quirco.Cqrs.Domain.Workflow;

/// <summary>
/// Переход между статусами объекта
/// </summary>
/// <typeparam name="T"></typeparam>
/// <typeparam name="TState"></typeparam>
public class WorkflowTransition<T, TState>
{
    /// <summary>
    /// Уникальный идентификатор перехода
    /// </summary>
    public string Id { get; }

    /// <summary>
    /// Название перехода
    /// </summary>
    public string Name { get; }

    /// <summary>
    /// Целевой статус, в который данный переход помещает сущность
    /// </summary>
    public WorkflowStatus<T, TState> TargetStatus { get; }

    /// <summary>
    /// Initializes a new instance of <see cref="WorkflowTransition{T, TState}"/>.
    /// </summary>
    /// <param name="id"></param>
    /// <param name="name"></param>
    /// <param name="targetStatus"></param>
    public WorkflowTransition(string id, string name, WorkflowStatus<T, TState> targetStatus)
    {
        Id = id;
        Name = name;
        TargetStatus = targetStatus;
    }

    /// <summary>
    /// Применяет данный переход, переводит сущность в целевое состояние
    /// </summary>
    public virtual Task Apply(T entity, CancellationToken token)
    {
        return TargetStatus.MoveIn(entity, token);
    }
}