namespace Quirco.Cqrs.Domain.Workflow;

/// <summary>
/// Движок для описания workflow по переходу объекта между состояниями
/// </summary>
/// <typeparam name="T"></typeparam>
/// <typeparam name="TState"></typeparam>
public abstract class WorkflowEngine<T, TState>
{
    /// <summary>
    /// Возвращает текущее состояние объекта
    /// </summary>
    public abstract Task<WorkflowStatus<T, TState>> GetStatus(T obj, CancellationToken token);
}