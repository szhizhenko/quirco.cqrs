namespace Quirco.Cqrs.Domain.Workflow;

/// <summary>
/// Статус объекта
/// </summary>
/// <typeparam name="T"></typeparam>
/// <typeparam name="TState"></typeparam>
public abstract class WorkflowStatus<T, TState>
{
    /// <summary>
    /// Уникальный идентификатор статуса
    /// </summary>
    public TState Id { get; }

    /// <summary>
    /// Initializes a new instance of <see cref="WorkflowStatus{T, TState}"/>.
    /// </summary>
    /// <param name="id"></param>
    protected WorkflowStatus(TState id)
    {
        Id = id;
    }

    /// <summary>
    /// Возвращает перечень возможных переходов из данного состояния
    /// </summary>
    public abstract Task<ICollection<WorkflowTransition<T, TState>>> GetTransitions(T entity, CancellationToken token);

    /// <summary>
    /// Помещает сущность в текущее состояние
    /// </summary>
    public abstract Task MoveIn(T entity, CancellationToken token);
}