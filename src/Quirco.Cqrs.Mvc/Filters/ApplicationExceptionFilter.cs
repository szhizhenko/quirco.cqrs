﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Quirco.Cqrs.Mvc.Filters;

/// <summary>
/// Фильтр для исключения <see cref="ApplicationException"/>
/// </summary>
public class ApplicationExceptionFilter : ExceptionFilter<ApplicationException>
{
    protected override void Handle(ApplicationException exception, ExceptionContext context)
    {
        context.Result = new BadRequestObjectResult(new ApplicationExceptionResultValue
        {
            ExceptionType = exception.GetType().ToString(),
            Message = exception.Message,
            Detailed = exception.ToString()
        });
    }

    /// <summary>
    /// Модель возвращаемая фильтром исключений <see cref="ApplicationException"/>
    /// </summary>
    public class ApplicationExceptionResultValue
    {
        /// <summary>
        /// Тип исключения
        /// </summary>
        public string ExceptionType { get; set; }

        /// <summary>
        /// Описание исключения
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Подробное описание исключения
        /// </summary>
        public string Detailed { get; set; }
    }
}