﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Quirco.Cqrs.Mvc.Filters;

public class OperationCancelledExceptionFilter : ExceptionFilter<OperationCanceledException>
{
    protected override void Handle(OperationCanceledException exception, ExceptionContext context)
    {
        context.Result = new StatusCodeResult(400);
    }
}