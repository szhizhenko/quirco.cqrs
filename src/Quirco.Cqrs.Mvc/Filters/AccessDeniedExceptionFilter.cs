﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Quirco.Cqrs.Domain.Exceptions;

namespace Quirco.Cqrs.Mvc.Filters;

/// <summary>
/// Фильтр для исключения <see cref="AccessDeniedException"/>.
/// </summary>
public class AccessDeniedExceptionFilter : ExceptionFilter<AccessDeniedException>
{
    /// <inheritdoc />
    protected override void Handle(AccessDeniedException exception, ExceptionContext context)
    {
        context.Result = new StatusCodeResult(StatusCodes.Status403Forbidden);
    }
}