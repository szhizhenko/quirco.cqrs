﻿using FluentValidation.AspNetCore;
using FluentValidation.Results;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using ValidationException = FluentValidation.ValidationException;

namespace Quirco.Cqrs.Mvc.Filters;

/// <summary>
/// Отлавливает ошибки валидации.
/// </summary>
public class ValidationExceptionFilter : ExceptionFilter<ValidationException>
{
    /// <inheritdoc />
    protected override void Handle(ValidationException exception, ExceptionContext context)
    {
        if (!exception.Errors.Any())
        {
            context.Result = new BadRequestObjectResult(exception.Message);
            return;
        }

        new ValidationResult(exception.Errors).AddToModelState(context.ModelState, null);
        context.Result = new BadRequestObjectResult(context.ModelState);
    }
}