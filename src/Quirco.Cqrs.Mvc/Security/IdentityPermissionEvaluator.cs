using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Quirco.Cqrs.Domain.Security;
using Quirco.Security.Principal;

namespace Quirco.Cqrs.Mvc.Security;

public abstract class IdentityPermissionEvaluator<TEntity> : IPermissionEvaluator<TEntity>
    where TEntity : class
{
    private readonly IAuthorizationService _authorizationService;
    private readonly IPrincipalAccessor _principalAccessor;

    protected IdentityPermissionEvaluator(
        IAuthorizationService authorizationService,
        IPrincipalAccessor principalAccessor)
    {
        _authorizationService = authorizationService;
        _principalAccessor = principalAccessor;
    }

    protected abstract string ReadPermission { get; }
    protected abstract string AddPermission { get; }
    protected abstract string EditPermission { get; }
    protected abstract string DeletePermission { get; }

    protected virtual async Task<bool> Authorize(TEntity entity, string permission, CancellationToken cancellationToken)
    {
        var principal = await _principalAccessor.GetPrincipal();
        if (principal is ClaimsPrincipal claimsPrincipal)
        {
            return (await _authorizationService.AuthorizeAsync(claimsPrincipal, permission)).Succeeded;
        }

        return false;
    }

    public Task<bool> CanRead(TEntity entity, CancellationToken cancellationToken) => Authorize(entity, ReadPermission, cancellationToken);

    public Task<bool> CanAdd(TEntity entity, CancellationToken cancellationToken) => Authorize(entity, AddPermission, cancellationToken);

    public Task<bool> CanUpdate(TEntity entity, CancellationToken cancellationToken) => Authorize(entity, EditPermission, cancellationToken);

    public Task<bool> CanDelete(TEntity entity, CancellationToken cancellationToken) => Authorize(entity, DeletePermission, cancellationToken);
}