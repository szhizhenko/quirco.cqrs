using Microsoft.AspNetCore.Routing;

namespace Quirco.Cqrs.Mvc.Routes;

/// <summary>
/// Generates URLs in format like this '/subscription-management/get-all'.
/// </summary>
public class SlugifyParameterTransformer : IOutboundParameterTransformer
{
    /// <inheritdoc />
    public string TransformOutbound(object value)
    {
        if (value == null) { return null; }

        return string.Concat(value.ToString().Select((x, i) => i > 0 && char.IsUpper(x) ? "-" + x : x.ToString())).ToLowerInvariant();
    }
}