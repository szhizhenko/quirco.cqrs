using System.Text.Json;
using Quirco.Cqrs.Infrastructure.Extensions;

namespace Quirco.Cqrs.Mvc.Json;

public class SnakeCaseNamingPolicy : JsonNamingPolicy
{
    public override string ConvertName(string name)
    {
        return name.ToSnakeCase();
    }
}