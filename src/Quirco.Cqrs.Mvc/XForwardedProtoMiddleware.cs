﻿using Microsoft.AspNetCore.Http;

namespace Quirco.Cqrs.Mvc;

/// <summary>
/// Добавляет заголовок X-Forwarded-Proto в запрос.
/// </summary>
public class XForwardedProtoMiddleware : IMiddleware
{
    private const string Header = "X-Forwarded-Proto";

    /// <inheritdoc />
    public Task InvokeAsync(HttpContext context, RequestDelegate next)
    {
        context.Request.Headers[Header] = Uri.UriSchemeHttps;
        return next(context);
    }
}