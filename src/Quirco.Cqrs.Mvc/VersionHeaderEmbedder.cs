using Microsoft.AspNetCore.Builder;

namespace Quirco.Cqrs.Mvc;

public static class VersionHeaderEmbedder
{
    public static void UseVersionHeaderEmbedder(this IApplicationBuilder app)
    {
        app.Use((context, next) =>
        {
            if (!string.IsNullOrEmpty(BuildDate.Value))
            {
                context.Response.Headers.Add("x-build-date", BuildDate.Value);
            }

            if (!string.IsNullOrEmpty(CommitSha.Value))
            {
                context.Response.Headers.Add("x-commit-sha", CommitSha.Value);
            }

            return next.Invoke();
        });
    }

    private static Lazy<string> BuildDate = new(Environment.GetEnvironmentVariable("BUILD_DATE"));
    private static Lazy<string> CommitSha = new(Environment.GetEnvironmentVariable("COMMIT_SHA"));
}