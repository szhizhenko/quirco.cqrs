using MediatR;
using Microsoft.AspNetCore.Mvc;
using Quirco.Cqrs.Core.Models;
using Quirco.Cqrs.DataAccess.Models;
using Quirco.Cqrs.Domain.Commands;
using Quirco.Cqrs.Domain.Queries;

namespace Quirco.Cqrs.Mvc.Controllers;

[ApiController, Route("api/[controller]")]
public abstract class CrudController
<
    TEntity,
    TCreateUpdateModel,
    TSlimModel,
    TDetailedModel,
    TListQuery
> : CrudController
<
    TEntity,
    TCreateUpdateModel,
    TSlimModel,
    TDetailedModel,
    TListQuery,
    int
>
    where TEntity : class, IHasId<int>
    where TCreateUpdateModel : class
    where TSlimModel : IHasId<int>
    where TDetailedModel : IHasId<int>
    where TListQuery : IRequest<PagedResponse<TSlimModel>>
{
    protected CrudController(IMediator mediator) : base(mediator)
    {
    }

    protected abstract override GetEntityByIdQuery<TDetailedModel, int> BuildGetByIdQuery(in int id);
}

/// <summary>
/// CQRS CRUD controller.
/// </summary>
/// <typeparam name="TEntity">Сущность в БД</typeparam>
/// <typeparam name="TCreateUpdateModel">Модель для создания/обновления сущности</typeparam>
/// <typeparam name="TSlimModel">Упрощённая модель для запроса на получение списка элементов</typeparam>
/// <typeparam name="TDetailedModel">Детализированная модель для запроса на получение элемента по id</typeparam>
/// <typeparam name="TListQuery">Запрос на поиск списка</typeparam>
/// <typeparam name="TId">Тип идентификатора</typeparam>
[ApiController, Route("api/[controller]")]
public abstract class CrudController
<
    TEntity,
    TCreateUpdateModel,
    TSlimModel,
    TDetailedModel,
    TListQuery,
    TId
> : CrudController<
    TEntity,
    TCreateUpdateModel,
    TCreateUpdateModel,
    TSlimModel,
    TDetailedModel,
    TListQuery,
    TId
>
    where TEntity : class, IHasId<TId>
    where TCreateUpdateModel : class
    where TId : struct
    where TSlimModel : IHasId<TId>
    where TDetailedModel : IHasId<TId>
    where TListQuery : IRequest<PagedResponse<TSlimModel>>
{
    protected abstract CreateUpdateCommand<TId, TCreateUpdateModel> BuildCreateUpdateCommand(TId? id, TCreateUpdateModel model);

    protected CrudController(IMediator mediator) : base(mediator)
    {
    }

    protected override IRequest<TId> BuildCreateCommand(TCreateUpdateModel model)
    {
        return BuildCreateUpdateCommand(null, model);
    }

    protected override IRequest<TId> BuildUpdateCommand(TId id, TCreateUpdateModel model)
    {
        return BuildCreateUpdateCommand(id, model);
    }
}

/// <summary>
/// CQRS CRUD controller.
/// </summary>
/// <typeparam name="TEntity">Сущность в БД</typeparam>
/// <typeparam name="TCreateModel">Модель для создания сущности</typeparam>
/// <typeparam name="TUpdateModel">Модель для обновления сущности</typeparam>
/// <typeparam name="TSlimModel">Упрощённая модель для запроса на получение списка элементов</typeparam>
/// <typeparam name="TDetailedModel">Детализированная модель для запроса на получение элемента по id</typeparam>
/// <typeparam name="TListQuery">Запрос на поиск списка</typeparam>
/// <typeparam name="TId">Тип идентификатора</typeparam>
[ApiController, Route("api/[controller]")]
public abstract class CrudController
<
    TEntity,
    TCreateModel,
    TUpdateModel,
    TSlimModel,
    TDetailedModel,
    TListQuery,
    TId
> : Controller
    where TEntity : class, IHasId<TId>
    where TCreateModel : class
    where TUpdateModel : class
    where TId : struct
    where TSlimModel : IHasId<TId>
    where TDetailedModel : IHasId<TId>
    where TListQuery : IRequest<PagedResponse<TSlimModel>>
{
    protected readonly IMediator Mediator;

    /// <summary>
    /// Initializes a new instance of <see cref="CrudController{TEntity,TModel,TSlimModel,TDetailedModel,TListQuery,TId}"/>.
    /// </summary>
    /// <param name="mediator"></param>
    protected CrudController(IMediator mediator)
    {
        Mediator = mediator;
    }

    /// <summary>
    /// Создание сущности.
    /// </summary>
    /// <param name="model">Информация о сущности.</param>
    /// <param name="cancellationToken">Токен для отмены операции.</param>
    /// <returns></returns>
    [HttpPost]
    public virtual async Task<IActionResult> CreateAsync([FromBody] TCreateModel model, CancellationToken cancellationToken)
    {
        var id = await Mediator.Send(BuildCreateCommand(model), cancellationToken);
        return CreatedAtAction(
            // ReSharper disable once Mvc.ActionNotResolved
            "Get",
            new { id },
            await Mediator.Send(BuildGetByIdQuery(id), cancellationToken));
    }

    /// <summary>
    /// Обновление сущности.
    /// </summary>
    /// <param name="id"></param>
    /// <param name="model">Информация о сущности.</param>
    /// <param name="cancellationToken">Токен для отмены операции.</param>
    /// <returns></returns>
    [HttpPut("{id}")]
    public virtual async Task<IActionResult> UpdateAsync(TId id, [FromBody] TUpdateModel model, CancellationToken cancellationToken)
    {
        await Mediator.Send(BuildUpdateCommand(id, model), cancellationToken);
        return Ok(await Mediator.Send(BuildGetByIdQuery(id), cancellationToken));
    }

    /// <summary>
    /// Удаление сущности.
    /// </summary>
    /// <param name="id">Идентификатор сущности</param>
    /// <param name="cancellationToken">Токен для отмены операции</param>
    [HttpDelete("{id}")]
    public virtual async Task<IActionResult> DeleteAsync([FromRoute] TId id, CancellationToken cancellationToken)
    {
        await Mediator.Send(new DeleteCommand<TEntity, TId>(id), cancellationToken);
        return NoContent();
    }

    /// <summary>
    /// Получение детализированной информации о сущности.
    /// </summary>
    /// <param name="id">Идентификатор сущности</param>
    [HttpGet("{id}")]
    public virtual async Task<IActionResult> GetAsync([FromRoute] TId id, CancellationToken cancellationToken)
    {
        return Ok(await Mediator.Send(BuildGetByIdQuery(id), cancellationToken));
    }

    /// <summary>
    /// Получение списка элементов.
    /// </summary>
    /// <param name="query">Запрос.</param>
    /// <param name="cancellationToken">Токен для отмены операции.</param>
    [HttpGet]
    public virtual async Task<IActionResult> ListAsync([FromQuery] TListQuery query, CancellationToken cancellationToken)
    {
        return Ok(await Mediator.Send(query, cancellationToken));
    }

    /// <summary>
    /// Фабрика для построения запроса на получение модели по идентификатору
    /// </summary>
    /// <param name="id">Идентификатор сущности</param>
    protected abstract GetEntityByIdQuery<TDetailedModel, TId> BuildGetByIdQuery(in TId id);

    protected abstract IRequest<TId> BuildCreateCommand(TCreateModel model);

    protected abstract IRequest<TId> BuildUpdateCommand(TId id, TUpdateModel model);
}