﻿using Microsoft.EntityFrameworkCore;
using Quirco.Cqrs.Demo.DataAccess;
using Quirco.Cqrs.Demo.DataAccess.Models;

namespace Quirco.Cqrs.Demo;

public static class Extensions
{
    public static IApplicationBuilder InitDatabase(this IApplicationBuilder app)
    {
        var ctxFactory = app.ApplicationServices.GetRequiredService<IDbContextFactory<DemoDbContext>>();
        using var ctx = ctxFactory.CreateDbContext();
        ctx.Courses.Add(
            new Course
            {
                Name = "1st course",
                Students = new List<Student>
                {
                    new()
                    {
                        Name = "John Brown",
                        ApplicationDate = new DateOnly(2020, 1, 1)
                    },
                    new()
                    {
                        Name = "Mike Adams",
                        ApplicationDate = new DateOnly(2020, 3, 1)
                    },
                    new()
                    {
                        Name = "Olivia Stone",
                        ApplicationDate = new DateOnly(2022, 1, 1)
                    }
                }
            });

        ctx.Courses.Add(
            new Course
            {
                Name = "2nd course",
                Students = new List<Student>
                {
                    new()
                    {
                        Name = "Kevin Spacey",
                        ApplicationDate = new DateOnly(2019, 1, 1)
                    },
                    new()
                    {
                        Name = "Penelopa Cruz",
                        ApplicationDate = new DateOnly(2020, 2, 1)
                    },
                    new()
                    {
                        Name = "Bill Murray",
                        ApplicationDate = new DateOnly(2021, 1, 1)
                    }
                }
            });

        ctx.Courses.Add(
            new Course
            {
                Name = "3rd course",
                Students = new List<Student>
                {
                    new()
                    {
                        Name = "Quentin Tarantino",
                        ApplicationDate = new DateOnly(2022, 1, 1)
                    },
                    new()
                    {
                        Name = "Tinto Brass",
                        ApplicationDate = new DateOnly(2021, 1, 1)
                    },
                    new()
                    {
                        Name = "Monica Belucci",
                        ApplicationDate = new DateOnly(2023, 1, 1)
                    },
                    new()
                    {
                        Name = "Angelina Jolie",
                        ApplicationDate = new DateOnly(2018, 1, 1)
                    },
                    new()
                    {
                        Name = "Sponge Bob",
                        ApplicationDate = new DateOnly(2017, 1, 1)
                    },
                    new()
                    {
                        Name = "Rassel Krow",
                        ApplicationDate = new DateOnly(2021, 1, 1)
                    },
                    new()
                    {
                        Name = "Marylin Brando",
                        ApplicationDate = new DateOnly(2021, 5, 1)
                    }
                }
            });

        ctx.Courses.Add(
            new Course
            {
                Name = "4rd course (BIG)",
                Students = Enumerable.Range(1, 50)
                    .Select(
                        x => new Student
                        {
                            Name = x.ToString(),
                            ApplicationDate = DateOnly.FromDateTime(DateTime.Today)
                        })
                    .ToArray()
            });
        ctx.SaveChanges();

        return app;
    }
}