﻿using MediatR;
using Nest;
using Quirco.Cqrs.Demo.DataAccess;
using Quirco.Cqrs.Demo.Domain.Courses;
using Quirco.Cqrs.Domain.Models;
using Quirco.Cqrs.Elasticsearch.Behaviors;
using Quirco.Cqrs.Elasticsearch.Commands.IndexAll;

namespace Quirco.Cqrs.Demo.Elasticsearch;

public static class IndexAllStudents
{
    public class Command : IndexAllCommand
    {
    }

    public class CommandHandler : IndexAllCommandHandler<
        Command,
        SearchStudents.Model,
        DemoDbContext,
        int
        >
    {
        public CommandHandler(
            DemoDbContext context,
            IMediator mediator,
            IElasticClient elasticClient,
            SearchPipelineContext searchContext,
            ILogger<CommandHandler> logger)
            : base(context, mediator, elasticClient, searchContext, logger)
        {
        }

        protected override ListQuery<SearchStudents.Model, int> BuildQuery()
        {
            return new SearchStudents.Query();
        }
    }
}