﻿using System.Reflection;
using Mapster;

namespace Quirco.Cqrs.Demo.Domain.Models;

public class Codegen : ICodeGenerationRegister
{
    public void Register(CodeGenerationConfig config)
    {
        config.AdaptTo("[name]Dto")
            .ForAllTypesInNamespace(Assembly.GetExecutingAssembly(), "Quirco.Cqrs.Demo.DataAccess.Models");
    }
}