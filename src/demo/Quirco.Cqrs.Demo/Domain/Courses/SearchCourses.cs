﻿using System.ComponentModel;
using Microsoft.EntityFrameworkCore;
using Quirco.Cqrs.Demo.DataAccess;
using Quirco.Cqrs.Demo.DataAccess.Models;
using Quirco.Cqrs.Domain.Filters;
using Quirco.Cqrs.Domain.Queries;
using IMapper = Quirco.Cqrs.Core.Mapping.IMapper;

namespace Quirco.Cqrs.Demo.Domain.Courses;

public static class SearchCourses
{
    public class Model
    {
        [DisplayName("#")]
        public int Id { get; set; }

        [DisplayName("Course name")]
        public string Name { get; set; }

        [DisplayName("Count of students")]
        public int StudentsCount { get; set; }
    }

    public class Query : SearchQuery<Model>
    {
        public int SomeParameterThatShouldBeAlwaysOne { get; set; }

        public int SomeChangeableParameterWithDefaultTwo { get; set; }
    }

    public class Handler : ListQueryHandler<Model, Course, DemoDbContext, Query>
    {
        public Handler(IMapper mapper, IDbContextFactory<DemoDbContext> context, IEnumerable<IQueryableModifier<Course>>? modifiers) : base(mapper, context, modifiers)
        {
        }

        protected override async Task<IQueryable<Course>> BuildQuery(DemoDbContext context, Query request, CancellationToken cancellationToken)
        {
            var query = await base.BuildQuery(context, request, cancellationToken);
            if (!string.IsNullOrEmpty(request.Query))
                query = query.Where(s => s.Name.ToLowerInvariant().Contains(request.Query.ToLowerInvariant()));
            return query;
        }
    }
}