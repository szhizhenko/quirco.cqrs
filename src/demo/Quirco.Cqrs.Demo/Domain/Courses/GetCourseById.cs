using Quirco.Cqrs.Demo.DataAccess;
using Quirco.Cqrs.Demo.DataAccess.Models;
using Quirco.Cqrs.Domain.Filters;
using Quirco.Cqrs.Domain.Queries;
using IMapper = Quirco.Cqrs.Core.Mapping.IMapper;

namespace Quirco.Cqrs.Demo.Domain.Courses;

public class GetCourseById
{
    /// <summary>
    /// Расширенная модель <see cref="Order"/>
    /// </summary>
    public class DetailedModel
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }

    /// <summary>
    /// Запрос поиска по идентификатору <see cref="DetailedModel"/>
    /// </summary>
    public class Query : GetEntityByIdQuery<DetailedModel>
    {
        public Query(int id) : base(id)
        {
        }
    }

    /// <summary>
    /// Обработчик запроса поиска по идентификатору <see cref="Query"/>
    /// </summary>
    public class QueryHandler : GetEntityByIdQueryHandler<Query, DetailedModel, Course, DemoDbContext>
    {
        public QueryHandler(
            IMapper mapper,
            DemoDbContext context,
            IEnumerable<IQueryableModifier<Course>> modifiers) : base(mapper, context, modifiers)
        {
        }
    }
}