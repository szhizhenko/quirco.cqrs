using System.ComponentModel;
using FluentValidation;
using Microsoft.EntityFrameworkCore;
using Quirco.Cqrs.Auditor;
using Quirco.Cqrs.Auditor.Commands;
using Quirco.Cqrs.Auditor.Core.Interfaces;
using Quirco.Cqrs.Demo.DataAccess;
using Quirco.Cqrs.Demo.DataAccess.Models;
using Quirco.Cqrs.Domain.Commands;
using Quirco.Cqrs.Domain.Security;
using IMapper = Quirco.Cqrs.Core.Mapping.IMapper;

namespace Quirco.Cqrs.Demo.Domain.Courses;

public static class CreateUpdateCourse
{
    public class Model
    {
        [DisplayName("Name")]
        public string Name { get; set; }
    }

    public class Command : CreateUpdateCommand<Model>, IAuditableCommand
    {
        public Command(Model model)
            : base(model)
        {
        }

        public Command(int? id, Model model)
            : base(id, model)
        {
        }

        public string EventType => "Course.Edit";

        public bool TrackProgress => false;
    }

    public class CommandHandler : CreateUpdateCommandHandler<Command, DemoDbContext, Model, Course>
    {
        public CommandHandler(DemoDbContext context, IMapper mapper, IEnumerable<IPermissionEvaluator<Course>> evaluators) : base(context, mapper, evaluators)
        {
        }
    }

    public class CommandValidator : AbstractValidator<Command>
    {
        public CommandValidator()
        {
            RuleFor(x => x.Model).SetValidator(new Validator());
        }
    }

    public class Validator : AbstractValidator<Model>
    {
        public Validator()
        {
            RuleFor(x => x.Name)
                .NotEmpty()
                .MaximumLength(256);
        }
    }

    public class Auditor : CommandProcessingAuditor<Command, int>
    {
        private readonly DemoDbContext _context;
        private string? _oldName;

        public Auditor(DemoDbContext context)
        {
            _context = context;
        }

        public override async ValueTask PreProcess(AuditScope auditScope, Command command, CancellationToken cancellationToken)
        {
            _oldName = await _context.Courses.Where(c => c.Id == command.Id).Select(c => c.Name).FirstOrDefaultAsync(cancellationToken);
            await base.PreProcess(auditScope, command, cancellationToken);
        }

        public override async ValueTask PostProcess(
            AuditScope auditScope,
            Command command,
            int response,
            CancellationToken cancellationToken)
        {
            if (command.Id is not null || command.Id == 0)
            {
                auditScope.AddEntityEvent<Course, int>(command.Id.Value, c => c.PropertyChanged(nameof(Course.Name), _oldName, command.Model.Name));
            }
            else
            {
                auditScope.AddEntityEvent<Course, int>(response, "Created", c => c.PropertyChanged(nameof(Course.Name), null, command.Model.Name));
            }

            await base.PostProcess(auditScope,
                                   command,
                                   response,
                                   cancellationToken);
        }
    }
}