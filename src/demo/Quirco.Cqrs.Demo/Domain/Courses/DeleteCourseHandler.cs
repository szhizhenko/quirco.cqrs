using Quirco.Cqrs.Demo.DataAccess;
using Quirco.Cqrs.Demo.DataAccess.Models;
using Quirco.Cqrs.Domain.Commands;
using Quirco.Cqrs.Domain.Security;
using Quirco.Security.Principal;

namespace Quirco.Cqrs.Demo.Domain.Courses;

public class DeleteCourseHandler : DeleteCommandHandler<DemoDbContext, Course>
{
    public DeleteCourseHandler(
        DemoDbContext context,
        IEnumerable<IPermissionEvaluator<Course>>? evaluators,
        IPrincipalAccessor principal) : base(context, evaluators, principal)
    {
    }
}