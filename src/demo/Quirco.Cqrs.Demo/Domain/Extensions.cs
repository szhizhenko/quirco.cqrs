﻿using Microsoft.EntityFrameworkCore;

using Quirco.Cqrs.Demo.DataAccess;
using Quirco.Cqrs.Mapster;

namespace Quirco.Cqrs.Demo.Domain;

public static class Extensions
{
    public static IServiceCollection AddDemo(this IServiceCollection services)
    {
        services.AddMediatR(cfg => cfg.RegisterServicesFromAssembly(typeof(Extensions).Assembly));
        services.AddMapsterAdapter(typeof(Extensions).Assembly);
        services.AddDbContextFactory<DemoDbContext>((_, c) => { c.UseInMemoryDatabase("Quirco.Cqrs.Demo"); });

        return services;
    }
}