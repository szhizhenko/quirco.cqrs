﻿using System.ComponentModel;
using Microsoft.EntityFrameworkCore;
using Quirco.Cqrs.Core.Mapping;
using Quirco.Cqrs.DataAccess.Models;
using Quirco.Cqrs.Demo.DataAccess;
using Quirco.Cqrs.Demo.DataAccess.Models;
using Quirco.Cqrs.Domain.Filters;
using Quirco.Cqrs.Domain.Queries;

namespace Quirco.Cqrs.Demo.Domain.Courses;

public static class SearchStudents
{
    public class Model : IHasId
    {
        [DisplayName("#")]
        public int Id { get; set; }

        [DisplayName("Student name")]
        public string Name { get; set; }

        [DisplayName("Course")]
        public SearchCourses.Model Course { get; set; }

        [DisplayName("User created")]
        public int UserCreatedId { get; set; }

        [DisplayName("Application date")]
        public DateOnly ApplicationDate { get; set; }
    }

    public class Query : SearchQuery<Model>
    {
        public int[]? CourseIds { get; set; }

        public DateOnly? ApplicationDateFrom { get; set; }
    }

    public class Handler : ListQueryHandler<Model, Student, DemoDbContext, Query>
    {
        public Handler(IMapper mapper, IDbContextFactory<DemoDbContext> context, IEnumerable<IQueryableModifier<Student>>? modifiers)
            : base(mapper, context, modifiers)
        {
        }

        protected override async Task<IQueryable<Student>> BuildQuery(DemoDbContext context, Query request, CancellationToken cancellationToken)
        {
            var query = await base.BuildQuery(context, request, cancellationToken);
            if (!string.IsNullOrEmpty(request.Query))
                query = query.Where(s => s.Name.ToLowerInvariant().Contains(request.Query.ToLowerInvariant()));
            if (request.CourseIds is not null)
                query = query.Where(s => request.CourseIds.Contains(s.CourseId));
            if (request.ApplicationDateFrom is not null)
                query = query.Where(s => s.ApplicationDate >= request.ApplicationDateFrom);
            return query;
        }
    }
}