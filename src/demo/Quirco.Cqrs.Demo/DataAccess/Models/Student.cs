﻿using Quirco.Cqrs.DataAccess.Models;

namespace Quirco.Cqrs.Demo.DataAccess.Models;

public abstract class Person : Entity, IHasDateDeleted, IHasDateCreated, IHasUserCreated
{
    public string Name { get; set; }

    public DateTime? DateDeleted { get; set; }

    public DateTime DateCreated { get; set; }

    public int UserCreatedId { get; set; }

    public string[]? Tags { get; set; }
}

public class Student : Person
{
    public int CourseId { get; set; }

    public virtual Course Course { get; set; }

    public DateOnly ApplicationDate { get; set; }
}

public enum Rating
{
    Stars1,
    Stars2,
    Stars3,
    Stars4,
    Stars5
}