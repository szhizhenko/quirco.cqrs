﻿using System.ComponentModel.DataAnnotations;
using Quirco.Cqrs.DataAccess.Models;

namespace Quirco.Cqrs.Demo.DataAccess.Models;

public class Course : Entity
{
    [MaxLength(128)]
    public string Name { get; set; }

    public virtual ICollection<Student> Students { get; set; }
}