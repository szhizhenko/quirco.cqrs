﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Quirco.Cqrs.Auditor.DataAccess.DbContextAuditor;
using Quirco.Cqrs.DataAccess.DataContext;
using Quirco.Cqrs.DataAccess.Extensions;
using Quirco.Cqrs.Demo.DataAccess.Models;
using Quirco.Security;
using Quirco.Security.Extensions;
using Quirco.Security.Principal;

namespace Quirco.Cqrs.Demo.DataAccess;

public class DemoDbContext : DbContext
{
    private readonly IMediator _mediator;
    private readonly IServiceScopeFactory _serviceScopeFactory;

    public DbSet<Course> Courses { get; set; }

    public DbSet<Student> Students { get; set; }

    public DemoDbContext(
        IMediator mediator,
        DbContextOptions<DemoDbContext> options,
        IServiceScopeFactory serviceScopeFactory) : base(options)
    {
        _mediator = mediator;
        _serviceScopeFactory = serviceScopeFactory;
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);
        modelBuilder.EnumsAsString();
        modelBuilder.EnableSoftDeletionQueryFilters();

        // Example for use Database in PersonConfiguration
        modelBuilder.ApplyConfigurationsFromAssemblies(Database,
                                                       GetType().Assembly,
                                                       typeof(DemoDbContext).Assembly);
    }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        base.OnConfiguring(optionsBuilder);

        optionsBuilder.AddInterceptors(
            new DbContextAuditorInterceptor<int>(_mediator, new HashSet<Type>(), TryGetUserId));
    }

    private bool TryGetUserId(out int userId)
    {
        var user = Task.Run(async () =>
            {
                await using var scope = _serviceScopeFactory.CreateAsyncScope();
                var principalAccessor = scope.ServiceProvider.GetRequiredService<IPrincipalAccessor>();

                return await principalAccessor.GetPrincipal();
            })
            .GetAwaiter()
            .GetResult();

        if (user.TryGetClaimValue<int>(Constants.ClaimTypes.UserId, out var val))
        {
            userId = val;

            return true;
        }

        userId = default;

        return false;
    }
}