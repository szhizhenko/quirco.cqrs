﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Quirco.Cqrs.Demo.DataAccess.Models;

namespace Quirco.Cqrs.Demo.DataAccess.EntityConfigurations;

public class PersonConfiguration : IEntityTypeConfiguration<Person>
{
    private readonly DatabaseFacade _databaseFacade;

    public PersonConfiguration(DatabaseFacade databaseFacade)
    {
        _databaseFacade = databaseFacade;
    }

    public void Configure(EntityTypeBuilder<Person> builder)
    {
        if (!_databaseFacade.IsNpgsql())
        {
            builder.Property(p => p.Tags)
                .HasConversion(
                    v => string.Join(",", v!),
                    v => v.Split(',', StringSplitOptions.RemoveEmptyEntries));
        }
    }
}