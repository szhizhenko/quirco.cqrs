using System;

namespace Quirco.Cqrs.Demo.Models
{
    public partial class PersonDto
    {
        public string Name { get; set; }
        public DateTime? DateDeleted { get; set; }
        public DateTime DateCreated { get; set; }
        public int UserCreatedId { get; set; }
        public string[] Tags { get; set; }
        public int Id { get; set; }
    }
}