using System;
using Quirco.Cqrs.Demo.Models;

namespace Quirco.Cqrs.Demo.Models
{
    public partial class StudentDto
    {
        public int CourseId { get; set; }
        public CourseDto Course { get; set; }
        public DateOnly ApplicationDate { get; set; }
        public string Name { get; set; }
        public DateTime? DateDeleted { get; set; }
        public DateTime DateCreated { get; set; }
        public int UserCreatedId { get; set; }
        public string[] Tags { get; set; }
        public int Id { get; set; }
    }
}