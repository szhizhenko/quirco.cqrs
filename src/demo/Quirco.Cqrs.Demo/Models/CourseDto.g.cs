using System.Collections.Generic;
using Quirco.Cqrs.Demo.Models;

namespace Quirco.Cqrs.Demo.Models
{
    public partial class CourseDto
    {
        public string Name { get; set; }
        public ICollection<StudentDto> Students { get; set; }
        public int Id { get; set; }
    }
}