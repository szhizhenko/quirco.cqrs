using System.Security.Claims;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Nest;
using Quirco.Cqrs.Auditor;
using Quirco.Cqrs.Auditor.Interfaces;
using Quirco.Cqrs.Blazor;
using Quirco.Cqrs.Demo;
using Quirco.Cqrs.Demo.Domain;
using Quirco.Cqrs.Demo.Domain.Courses;
using Quirco.Cqrs.Elasticsearch;
using Quirco.Cqrs.Infrastructure.DateTime;
using Quirco.Cqrs.Infrastructure.DayOff;
using Quirco.Cqrs.Infrastructure.Logging;
using Quirco.Cqrs.Tests.Principal;
using Quirco.Security.Principal;
using Serilog;

var builder = WebApplication.CreateBuilder(args);

builder.Host
    .ConfigureLogging((c, l) => { l.AddConfiguration(c.Configuration); })
    .UseSerilog((_, lc) =>
    {
        lc.ReadFrom.Configuration(builder.Configuration);
        lc.WriteTo.Console();
        lc.MinimumLevel.Debug();
    });

// Add services to the container.
builder.Services.AddRazorPages();
builder.Services.AddServerSideBlazor();
builder.Services.AddAntDesign();
builder.Services.AddQuircoBlazor();
builder.Services.AddDemo();
builder.Services.AddElasticsearch(new ConnectionSettings());
builder.Services.AddSingleton<IPrincipalAccessor>(new TestPrincipalAccessor(new ClaimsPrincipal()));
builder.Services.AddDayOffService();
builder.Services.AddHttpContextAccessor();
builder.Services.AddTransient<ICurrentDateTimeProvider, CurrentSystemUtcDateTimeProvider>();

builder.Services.AddAuditor(cfg => cfg.UseInMemoryDatabase("audit"));
builder.Services.AddTransient<ICommandProcessingAuditor<CreateUpdateCourse.Command, int>, CreateUpdateCourse.Auditor>();

builder.Services.AddTransient(typeof(IPipelineBehavior<,>), typeof(LoggingBehavior<,>));

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Error");

    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.InitDatabase();

app.UseHttpsRedirection();

app.UseStaticFiles();

app.UseRouting();

app.MapBlazorHub();
app.MapFallbackToPage("/_Host");

app.Run();