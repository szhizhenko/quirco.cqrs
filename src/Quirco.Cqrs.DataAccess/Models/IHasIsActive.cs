﻿namespace Quirco.Cqrs.DataAccess.Models;

public interface IHasIsActive
{
    bool IsActive { get; set; }
}

public interface IHasIsActiveNullable
{
    bool? IsActive { get; set; }
}