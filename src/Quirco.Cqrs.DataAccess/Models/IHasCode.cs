namespace Quirco.Cqrs.DataAccess.Models;

public interface IHasCode
{
    string Code { get; }
}