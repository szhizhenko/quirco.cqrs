﻿namespace Quirco.Cqrs.DataAccess.Models;

public interface IHasDateModified
{
    DateTime DateModified { get; set; }
}