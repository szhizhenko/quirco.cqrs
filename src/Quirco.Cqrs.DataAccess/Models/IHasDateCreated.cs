﻿namespace Quirco.Cqrs.DataAccess.Models;

public interface IHasDateCreated
{
    DateTime DateCreated { get; set; }
}