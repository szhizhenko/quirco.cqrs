﻿namespace Quirco.Cqrs.DataAccess.Models;

/// <summary>
/// Used to represent parent/child related entities
/// </summary>
/// <typeparam name="TId">Type of node identifier</typeparam>
/// <typeparam name="TEntity">Concrete type of node</typeparam>
public interface ITreeEntity<TId, TEntity> : IHasId<TId>
    where TEntity : ITreeEntity<TId, TEntity>
    where TId : struct
{
    /// <summary>
    /// Path to current node in form of "1.2.3", where 1,2,3 are the identifiers to traverse from root (1) to current (3) node.
    /// </summary>
    string MaterializedPath { get; set; }

    /// <summary>
    /// Id of parent node or null if node is null
    /// </summary>
    TId? ParentId { get; set; }

    /// <summary>
    /// Parent instance or null
    /// </summary>
    TEntity? Parent { get; set; }

    /// <summary>
    /// Children instances or null
    /// </summary>
    ICollection<TEntity>? Children { get; set; }
}