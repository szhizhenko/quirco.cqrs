﻿namespace Quirco.Cqrs.DataAccess.Models;

public interface IHasDraft
{
    bool IsDraft { get; }
}