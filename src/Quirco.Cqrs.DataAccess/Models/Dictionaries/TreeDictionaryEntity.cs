﻿namespace Quirco.Cqrs.DataAccess.Models.Dictionaries;

public abstract class TreeDictionaryEntity<TEntity> : TreeDictionaryEntity<int, TEntity>
    where TEntity : ITreeEntity<int, TEntity>
{
}

/// <summary>
/// An dictionary entity with parent/child relations
/// </summary>
/// <typeparam name="TId">Type of node identifier.</typeparam>
/// <typeparam name="TEntity">Type of concrete node.</typeparam>
public abstract class TreeDictionaryEntity<TId, TEntity> : Entity<TId>, ITreeEntity<TId, TEntity>, IHasDateDeleted, IHasIsActiveNullable
    where TEntity : ITreeEntity<TId, TEntity>
    where TId : struct
{
    /// <summary>
    /// Name of entry
    /// </summary>
    public string Name { get; set; }

    /// <summary>
    /// Order for sorting among others
    /// </summary>
    public decimal SortOrder { get; set; }

    /// <summary>
    /// Used to temporary deactivate items or make them draft (to preview something somewere), can be null on parent nodes. Non null values should propagate into children nodes recursively
    /// </summary>
    public bool? IsActive { get; set; }

    /// <summary>
    /// Date of deletion to support soft deletion
    /// </summary>
    public DateTime? DateDeleted { get; set; }

    /// <summary>
    /// Path to node built of parent's ids: RootId.GrandPId.ParentId.ThisNodeId.
    /// </summary>
    public string MaterializedPath { get; set; }

    /// <inheritdoc />
    public TId? ParentId { get; set; }

    /// <inheritdoc />
    public TEntity? Parent { get; set; }

    /// <inheritdoc />
    public ICollection<TEntity>? Children { get; set; }
}