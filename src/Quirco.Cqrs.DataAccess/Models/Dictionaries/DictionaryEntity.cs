﻿namespace Quirco.Cqrs.DataAccess.Models.Dictionaries;

/// <inheritdoc/>
public abstract class DictionaryEntity : DictionaryEntity<int>, IHasId
{
}

/// <summary>
/// Base class for "dictionary"-kind entities
/// </summary>
/// <typeparam name="TId">Type of identifier</typeparam>
public abstract class DictionaryEntity<TId> : Entity<TId>, IHasDateDeleted, IHasIsActive
{
    /// <summary>
    /// Name of entry
    /// </summary>
    public string Name { get; set; }

    /// <summary>
    /// Order for sorting among others
    /// </summary>
    public decimal SortOrder { get; set; }

    /// <summary>
    /// Used to temporary deactivate items or make them draft (to preview something somewere)
    /// </summary>
    public bool IsActive { get; set; }

    /// <summary>
    /// Date of deletion to support soft deletion
    /// </summary>
    public DateTime? DateDeleted { get; set; }
}