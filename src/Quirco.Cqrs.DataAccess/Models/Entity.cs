using System.ComponentModel.DataAnnotations;

namespace Quirco.Cqrs.DataAccess.Models;

/// <summary>
/// Base class for database entities
/// </summary>
public abstract class Entity : Entity<int>, IHasId
{
}

public abstract class Entity<T> : IHasId<T>
{
    /// <summary>
    /// Primary key
    /// </summary>
    [Key]
    public T Id { get; set; }
}