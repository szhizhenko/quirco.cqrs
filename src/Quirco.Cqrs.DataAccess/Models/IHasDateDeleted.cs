﻿namespace Quirco.Cqrs.DataAccess.Models;

public interface IHasDateDeleted
{
    DateTime? DateDeleted { get; set; }
}