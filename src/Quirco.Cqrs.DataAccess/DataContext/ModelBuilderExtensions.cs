﻿using System.Linq.Expressions;
using System.Reflection;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.Query;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Quirco.Cqrs.DataAccess.Models;

namespace Quirco.Cqrs.DataAccess.DataContext;

/// <summary>
/// Extensions for the <see cref="ModelBuilder"/>.
/// </summary>
public static class ModelBuilderExtensions
{
    /// <summary>
    /// Disable cascade delete on all relations
    /// </summary>
    /// <param name="modelBuilder">Database model builder</param>
    public static ModelBuilder DisableCascadeDeleteGlobally(this ModelBuilder modelBuilder)
    {
        var cascadeForeignKeys = modelBuilder.Model.GetEntityTypes()
            .SelectMany(t => t.GetForeignKeys())
            .Where(fk => !fk.IsOwnership && fk.DeleteBehavior == DeleteBehavior.Cascade);

        foreach (var fk in cascadeForeignKeys)
        {
            fk.DeleteBehavior = DeleteBehavior.Restrict;
        }

        return modelBuilder;
    }

    /// <summary>
    /// Converts all enums to string fields. Enums with Flags attributes are not supported and will not be affected.
    /// </summary>
    /// <param name="modelBuilder">Database model builder</param>
    public static ModelBuilder EnumsAsString(this ModelBuilder modelBuilder)
    {
        foreach (var entityType in modelBuilder.Model.GetEntityTypes())
        {
            foreach (var property in entityType.GetProperties())
            {
                if (property.ClrType.BaseType == typeof(Enum))
                {
                    var attribute = property.ClrType.GetCustomAttribute<FlagsAttribute>();
                    if (attribute == null)
                    {
                        var type = typeof(EnumToStringConverter<>).MakeGenericType(property.ClrType);
                        var converter = Activator.CreateInstance(type) as ValueConverter;
                        property.SetValueConverter(converter);
                    }
                }
            }
        }

        return modelBuilder;
    }

    /// <summary>
    /// Automatically filters out all entities that implements IHasDateDeleted and has non-null value in DateDeleted column
    /// </summary>
    /// <param name="modelBuilder">Database model builder</param>
    public static ModelBuilder EnableSoftDeletionQueryFilters(this ModelBuilder modelBuilder)
    {
        foreach (var entity in modelBuilder.Model.GetEntityTypes().ToList())
        {
            var hasDeletedDateType = entity.ClrType.GetFirstTypeInHierarchyImplementing<IHasDateDeleted>();
            if (hasDeletedDateType == null)
                continue;
            var parameter = Expression.Parameter(hasDeletedDateType, "p");
            var expression = Expression.Equal(
                Expression.Property(parameter, nameof(IHasDateDeleted.DateDeleted)),
                Expression.Constant(null));

            modelBuilder.Entity(hasDeletedDateType).AppendQueryFilter(parameter, expression);
        }

        return modelBuilder;
    }

    /// <summary>
    /// Automatically filters out all entities that implements IHasIsActive and has non-null value in IsActive equals to false
    /// </summary>
    /// <param name="modelBuilder">Database model builder</param>
    public static ModelBuilder EnableIsActiveQueryFilters(this ModelBuilder modelBuilder)
    {
        foreach (var entity in modelBuilder.Model.GetEntityTypes().ToList())
        {
            var hasIsActiveType = entity.ClrType.GetFirstTypeInHierarchyImplementing<IHasIsActive>();
            var hasIsActiveNullableType = entity.ClrType.GetFirstTypeInHierarchyImplementing<IHasIsActiveNullable>();
            if (hasIsActiveType == null && hasIsActiveNullableType == null)
                continue;
            var parameter = Expression.Parameter(hasIsActiveType ?? hasIsActiveNullableType, "p");
            var expression =
                Expression.NotEqual(
                    Expression.Property(
                        parameter,
                        hasIsActiveType != null
                            ? nameof(IHasIsActive.IsActive)
                            : nameof(IHasIsActiveNullable.IsActive)),
                    hasIsActiveType != null
                        ? Expression.Constant(false)
                        : Expression.Constant(false, typeof(bool?)));

            modelBuilder.Entity(hasIsActiveType ?? hasIsActiveNullableType).AppendQueryFilter(parameter, expression);
        }

        return modelBuilder;
    }

    private static Type? GetFirstTypeInHierarchyImplementing<T>(this Type type)
    {
        if (typeof(T).IsAssignableFrom(type) && (type.BaseType == null || type.BaseType.IsAbstract || !typeof(T).IsAssignableFrom(type.BaseType)))
            return type;

        return type.BaseType == null ? null : GetFirstTypeInHierarchyImplementing<T>(type.BaseType);
    }

    public static void AppendQueryFilter(this EntityTypeBuilder entityTypeBuilder, ParameterExpression parameterExpression, Expression expression)
    {
        var expressionFilter = ReplacingExpressionVisitor.Replace(
            parameterExpression, parameterExpression, expression);

        if (entityTypeBuilder.Metadata.GetQueryFilter() != null)
        {
            var currentQueryFilter = entityTypeBuilder.Metadata.GetQueryFilter();
            var currentExpressionFilter = ReplacingExpressionVisitor.Replace(
                currentQueryFilter.Parameters.Single(), parameterExpression, currentQueryFilter.Body);
            if (!currentExpressionFilter.ToString().Equals(expressionFilter.ToString()))
                expressionFilter = Expression.And(currentExpressionFilter, expressionFilter);
        }

        var lambdaExpression = Expression.Lambda(expressionFilter, parameterExpression);
        entityTypeBuilder.HasQueryFilter(lambdaExpression);
    }
}