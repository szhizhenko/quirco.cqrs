using Microsoft.EntityFrameworkCore;
using Quirco.Cqrs.DataAccess.Models;

namespace Quirco.Cqrs.DataAccess.DataContext;

public static class DataContextExtensions
{
    public static async Task<T> GetByCodeAsync<T>(this IQueryable<T> queryable, string code, CancellationToken cancellationToken)
        where T : IHasCode
    {
        return await queryable.FirstOrDefaultAsync(e => e.Code == code, cancellationToken);
    }

    public static T GetByCode<T>(this IQueryable<T> queryable, string code)
        where T : IHasCode
    {
        return queryable.FirstOrDefault(e => e.Code == code);
    }
}