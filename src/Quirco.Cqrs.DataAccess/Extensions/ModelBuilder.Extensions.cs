using System.Reflection;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;

namespace Quirco.Cqrs.DataAccess.Extensions;

public static class ModelBuilderExtensions
{
    /// <summary>
    /// Extension for pass DatabaseFacade in EntityConfigurationType. Need more cases for entity configuration.
    /// For example: configure array field for memory db context in <see cref="IEntityTypeConfiguration{TEntity}"/>
    /// </summary>
    /// <param name="modelBuilder"><see cref="ModelBuilder"/></param>
    /// <param name="databaseFacade">Pass from DbContext.Database</param>
    /// <param name="assemblies">Array of assemblies with configurations</param>
    /// <exception cref="ArgumentNullException">When args is null</exception>
    public static void ApplyConfigurationsFromAssemblies(
        this ModelBuilder modelBuilder,
        DatabaseFacade databaseFacade,
        params Assembly[] assemblies)
    {
        if (modelBuilder == null)
        {
            throw new ArgumentNullException(nameof(modelBuilder));
        }

        foreach (var assembly in assemblies)
        {
            var configurations = assembly
                .GetTypes()
                .Where(t => t is { IsAbstract: false, IsGenericTypeDefinition: false })
                .Where(t => t.GetInterfaces()
                           .Any(i => i.IsGenericType &&
                                     i.GetGenericTypeDefinition() == typeof(IEntityTypeConfiguration<>)))
                .ToList();

            foreach (var configuration in configurations)
            {
                configuration.HasConstructorArguments();

                var entityConfigurationInstance = configuration.HasConstructorArguments()
                    ? Activator.CreateInstance(configuration, databaseFacade)
                    : Activator.CreateInstance(configuration);

                var entityType = configuration.GetInterfaces()
                    .Single(i => i.IsGenericType &&
                                 i.GetGenericTypeDefinition() == typeof(IEntityTypeConfiguration<>))
                    .GetGenericArguments()
                    .First();

                var methodInfo = typeof(ModelBuilder)
                    .GetMethods()
                    .Single(e => e is { Name: nameof(ModelBuilder.ApplyConfiguration), ContainsGenericParameters: true }
                                 && e.GetParameters()
                                     .SingleOrDefault()
                                     ?.ParameterType
                                     .GetGenericTypeDefinition() == typeof(IEntityTypeConfiguration<>));

                methodInfo.MakeGenericMethod(entityType)
                    .Invoke(modelBuilder, new[] { entityConfigurationInstance });
            }
        }
    }

    private static bool HasConstructorArguments(this Type type)
    {
        return type.GetConstructors().Any(x => x.GetParameters().Any());
    }
}