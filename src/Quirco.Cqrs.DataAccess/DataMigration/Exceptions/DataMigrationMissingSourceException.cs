﻿namespace Quirco.Cqrs.DataAccess.DataMigration.Exceptions;

public class DataMigrationMissingSourceException : ApplicationException
{
    public DataMigrationMissingSourceException()
    {
    }

    public DataMigrationMissingSourceException(string? message) : base(message)
    {
    }

    public DataMigrationMissingSourceException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}