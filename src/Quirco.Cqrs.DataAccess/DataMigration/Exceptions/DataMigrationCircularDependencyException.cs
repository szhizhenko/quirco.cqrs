﻿using Quirco.Cqrs.DataAccess.DataMigration.Interfaces;

namespace Quirco.Cqrs.DataAccess.DataMigration.Exceptions;

public class DataMigrationCircularDependencyException : ApplicationException
{
    public MigrationStepNodeInfo[] Path { get; }

    public DataMigrationCircularDependencyException(MigrationStepNodeInfo[] path) : base(
        $"There is circular dependency between migration steps: {string.Join("->", path.Select(p => p.Type.Name))}")
    {
        Path = path;
    }
}