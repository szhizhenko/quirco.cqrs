﻿namespace Quirco.Cqrs.DataAccess.DataMigration.Exceptions;

public class DataMigrationException : ApplicationException
{
    public DataMigrationException()
    {
    }

    public DataMigrationException(string? message) : base(message)
    {
    }

    public DataMigrationException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}