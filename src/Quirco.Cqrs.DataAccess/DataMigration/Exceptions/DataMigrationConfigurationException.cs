﻿namespace Quirco.Cqrs.DataAccess.DataMigration.Exceptions;

public class DataMigrationConfigurationException : ApplicationException
{
    public DataMigrationConfigurationException()
    {
    }

    public DataMigrationConfigurationException(string? message) : base(message)
    {
    }

    public DataMigrationConfigurationException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}