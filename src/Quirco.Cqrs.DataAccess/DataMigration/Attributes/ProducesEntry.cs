﻿namespace Quirco.Cqrs.DataAccess.DataMigration.Attributes;

[AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
public class ProducesEntry : Attribute
{
    public Type Type { get; protected set; }

    public ProducesEntry(Type type)
    {
        Type = type;
    }
}

#if NET7_0_OR_GREATER
[AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
public class ProducesEntry<T> : ProducesEntry
{
    public ProducesEntry() : base(typeof(T))
    {
    }
}
#endif