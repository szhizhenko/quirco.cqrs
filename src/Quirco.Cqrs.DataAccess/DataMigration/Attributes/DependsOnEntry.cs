﻿namespace Quirco.Cqrs.DataAccess.DataMigration.Attributes;

[AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
public class DependsOnEntry : Attribute
{
    public Type Type { get; protected set; }

    public DependsOnEntry(Type type)
    {
        Type = type;
    }
}

#if NET7_0_OR_GREATER
[AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
public class DependsOnEntry<T> : DependsOnEntry
{
    public DependsOnEntry() : base(typeof(T))
    {
    }
}
#endif