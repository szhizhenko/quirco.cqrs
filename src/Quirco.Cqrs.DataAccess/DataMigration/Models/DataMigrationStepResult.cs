﻿namespace Quirco.Cqrs.DataAccess.DataMigration.Models;

/// <summary>
/// Result of migration step execution
/// </summary>
public record DataMigrationStepResult
{
    /// <summary>
    /// Number of records inserted/processed
    /// </summary>
    public int Inserted { get; set; }
}