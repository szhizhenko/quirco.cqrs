﻿using System.Reflection;
using Microsoft.Extensions.DependencyInjection;
using Quirco.Cqrs.DataAccess.DataMigration.Interfaces;

namespace Quirco.Cqrs.DataAccess.DataMigration;

public static class Extensions
{
    /// <summary>
    /// Adds all migration from specified assemblies. Migration steps will be registered in order regarding <see cref="DependsOnAttribute"/>
    /// </summary>
    /// <typeparam name="T">Type of base interface/class of migration step to register</typeparam>
    public static IServiceCollection AddDataMigrationSteps<T>(this IServiceCollection serviceCollection, params Assembly[] assemblies)
    {
        serviceCollection.AddScoped<DataMigrationContext>();

        foreach (var assembly in assemblies)
        {
            var types = assembly.GetTypes().Where(t => t.IsAssignableTo(typeof(T)) && !t.IsAbstract);

            foreach (var type in types.OrderByDependencies())
            {
                serviceCollection.AddScoped(typeof(T), type);
            }
        }

        return serviceCollection;
    }
}