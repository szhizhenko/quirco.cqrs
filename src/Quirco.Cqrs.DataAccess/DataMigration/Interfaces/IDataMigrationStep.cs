﻿using Quirco.Cqrs.DataAccess.DataMigration.Models;

namespace Quirco.Cqrs.DataAccess.DataMigration.Interfaces;

/// <summary>
/// Single step of data migration process
/// </summary>
public interface IDataMigrationStep
{
    /// <summary>
    /// Method to execute migration step
    /// </summary>
    ValueTask<DataMigrationStepResult> Execute(CancellationToken cancellationToken);
}