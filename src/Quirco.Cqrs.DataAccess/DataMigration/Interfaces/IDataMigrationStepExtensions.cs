﻿using Quirco.Cqrs.DataAccess.DataMigration.Exceptions;

namespace Quirco.Cqrs.DataAccess.DataMigration.Interfaces;

public static class IDataMigrationStepExtensions
{
    /// <summary>
    /// Order migration steps by their dependencies
    /// </summary>
    /// <param name="steps">Migration steps</param>
    /// <returns>Ordered migration steps in proper order for execution</returns>
    public static IEnumerable<Type> OrderByDependencies(this IEnumerable<Type> steps)
    {
        var distanceMap = new Dictionary<MigrationStepNodeInfo, int>();

        // Рассчитать "дистанцию" между шагами согласно зависимостям друг от друга
        var stepInfos = steps.Select(s => new MigrationStepNodeInfo(s)).ToList();

        foreach (var stepInfo in stepInfos)
        {
            var visitedHash = new HashSet<MigrationStepNodeInfo>();

            distanceMap[stepInfo] = CalculateDistance(stepInfo,
                                                      stepInfos,
                                                      distanceMap,
                                                      visitedHash,
                                                      new[] { stepInfo });
        }

        return distanceMap.OrderBy(d => d.Value).Select(c => c.Key.Type);
    }

    private static int CalculateDistance(
        MigrationStepNodeInfo migrationStep,
        IEnumerable<MigrationStepNodeInfo> steps,
        IReadOnlyDictionary<MigrationStepNodeInfo, int> distanceMap,
        HashSet<MigrationStepNodeInfo> visitedHash,
        MigrationStepNodeInfo[] path)
    {
        if (distanceMap.TryGetValue(migrationStep, out var dist) && dist > 0)
            return dist;

        if (!migrationStep.DependantTypes.Any())
            return 1;

        if (visitedHash.Contains(migrationStep))
        {
            throw new DataMigrationException($"Migrator of type '{migrationStep.Type.Name}' used with circular dependency");
        }

        visitedHash.Add(migrationStep);

        var st = steps.Where(c => !Equals(c, migrationStep)).ToList();

        var suitableSteps = new HashSet<MigrationStepNodeInfo>();

        foreach (var depType in migrationStep.DependantTypes)
        {
            var producers = st.Where(step => step.ProducedTypes.Any(p => p == depType || p.IsAssignableTo(depType))).ToList();

            if (!producers.Any())
            {
                throw new DataMigrationMissingSourceException($"There are no steps with source with types: `{depType.Name}`. {migrationStep.Type.Name} depends on it.");
            }

            suitableSteps.UnionWith(producers);
        }

        var distance = suitableSteps.Select(a =>
            {
                try
                {
                    return CalculateDistance(a,
                                             steps,
                                             distanceMap,
                                             new HashSet<MigrationStepNodeInfo>(visitedHash),
                                             path.Concat(new[] { a }).ToArray());
                }
                catch (DataMigrationException)
                {
                    throw new DataMigrationCircularDependencyException(path.Concat(new[] { a }).ToArray());
                }
            })
            .Max() + 1;

        return distance;
    }
}