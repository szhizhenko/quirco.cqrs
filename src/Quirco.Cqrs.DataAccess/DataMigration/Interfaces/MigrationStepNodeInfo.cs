﻿using System.Reflection;
using Quirco.Cqrs.DataAccess.DataMigration.Attributes;

namespace Quirco.Cqrs.DataAccess.DataMigration.Interfaces;

public record MigrationStepNodeInfo(Type Type)
{
    public Type[] ProducedTypes { get; } = GetProducedEntries(Type);

    public Type[] DependantTypes { get; } = GetDependantEntries(Type);

    public virtual bool Equals(MigrationStepNodeInfo? other)
    {
        if (ReferenceEquals(null, other))
        {
            return false;
        }

        if (ReferenceEquals(this, other))
        {
            return true;
        }

        return Type.Equals(other.Type);
    }

    public override int GetHashCode()
    {
        return Type.GetHashCode();
    }

    private static Type[] GetProducedEntries(ICustomAttributeProvider type)
    {
        var attrTypes = type.GetCustomAttributes(typeof(ProducesEntry), true).OfType<ProducesEntry>();
        var types = attrTypes?.Select(a => a.Type).ToArray() ?? Array.Empty<Type>();
        return types;
    }

    private static Type[] GetDependantEntries(ICustomAttributeProvider type)
    {
        var attrTypes = type.GetCustomAttributes(typeof(DependsOnEntry), true).OfType<DependsOnEntry>();
        var types = attrTypes?.Select(a => a.Type).ToArray() ?? Array.Empty<Type>();
        return types;
    }
}