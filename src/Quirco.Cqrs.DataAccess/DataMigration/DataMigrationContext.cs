﻿using Microsoft.Extensions.Logging;
using Quirco.Cqrs.DataAccess.DataMigration.Exceptions;
using Quirco.Cqrs.DataAccess.DataMigration.Interfaces;

namespace Quirco.Cqrs.DataAccess.DataMigration;

public class DataMigrationContext
{
    private readonly ILogger<DataMigrationContext> _logger;
    private readonly Dictionary<Type, Dictionary<long, object>> _map = new();
    private readonly Dictionary<Type, Dictionary<string, object>> _stringMap = new();
    private readonly Dictionary<string, object> _customMap = new();

    public DataMigrationContext(ILogger<DataMigrationContext> logger)
    {
        _logger = logger;
    }

    /// <summary>
    /// Adds mapping from old legacy ID to new entity
    /// </summary>
    public DataMigrationContext AddMap<TEntity>(int legacyId, TEntity obj)
        where TEntity : notnull
    {
        return AddMap<TEntity>((long)legacyId, obj);
    }

    /// <summary>
    /// Adds mapping from old legacy ID to new entity
    /// </summary>
    public DataMigrationContext AddMap<TEntity>(long legacyId, TEntity obj)
        where TEntity : notnull
    {
        if (!_map.TryGetValue(typeof(TEntity), out var map))
        {
            map = new Dictionary<long, object>();
            _map[typeof(TEntity)] = map;
        }

        map[legacyId] = obj;

        return this;
    }

    /// <summary>
    /// Add mapping by string
    /// </summary>
    public DataMigrationContext AddMap<TEntity>(string code, TEntity obj)
        where TEntity : notnull
    {
        if (!_stringMap.TryGetValue(typeof(TEntity), out var map))
        {
            map = new Dictionary<string, object>();
            _stringMap[typeof(TEntity)] = map;
        }

        map[code] = obj;

        return this;
    }

    /// <summary>
    /// Returns optional new entity by it's legacy ID
    /// </summary>
    public TEntity? GetFromMap<TEntity>(long? legacyId)
    {
        if (legacyId is null)
            return default;

        if (!_map.TryGetValue(typeof(TEntity), out var map))
            throw new DataMigrationException($"No dictionary with type {typeof(TEntity).Name} has been initialized. Did you forgot 'DependsOn' attribute?");

        if (map.TryGetValue(legacyId.Value, out var entity))
        {
            return (TEntity)entity;
        }

        return default;
    }

    /// <summary>
    /// Returns optional new entity by it's string code
    /// </summary>
    public TEntity? GetFromMap<TEntity>(string? code)
    {
        if (string.IsNullOrEmpty(code))
            return default;

        if (!_stringMap.TryGetValue(typeof(TEntity), out var map))
            throw new DataMigrationException($"No dictionary with type {typeof(TEntity).Name} has been initialized. Did you forgot 'DependsOn' attribute?");

        if (map.TryGetValue(code, out var entity))
        {
            return (TEntity)entity;
        }

        return default;
    }

    /// <summary>
    /// Returns new entity by it's legacy ID. If no entity found, <see cref="DataMigrationException"/> will be thrown.
    /// </summary>
    public TEntity GetFromMapRequired<TEntity>(long legacyId)
    {
        var res = GetFromMap<TEntity>(legacyId);

        if (Equals(res, default(TEntity)))
        {
            throw new DataMigrationException($"No mapping dictionary value found for '{typeof(TEntity).Name}' with legacy id '{legacyId}'");
        }

        return res!;
    }

    /// <summary>
    /// Возвращает все сущности указанного типа из карты соотвествий
    /// </summary>
    /// <typeparam name="TEntity">Тип сущности</typeparam>
    public IEnumerable<TEntity> GetAllEntries<TEntity>()
    {
        if (_map.TryGetValue(typeof(TEntity), out var entries))
            return entries.Values.OfType<TEntity>();

        return Enumerable.Empty<TEntity>();
    }

    /// <summary>
    /// Сохранить произвольное значение в словаре значений
    /// </summary>
    /// <param name="key">Ключ</param>
    /// <param name="value">Значение</param>
    public DataMigrationContext SetCustomValue<T>(string key, T value)
    {
        _customMap[key] = value;

        return this;
    }

    /// <summary>
    /// Получить произвольное значение из словаря значений
    /// </summary>
    /// <param name="key">Ключ</param>
    /// <typeparam name="T">Тип значения</typeparam>
    /// <returns>Ранее сохранённое значение либо null</returns>
    public T? GetCustomValue<T>(string key)
    {
        if (_customMap.TryGetValue(key, out var value))
        {
            return (T)value;
        }

        return default;
    }

    internal void EnsureMapInitialized(IDataMigrationStep dataMigrationStep, params Type[] types)
    {
        foreach (var type in types)
        {
            var anyInitialized = false;

            if (!_map.ContainsKey(type))
            {
                _map.Add(type, new Dictionary<long, object>());
                anyInitialized = true;
            }

            if (!_stringMap.ContainsKey(type))
            {
                _stringMap.Add(type, new Dictionary<string, object>());
                anyInitialized = true;
            }

            if (!anyInitialized)
            {
                _logger.LogWarning(
                    "Step '{Step}' has declared to produce type {Type} but no entries has been added to map. Ensure step is calling DataMigrationContext.AddMap on execution",
                    dataMigrationStep.GetType().Name,
                    type.Name);
            }
        }
    }
}