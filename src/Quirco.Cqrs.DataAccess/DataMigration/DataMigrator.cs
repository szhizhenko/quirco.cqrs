﻿using System.Diagnostics;
using Microsoft.Extensions.Logging;
using Quirco.Cqrs.DataAccess.DataMigration.Interfaces;

namespace Quirco.Cqrs.DataAccess.DataMigration;

/// <summary>
/// Runs data migration process and logs all steps
/// </summary>
public class DataMigrator
{
    private readonly DataMigrationContext _context;

    protected ILogger<DataMigrator> Logger { get; }

    private readonly ICollection<IDataMigrationStep> _steps;

    public DataMigrator(
        ILogger<DataMigrator> logger,
        IEnumerable<IDataMigrationStep> steps,
        DataMigrationContext context)
    {
        _context = context;
        Logger = logger;
        _steps = steps.ToList();
    }

    /// <summary>
    /// Run migration process
    /// </summary>
    public virtual async ValueTask Migrate(CancellationToken cancellationToken)
    {
        var stopWatch = Stopwatch.StartNew();
        var total = _steps.Count;

        Logger.LogWarning("Executing migrations ({MigrationsCont}): {MigrationsList}",
                          total,
                          string.Join(", ", _steps.Select(s => s.GetType().Name)));

        var current = 1;

        foreach (var step in _steps)
        {
            try
            {
                var sw = new Stopwatch();
                sw.Start();

                Logger.LogInformation("Running step {CurrentStep} of {StepsTotal}: {StepName}",
                                      current++,
                                      total,
                                      step.GetType().Name);

                var result = await step.Execute(cancellationToken);

                var stepInfo = new MigrationStepNodeInfo(step.GetType());
                _context.EnsureMapInitialized(step, stepInfo.ProducedTypes);

                Logger.LogWarning("Step {StepName} completed in {Elapsed} ({@Result})",
                                  step.GetType().Name,
                                  sw.Elapsed,
                                  result);
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, "Error processing step {StepName}", step.GetType().Name);

                throw;
            }
        }

        Logger.LogWarning("Migrations completed in {Elapsed}", stopWatch.Elapsed);
    }
}