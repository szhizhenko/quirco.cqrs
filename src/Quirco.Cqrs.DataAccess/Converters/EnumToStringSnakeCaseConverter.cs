﻿using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion.Internal;
using Quirco.Cqrs.Infrastructure.Extensions;

namespace Quirco.Cqrs.DataAccess.Converters;

public class EnumToStringSnakeCaseConverter<TEnum> : StringEnumConverter<TEnum, string, TEnum>
    where TEnum : struct
{
    public EnumToStringSnakeCaseConverter(ConverterMappingHints mappingHints = null)
        : base(
            sourceEnum => sourceEnum.ToString().ToSnakeCase(),
            str => ToEnum().Compile().Invoke(str.ToPascalCaseFromSnakeCase()),
            mappingHints)
    {
    }

    /// <summary>
    ///     A <see cref="ValueConverterInfo" /> for the default use of this converter.
    /// </summary>
    public static ValueConverterInfo DefaultInfo { get; }
        = new(typeof(TEnum), typeof(string), i => new EnumToStringSnakeCaseConverter<TEnum>(i.MappingHints));
}