using Microsoft.EntityFrameworkCore;

namespace Quirco.Cqrs.Hangfire;

public class JobsContext : DbContext
{
    protected JobsContext()
    {
    }

    public JobsContext(DbContextOptions<JobsContext> options) : base(options)
    {
    }
}