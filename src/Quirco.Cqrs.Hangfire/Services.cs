using Hangfire;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Quirco.Cqrs.Hangfire;

public static class Services
{
    public static IServiceCollection AddHangfireWarmup(
        this IServiceCollection services,
        Action<IGlobalConfiguration> configuration,
        Action<DbContextOptionsBuilder> createDbContextConfiguration)
    {
        return services;
    }
}