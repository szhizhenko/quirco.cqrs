using System.Linq.Expressions;
using Hangfire;
using Microsoft.Extensions.Logging;
using Quirco.Cqrs.Infrastructure.Jobs;

namespace Quirco.Cqrs.Hangfire;

public class HangfireJobsService : IJobsService
{
    private readonly ILogger<HangfireJobsService> _logger;

    public HangfireJobsService(ILogger<HangfireJobsService> logger)
    {
        _logger = logger;
    }

    public Task<string> Enqueue<T>(Expression<Func<T, Task>> methodCall)
    {
        try
        {
            var jobName = BackgroundJob.Enqueue(methodCall);

            return Task.FromResult(jobName);
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "Can't enqueue job: {JobName}", typeof(T));

            throw;
        }
    }

    public Task<string> Enqueue<T>(Expression<Func<T, Task>> methodCall, TimeSpan delay)
    {
        try
        {
            var jobName = BackgroundJob.Schedule(methodCall, delay);

            return Task.FromResult(jobName);
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "Can't enqueue job: {JobName}", typeof(T));

            throw;
        }
    }

    public Task Schedule<T>(string id, Expression<Func<T, Task>> methodCall, string cronExpression)
    {
        try
        {
            RecurringJob.AddOrUpdate(id,
                                     methodCall,
                                     cronExpression,
                                     new RecurringJobOptions
                                     {
                                         TimeZone = TimeZoneInfo.Local
                                     }
            );

            return Task.CompletedTask;
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "Can't schedule job: {JobName}", typeof(T));

            throw;
        }
    }

    public Task RemoveIfExists(string recurringJobId)
    {
        try
        {
            RecurringJob.RemoveIfExists(recurringJobId);

            return Task.CompletedTask;
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "Can't remove job: {JobName}", recurringJobId);

            throw;
        }
    }
}