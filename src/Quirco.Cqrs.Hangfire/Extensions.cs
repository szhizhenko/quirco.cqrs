using System.Linq.Expressions;
using Hangfire;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Quirco.Cqrs.Infrastructure.Jobs;
using Quirco.Cqrs.Infrastructure.Startup;

namespace Quirco.Cqrs.Hangfire;

public static class Extensions
{
    [Obsolete("Use instead IServiceCollection.ScheduleJob")]
    public static IApplicationBuilder ScheduleJob<TJob>(
        this IApplicationBuilder app,
        string jobId,
        Expression<Func<TJob, Task>> execute,
        string? cron)
        where TJob : class
    {
        var logger = app.ApplicationServices.GetRequiredService<ILogger<TJob>>();

        var jobName = typeof(TJob).Name;

        try
        {
            var jobsService = app.ApplicationServices.GetRequiredService<IJobsService>();

            jobsService.Schedule(jobId, execute, cron ?? Cron.Never());
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "Can't schedule job: {JobName}", jobName);
        }

        return app;
    }

    [Obsolete("Use instead IServiceCollection.EnqueueJob")]
    public static IApplicationBuilder EnqueueJob<TJob>(
        this IApplicationBuilder app,
        Expression<Func<TJob, Task>> execute)
        where TJob : class
    {
        var logger = app.ApplicationServices.GetRequiredService<ILogger<TJob>>();

        var jobName = typeof(TJob).Name;

        try
        {
            var jobsService = app.ApplicationServices.GetRequiredService<IJobsService>();
            jobsService.Enqueue(execute);
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "Can't enqueue job: {JobName}", jobName);
        }

        return app;
    }

    /// <summary>
    /// Schedule job to execute on cron
    /// </summary>
    /// <param name="services"></param>
    /// <param name="jobId"></param>
    /// <param name="execute"></param>
    /// <param name="cron"></param>
    /// <typeparam name="TJob"></typeparam>
    /// <returns></returns>
    public static IServiceCollection ScheduleJob<TJob>(
        this IServiceCollection services,
        string jobId,
        Expression<Func<TJob, Task>> execute,
        string? cron)
        where TJob : class
    {
        return services.AddPostStartup<IJobsService>(x => x.Schedule(jobId, execute, cron ?? Cron.Never()));
    }

    /// <summary>
    /// Equeue job to run immediately (almost)
    /// </summary>
    /// <param name="services"></param>
    /// <param name="execute"></param>
    /// <typeparam name="TJob"></typeparam>
    /// <returns></returns>
    public static IServiceCollection EnqueueJob<TJob>(
        this IServiceCollection services,
        Expression<Func<TJob, Task>> execute)
        where TJob : class
    {
        return services.AddPostStartup<IJobsService>(x => x.Enqueue(execute));
    }
}