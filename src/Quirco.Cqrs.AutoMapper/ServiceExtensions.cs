﻿using System.Reflection;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Quirco.Cqrs.Core.Mapping;

namespace Quirco.Cqrs.AutoMapper;

public static class ServiceExtensions
{
    /// <summary>
    /// Adds automapper and initializes mapping adapter
    /// </summary>
    public static IServiceCollection AddAutoMapperAdapter(this IServiceCollection services, params Assembly[] assemblies)
    {
        services.AddAutoMapper(assemblies);
        services.TryAddScoped<IMapper, AutoMapperAdapter>();
        return services;
    }
}