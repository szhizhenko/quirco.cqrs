﻿using AutoMapper.QueryableExtensions;
using Quirco.Cqrs.Core.Mapping;
using IAutoMapper = AutoMapper.IMapper;

namespace Quirco.Cqrs.AutoMapper;

public class AutoMapperAdapter : IMapper
{
    private readonly IAutoMapper _autoMapper;

    public AutoMapperAdapter(IAutoMapper autoMapper)
    {
        _autoMapper = autoMapper;
    }

    public TDest Map<TDest>(object source) => _autoMapper.Map<TDest>(source);
    public TDest Map<TSrc, TDest>(TSrc source) => _autoMapper.Map<TSrc, TDest>(source);
    public TDest Map<TSrc, TDest>(TSrc source, TDest destination) => _autoMapper.Map(source, destination);
    public IQueryable<TDest> ProjectTo<TDest>(IQueryable sourceQuery) =>
        sourceQuery.ProjectTo<TDest>(_autoMapper.ConfigurationProvider);

    public IQueryable<TDest> ProjectTo<TDest>(IQueryable sourceQuery, object parameters) =>
        sourceQuery.ProjectTo<TDest>(_autoMapper.ConfigurationProvider, parameters);
}