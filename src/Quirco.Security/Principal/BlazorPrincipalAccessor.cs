﻿using System.Security.Principal;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.AspNetCore.Http;

namespace Quirco.Security.Principal;

public class BlazorPrincipalAccessor : IPrincipalAccessor
{
    private readonly IHttpContextAccessor _httpContextAccessor;
    private readonly AuthenticationStateProvider _authStateProvider;
    private readonly ScopePrincipalStorage _scopePrincipalStorage;

    public BlazorPrincipalAccessor(
        IHttpContextAccessor httpContextAccessor,
        AuthenticationStateProvider authStateProvider,
        ScopePrincipalStorage scopePrincipalStorage)
    {
        _httpContextAccessor = httpContextAccessor;
        _authStateProvider = authStateProvider;
        _scopePrincipalStorage = scopePrincipalStorage;
    }

    public async Task<IPrincipal> GetPrincipal()
    {
        var principal = _scopePrincipalStorage.GetPrincipal();
        if (principal == null)
        {
            var authState = await _authStateProvider.GetAuthenticationStateAsync();

            principal = (authState.User.Identity?.IsAuthenticated == true ? authState.User : _httpContextAccessor.HttpContext?.User) ??
                        IPrincipalAccessor.PrincipalUnauthorized;

            _scopePrincipalStorage.SetPrincipal(principal);
        }

        return principal;
    }
}