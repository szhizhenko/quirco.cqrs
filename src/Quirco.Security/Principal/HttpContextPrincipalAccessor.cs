﻿using System.Security.Principal;
using Microsoft.AspNetCore.Http;

namespace Quirco.Security.Principal;

/// <summary>
/// Factory to access current principal from current http context
/// </summary>
public class HttpContextPrincipalAccessor : IPrincipalAccessor
{
    private readonly IHttpContextAccessor _httpContextAccessor;

    public HttpContextPrincipalAccessor(IHttpContextAccessor httpContextAccessor)
    {
        _httpContextAccessor = httpContextAccessor;
    }

    public Task<IPrincipal> GetPrincipal()
    {
        return _httpContextAccessor.HttpContext == null
            ? Task.FromResult(IPrincipalAccessor.PrincipalUnauthorized)
            : Task.FromResult<IPrincipal>(_httpContextAccessor.HttpContext.User);
    }
}