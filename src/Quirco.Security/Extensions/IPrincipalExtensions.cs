using System.Diagnostics.CodeAnalysis;
using System.Security.Claims;
using System.Security.Principal;

namespace Quirco.Security.Extensions;

/// <summary>
/// Set of extensions methods for <see cref="ClaimsPrincipal"/>
/// </summary>
public static class PrincipalExtensions
{
    /// <summary>
    /// Ensures that user has all specified permissions
    /// </summary>
    /// <param name="principal">Principal instance, should be of type <see cref="ClaimsPrincipal"/></param>
    /// <param name="permissions">List of permissions to check</param>
    public static bool HasPermissions(this IPrincipal principal, params string[] permissions)
    {
        if (principal is ClaimsPrincipal claimsPrincipal)
        {
            return permissions.All(p => claimsPrincipal.HasClaim(Constants.ClaimTypes.Permission, p));
        }

        return false;
    }

    /// <summary>
    /// Ensures that user has all specified roles
    /// </summary>
    /// <param name="principal">Principal instance, should be of type <see cref="ClaimsPrincipal"/></param>
    /// <param name="roles">List of roles to check</param>
    public static bool IsInRoles(this IPrincipal principal, params string[] roles)
    {
        if (principal is ClaimsPrincipal claimsPrincipal)
        {
            return roles.Any(r => claimsPrincipal.IsInRole(r));
        }

        return false;
    }

    /// <summary>
    /// Returns first claim of specified type
    /// </summary>
    /// <param name="principal">Principal instance, should be of type <see cref="ClaimsPrincipal"/></param>
    /// <param name="claimType">Type of claim to return</param>
    /// <returns></returns>
    public static Claim? GetClaim(this IPrincipal principal, string claimType)
    {
        if (principal is ClaimsPrincipal claimsPrincipal)
        {
            return claimsPrincipal.Claims.FirstOrDefault(x => x.Type == claimType);
        }

        return null;
    }

    /// <summary>
    /// Returns value of claim by the specified type
    /// </summary>
    /// <param name="principal"></param>
    /// <param name="claimType"></param>
    /// <param name="value"></param>
    /// <typeparam name="TValue"></typeparam>
    /// <returns></returns>
    public static bool TryGetClaimValue<TValue>(this IPrincipal principal, string claimType, [MaybeNullWhen(false)] out TValue value)
        where TValue : IConvertible
    {
        var claim = principal.GetClaim(claimType);

        if (string.IsNullOrWhiteSpace(claim?.Value))
        {
            value = default;

            return false;
        }

        try
        {
            value = (TValue)Convert.ChangeType(claim.Value, typeof(TValue));

            return true;
        }
        catch
        {
            value = default;

            return false;
        }
    }
}
