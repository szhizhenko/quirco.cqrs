using System.Collections.Concurrent;
using System.Collections.ObjectModel;
using System.Reflection;
using Quirco.Security.Attributes;

namespace Quirco.Security.Extensions;

internal static class PermissionsExtensions
{
    /// <summary>
    /// Получение разрешений для ролей
    /// </summary>
    /// <param name="assemblies">Сборки, по которым будет произведен поиск классов, описывающих разрешения</param>
    /// <returns>Коллекция разрешений с разрешенным массивом ролей</returns>
    internal static IDictionary<string, string[]> GetRequiredRolePolicies(this Assembly[] assemblies)
    {
        var availablePermissions = assemblies.GetRequiredClaimPolicies()
            .Select(x => KeyValuePair.Create(x.Key, new List<string>()));

        var permissions = new ConcurrentDictionary<string, List<string>>(availablePermissions);

        var types = GetPermissionsTypes(assemblies);

        foreach (var type in types.SelectMany(t => t.GetNestedTypes()))
        {
            var rolesOnClass = type.CustomAttributes.GetRelatedRoles();

            foreach (var property in type.GetProperties())
            {
                var propertyValue = property.ConcatAndSetPermission();
                var rolesOnProperty = property.CustomAttributes.GetRelatedRoles();

                permissions.AddOrUpdate(
                    propertyValue,
                    s => new List<string>(),
                    (s, list) =>
                    {
                        list.AddRange(rolesOnClass.Union(rolesOnProperty).Distinct());

                        return list;
                    });
            }

            foreach (var field in type.GetFields())
            {
                var fieldValue = field.GetRawConstantValue()?.ToString();
                var rolesOnFields = field.CustomAttributes.GetRelatedRoles();

                if (fieldValue != null)
                {
                    permissions.AddOrUpdate(
                        fieldValue,
                        s => new List<string>(),
                        (s, list) =>
                        {
                            list.AddRange(rolesOnClass.Union(rolesOnFields).Distinct());

                            return list;
                        });
                }
            }
        }

        return permissions.ToDictionary(x => x.Key, x => x.Value.ToArray());
    }

    /// <summary>
    /// Получение и инициализация разрешений
    /// </summary>
    /// <param name="assemblies">Сборки, по которым будет произведен поиск классов, описывающих разрешения</param>
    /// <returns>Коллекция разрешений</returns>
    public static IDictionary<string, string> GetRequiredClaimPolicies(this Assembly[] assemblies)
    {
        var types = GetPermissionsTypes(assemblies);

        var permissions = types.SelectMany(t => t.GetNestedTypes())
            .SelectMany(type => type.GetProperties()
                .Select(property =>
                {
                    var propertyValue = property.ConcatAndSetPermission();
                    return KeyValuePair.Create(propertyValue, property.GetValue(null)?.ToString() ?? string.Empty);
                }))
            .Where(x => !string.IsNullOrEmpty(x.Value))
            .ToDictionary(x => x.Key, x => x.Value);

        var permissionsFieldValues = types.SelectMany(t => t.GetNestedTypes())
            .SelectMany(type => type.GetFields())
            .Select(a => a.GetRawConstantValue()?.ToString())
            .Where(a => !string.IsNullOrEmpty(a));

        foreach (var permissionsFieldValue in permissionsFieldValues)
        {
            permissions.Add(permissionsFieldValue!, permissionsFieldValue!);
        }

        return permissions;
    }

    private static Type[] GetPermissionsTypes(Assembly[] assemblies)
    {
        return assemblies
            .SelectMany(a => a.GetTypes()
                .Where(t => t.GetCustomAttributes(typeof(PermissionsDataAttribute), true).Length > 0))
            .ToArray();
    }

    internal static string ConcatAndSetPermission(this PropertyInfo property)
    {
        var type = property.DeclaringType;
        var chainOfNames = new List<string>();
        var iteratedType = type;

        while (!string.IsNullOrEmpty(iteratedType?.DeclaringType?.Name))
        {
            chainOfNames.Add(iteratedType.DeclaringType?.Name ?? string.Empty);
            iteratedType = iteratedType.DeclaringType;
        }

        chainOfNames.Reverse();
        chainOfNames.Add(type!.Name);
        chainOfNames.Add(property.Name);

        var propertyValue = string.Join('.', chainOfNames.Where(s => !string.IsNullOrEmpty(s)));
        property.SetValue(null, propertyValue);

        return propertyValue;
    }

    internal static IEnumerable<string> GetRelatedRoles(this IEnumerable<CustomAttributeData> attributes)
    {
        return attributes
            .Where(x => x.AttributeType == typeof(RequireRolesAttribute))
            .SelectMany(x => x.ConstructorArguments)
            .SelectMany(x =>
            {
                return x.Value is ReadOnlyCollection<CustomAttributeTypedArgument> arguments
                    ? arguments.Select(a => a.Value is string value ? value : string.Empty)
                    : Array.Empty<string>();
            });
    }
}