namespace Quirco.Security.Attributes;

[AttributeUsage(AttributeTargets.Class)]
public class PermissionsDataAttribute : Attribute
{
    public string ModuleName { get; set; }

    public PermissionsDataAttribute(string moduleName)
    {
        ModuleName = moduleName;
    }
}