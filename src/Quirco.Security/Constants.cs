namespace Quirco.Security;

public static class Constants
{
    public static class ClaimTypes
    {
        public static readonly string Permission = "Permission";
        public static readonly string UserId = "user_id";
        public static readonly string Login = "login";
    }
}