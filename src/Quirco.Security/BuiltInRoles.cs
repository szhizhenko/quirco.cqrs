﻿namespace Quirco.Security;

public static class BuiltInRoles
{
    public const string Admin = nameof(Admin);
    public const string Employee = nameof(Employee);
}