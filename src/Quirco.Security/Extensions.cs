﻿using Microsoft.Extensions.DependencyInjection;
using Quirco.Security.Extensions;
using Quirco.Security.Permission;
using Quirco.Security.Principal;

namespace Quirco.Security;

public static class PolicyServicesExtensions
{
    /// <summary>
    /// Configures asp.net authorization for list of claims and policies
    /// </summary>
    public static IServiceCollection AddPermissions(this IServiceCollection services, Action<PermissionsOptions>? configure = null)
    {
        if (services == null)
        {
            throw new ArgumentNullException(nameof(services));
        }

        var options = new PermissionsOptions();
        configure?.Invoke(options);

        var permissionsProvider = new AssemblyScanPermissionsProvider(options.Assemblies);
        services.AddSingleton<IPermissionsProvider>(permissionsProvider);
        services.AddScoped<ScopePrincipalStorage>();

        var rolePolicies = options.Assemblies.GetRequiredRolePolicies();
        var permissions = permissionsProvider.GetAllPermissions();

        var permissionsWithRoles = permissions
            .SelectMany(p => p.Permissions)
            .Join(rolePolicies, permission => permission, role => role.Key, (permission, roles) => new
            {
                Permission = permission,
                Roles = roles.Value
            })
            .ToArray();

        services.AddAuthorizationCore(opt =>
        {
            foreach (var permissionWithRoles in permissionsWithRoles)
            {
                opt.AddPolicy(permissionWithRoles.Permission, policy =>
                {
                    if (options.ImplicitRoles)
                    {
                        policy
                            .RequireAuthenticatedUser()
                            .RequireAssertion(authContext
                                => authContext.User.IsInRoles(permissionWithRoles.Roles)
                                   || authContext.User.HasPermissions(permissionWithRoles.Permission));
                    }
                    else
                    {
                        policy
                            .RequireAuthenticatedUser()
                            .RequireAssertion(authContext => authContext.User.HasPermissions(permissionWithRoles.Permission));
                    }
                });
            }
        });

        return services;
    }
}