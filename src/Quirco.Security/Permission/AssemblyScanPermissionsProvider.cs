using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Reflection;
using Quirco.Security.Attributes;
using Quirco.Security.Extensions;

namespace Quirco.Security.Permission;

/// <summary>
/// Ищет в сборках все типы декорированные атрибутом PermissionsData и формирует из них массив типа
/// "Имя подсистемы - Имя вложенного типа - Массив прав этого типа"
/// </summary>
public class AssemblyScanPermissionsProvider : IPermissionsProvider
{
    private readonly Assembly[] _assemblies;

    /// <summary>
    /// Конструктор
    /// </summary>
    /// <param name="assemblies">Сборки в которых будет происходит поиск</param>
    public AssemblyScanPermissionsProvider(params Assembly[] assemblies)
    {
        _assemblies = assemblies;
    }

    public IEnumerable<Permission> GetAllPermissions()
    {
        foreach (var asm in _assemblies)
        {
            foreach (var type in asm.GetTypes())
            {
                var attrib = (PermissionsDataAttribute?)type.GetCustomAttribute(typeof(PermissionsDataAttribute));
                if (attrib == null)
                    continue;

                foreach (var nestedType in type.GetNestedTypes())
                {
                    var readableModuleName = attrib.ModuleName;
                    var readableEntityName =
                        nestedType.GetCustomAttribute<DisplayNameAttribute>()?.DisplayName
                        ?? nestedType.GetCustomAttribute<DisplayAttribute>()?.Name
                        ?? nestedType.Name;
                    var roles = nestedType.CustomAttributes.GetRelatedRoles();

                    var readablePermissions = new List<string>();
                    var permissions = new List<string>();

                    foreach (var property in nestedType.GetProperties())
                    {
                        var readablePropertyName =
                            property.GetCustomAttribute<DisplayNameAttribute>()?.DisplayName
                            ?? property.GetCustomAttribute<DisplayAttribute>()?.Name
                            ?? property.Name;
                        readablePermissions.Add($"{readableModuleName}.{readableEntityName}.{readablePropertyName}");
                        var permissionName = property.ConcatAndSetPermission();
                        permissions.Add("" + permissionName);
                    }

                    var constants = nestedType.GetFields(BindingFlags.Public | BindingFlags.Static | BindingFlags.FlattenHierarchy);
                    foreach (var property in constants.Where(fi => fi.IsLiteral && !fi.IsInitOnly))
                    {
                        var readablePropertyName =
                            property.GetCustomAttribute<DisplayNameAttribute>()?.DisplayName
                            ?? property.GetCustomAttribute<DisplayAttribute>()?.Name
                            ?? property.Name;
                        readablePermissions.Add($"{readableModuleName}.{readableEntityName}.{readablePropertyName}");
                        permissions.Add("" + property.GetValue(null));
                    }

                    yield return new Permission(
                        readableModuleName,
                        readableEntityName,
                        readablePermissions,
                        permissions,
                        roles);
                }
            }
        }
    }
}