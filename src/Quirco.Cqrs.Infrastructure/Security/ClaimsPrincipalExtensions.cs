using System.Security.Claims;

namespace Quirco.Cqrs.Infrastructure.Security;

public static class ClaimsPrincipalExtensions
{
    public static string? FindFirstValue(this ClaimsPrincipal principal, string claimName)
    {
        return principal.FindFirst(claimName)?.Value;
    }
}