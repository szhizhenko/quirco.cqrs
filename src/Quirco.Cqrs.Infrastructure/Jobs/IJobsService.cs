using System.Diagnostics.CodeAnalysis;
using System.Linq.Expressions;

namespace Quirco.Cqrs.Infrastructure.Jobs;

/// <summary>
/// Background jobs service
/// </summary>
public interface IJobsService
{
    /// <summary>
    /// Equeue job to run immediately (almost)
    /// </summary>
    /// <param name="methodCall">Expression for doing work.</param>
    /// <typeparam name="T">Type of job to enqueue.</typeparam>
    /// <returns>ID of job.</returns>
    Task<string> Enqueue<T>([NotNull] Expression<Func<T, Task>> methodCall);

    /// <summary>
    /// Enqueue job to execute after some delay
    /// </summary>
    /// <param name="methodCall">Job method.</param>
    /// <param name="delay">TimeSpan to delay job execution.</param>
    /// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
    Task<string> Enqueue<T>([NotNull] Expression<Func<T, Task>> methodCall, TimeSpan delay);

    /// <summary>
    /// Schedule job to execute on cron
    /// </summary>
    /// <param name="id">Job unique id.</param>
    /// <param name="methodCall">Job method.</param>
    /// <param name="cronExpression">Cron expression.</param>
    /// <typeparam name="T">Type of job.</typeparam>
    /// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
    Task Schedule<T>(string id, [NotNull] Expression<Func<T, Task>> methodCall, string cronExpression);

    /// <summary>
    /// Cancels recurring job execution.
    /// </summary>
    /// <param name="recurringJobId">Job unique id.</param>
    /// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
    Task RemoveIfExists(string recurringJobId);
}