using System.Diagnostics;
using FluentValidation;
using MediatR;
using Microsoft.Extensions.Logging;

namespace Quirco.Cqrs.Infrastructure.Logging;

/// <summary>
/// Logs any command execution to log pipeline.
/// </summary>
/// <typeparam name="TRequest">Type of request.</typeparam>
/// <typeparam name="TResponse">Type of response.</typeparam>
public class LoggingBehavior<TRequest, TResponse> : IPipelineBehavior<TRequest, TResponse>
    where TRequest : notnull
{
    private readonly ILogger _logger;

    public LoggingBehavior(ILogger<LoggingBehavior<TRequest, TResponse>> logger)
    {
        _logger = logger;
    }

    public async Task<TResponse> Handle(
        TRequest request,
        RequestHandlerDelegate<TResponse> next,
        CancellationToken cancellationToken)
    {
        var commandName = GetName(typeof(TRequest));

        try
        {
            if (_logger.IsEnabled(LogLevel.Debug))
            {
                _logger.LogDebug("Command {CommandName} is executing. Payload:\n{@Request}", commandName, request);
            }

            using var activity = new Activity(typeof(TRequest).Name);
            activity.Start();
            var result = await next();
            activity.Stop();
            _logger.Log(
                activity.Duration.TotalSeconds > 10
                    ? LogLevel.Warning
                    : LogLevel.Information,
                "Command {CommandName} completed in {ElapsedMilliseconds} ms",
                commandName,
                activity.Duration.TotalMilliseconds);

            return result;
        }
        catch (ValidationException ex)
        {
            _logger.LogInformation("Validation error occured: {ValidationMessage}", ex.Message);

            throw;
        }
        catch (Exception ex)
        {
            _logger.LogError(ex,
                "Command {CommandName} ended up with error. Payload:\n{@Request}",
                commandName,
                request);

            throw;
        }
    }

    private static string GetName(Type type)
    {
        return type.DeclaringType != null
            ? $"{type.DeclaringType.Name}.{type.Name}"
            : type.Name;
    }
}