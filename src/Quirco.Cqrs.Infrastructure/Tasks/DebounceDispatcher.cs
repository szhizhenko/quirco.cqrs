using System.Reactive.Linq;
using System.Reactive.Subjects;

namespace Quirco.Cqrs.Infrastructure.Tasks;

public class DebounceDispatcher<TSource, TResult> : IDisposable
{
    private readonly IDisposable? _subscription;

    private readonly Subject<TSource> _searchSubject = new();

    public DebounceDispatcher(int intervalMilliseconds, Func<TSource, Task<TResult>> func, Action<TResult> consumer)
    {
        _subscription = _searchSubject
            .Throttle(TimeSpan.FromMilliseconds(intervalMilliseconds))
            .DistinctUntilChanged()
            .Select(async item => await func.Invoke(item))
            .Switch()
            .Subscribe(consumer);
    }

    public void Add(TSource item)
    {
        _searchSubject.OnNext(item);
    }

    public void Dispose()
    {
        _subscription?.Dispose();
        _searchSubject.Dispose();
    }
}