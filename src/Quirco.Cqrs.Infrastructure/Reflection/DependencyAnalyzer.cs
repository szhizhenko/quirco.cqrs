﻿using System.Reflection;

namespace Quirco.Cqrs.Infrastructure.Reflection;

/// <summary>
/// Analyze dependencies between assemblies and displays wrong version's sources
/// </summary>
public class DependencyAnalyzer
{
    private readonly List<AssemblyInfo> _assemblies;

    public DependencyAnalyzer()
    {
        var assembliesPaths = Directory.GetFiles(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location), "*.dll");
        _assemblies = new List<AssemblyInfo>();
        foreach (var assemblyPath in assembliesPaths.OrderBy(f => f))
        {
            try
            {
                var assembly = Assembly.LoadFrom(assemblyPath);
                var assemblyName = assembly.GetName();
                if (assemblyName.Name.StartsWith("System."))
                    continue;

                var dependencies = assembly.GetReferencedAssemblies()
                    .Where(r => !r.Name.StartsWith("System."))
                    .Select(a => new AssemblyInfo(a.Name, a.Version.ToString(), null))
                    .ToArray();
                _assemblies.Add(new AssemblyInfo(assemblyName.Name, assemblyName.Version.ToString(), dependencies));
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"Error loading assembly '{assemblyPath}': {ex.Message}");
                Console.ResetColor();
            }
        }
    }

    /// <summary>
    /// Processes <see cref="FileNotFoundException"/> and shows what assemblies has wrong references to old version's assemblies
    /// </summary>
    /// <param name="ex">Exception about file not found</param>
    public void ProcessFileNotFoundException(FileNotFoundException ex)
    {
        Console.ForegroundColor = ConsoleColor.Red;
        var file = ex.FileName.Split(',').FirstOrDefault();
        if (file != null)
        {
            var actualVersion = _assemblies.FirstOrDefault(a => a.Name == file);
            if (actualVersion != null)
            {
                Console.WriteLine($"Expected: {ex.FileName} but got version {actualVersion.Version}");
                var dependant = _assemblies.Where(a => a.Dependencies.Any(d => d.Name == file && d.Version == actualVersion.Version));
                Console.WriteLine("Dependant assemblies: ");
                foreach (var info in dependant)
                {
                    Console.WriteLine(info.Name + ": " + info.Version);
                }
            }
        }

        Console.ResetColor();
    }

    private sealed record AssemblyInfo(string Name, string Version, AssemblyInfo[]? Dependencies);
}