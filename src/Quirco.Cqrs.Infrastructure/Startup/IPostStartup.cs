namespace Quirco.Cqrs.Infrastructure.Startup;

/// <summary>
/// Контракт для вызова функции при старте приложения
/// </summary>
public interface IPostStartup
{
    /// <summary>
    /// Execute logic
    /// </summary>
    /// <param name="cancellationToken">CancellationToken</param>
    /// <returns>Task</returns>
    Task Execute(CancellationToken cancellationToken);
}