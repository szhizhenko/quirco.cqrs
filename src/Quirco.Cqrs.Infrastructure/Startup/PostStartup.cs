namespace Quirco.Cqrs.Infrastructure.Startup;

/// <summary>
/// Оборачивает функцию инициализации какого-то сервиса для вызова после запуска
/// </summary>
/// <typeparam name="TService">Сервис предоставляющий функцию инициализации</typeparam>
internal sealed class PostStartup<TService> : IPostStartup
{
    private readonly TService _service;
    private readonly Func<TService, Task> _initialize;

    public PostStartup(TService service, Func<TService, Task> initialize)
    {
        _service = service;
        _initialize = initialize;
    }

    /// <inheritdoc />
    public Task Execute(CancellationToken cancellationToken)
    {
        return _initialize(_service);
    }
}