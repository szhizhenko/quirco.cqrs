using System.Reflection;
using Microsoft.Extensions.DependencyInjection;

namespace Quirco.Cqrs.Infrastructure.Startup;

/// <summary>
/// Extensions
/// </summary>
public static class ServiceCollectionExtensions
{
    /// <summary>
    /// Запускает метод после старта приложения определенного сервиса
    /// </summary>
    /// <param name="services"></param>
    /// <param name="func"></param>
    /// <typeparam name="TService"></typeparam>
    /// <returns></returns>
    public static IServiceCollection AddPostStartup<TService>(
        this IServiceCollection services,
        Func<TService, Task> func)
    {
        services.AddHostedService<PostStartupsExecutor>();

        return services
            .AddTransient<IPostStartup>(x => new PostStartup<TService>(x.GetRequiredService<TService>(), func));
    }

    /// <summary>
    /// Регистрирует сервисы для автоматического старта после запуска приложения
    /// </summary>
    public static IServiceCollection AddPostStartups(this IServiceCollection services, params Assembly[] assemblies)
    {
        services.AddHostedService<PostStartupsExecutor>();

        var postStartups = assemblies
            .SelectMany(x => x.GetTypes())
            .Where(x => x.IsClass && !x.IsAbstract && x.IsAssignableTo(typeof(IPostStartup)))
            .ToArray();

        foreach (var postStartup in postStartups)
        {
            services.AddTransient(typeof(IPostStartup), postStartup);
        }

        return services;
    }

    /// <summary>
    /// Регистрирует сервис для автоматического старта после запуска приложения
    /// </summary>
    /// <param name="services"></param>
    /// <typeparam name="TPostStartup"></typeparam>
    /// <returns></returns>
    public static IServiceCollection AddPostStartup<TPostStartup>(this IServiceCollection services)
        where TPostStartup : class, IPostStartup
    {
        services.AddHostedService<PostStartupsExecutor>();

        return services.AddTransient<IPostStartup, TPostStartup>();
    }
}