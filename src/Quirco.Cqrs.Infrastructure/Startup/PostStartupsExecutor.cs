using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace Quirco.Cqrs.Infrastructure.Startup;

/// <summary>
/// Service for initialization services after startup application
/// </summary>
internal sealed class PostStartupsExecutor : BackgroundService
{
    private readonly IServiceProvider _serviceProvider;
    private readonly ILogger<PostStartupsExecutor> _logger;

    public PostStartupsExecutor(IServiceProvider serviceProvider, ILogger<PostStartupsExecutor> logger)
    {
        _serviceProvider = serviceProvider;
        _logger = logger;
    }

    /// <inheritdoc />
    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        await using var scope = _serviceProvider.CreateAsyncScope();

        var postStartups = scope.ServiceProvider.GetServices<IPostStartup>();

        _logger.LogTrace("Starting post-startup services...");

        foreach (var postStartup in postStartups)
        {
            try
            {
                await postStartup.Execute(stoppingToken);

                _logger.LogTrace("Service '{ServiceType}' has been started", postStartup.GetType());
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Can't start service '{ServiceType}'.", postStartup.GetType());
            }
        }

        await StopAsync(stoppingToken);
    }
}