using Newtonsoft.Json.Converters;

namespace Quirco.Cqrs.Infrastructure.Json;

/// <summary>
/// Custom date time converter.
/// </summary>
public class DateTimeConverter : IsoDateTimeConverter
{
    /// <summary>
    /// Initializes a new instance of the <see cref="DateTimeConverter"/>.
    /// </summary>
    public DateTimeConverter(string format)
    {
        DateTimeFormat = format;
    }
}