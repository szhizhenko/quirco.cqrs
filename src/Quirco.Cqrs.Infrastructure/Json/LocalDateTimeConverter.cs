﻿using System.Globalization;
using Newtonsoft.Json.Converters;

namespace Quirco.Cqrs.Infrastructure.Json;

/// <summary>
/// Подразумечает, что время локальное.
/// </summary>
public class LocalDateTimeConverter : IsoDateTimeConverter
{
    /// <summary>
    /// Initializes a new instance of the <see cref="LocalDateTimeConverter"/>.
    /// </summary>
    public LocalDateTimeConverter()
    {
        DateTimeStyles = DateTimeStyles.AssumeLocal;
    }
}