using isdayoff;
using isdayoff.Contract;
using Microsoft.Extensions.DependencyInjection;
using Quirco.Cqrs.Infrastructure.Startup;

namespace Quirco.Cqrs.Infrastructure.DayOff;

public static class ServiceCollectionExtensions
{
    /// <summary>
    /// Добавляет сервис для проверки праздничных дней
    /// </summary>
    /// <param name="services"></param>
    /// <param name="cfg"></param>
    /// <returns></returns>
    public static IServiceCollection AddDayOffService(
        this IServiceCollection services,
        Action<DayOffServiceConfig>? cfg = null)
    {
        var settings = IsDayOffSettings.Build
            .UseDefaultCountry(Country.Russia)
            .UseInMemoryCache()
            .Create();

        var isDayOff = new IsDayOff(settings);

        services
            .AddSingleton(isDayOff)
            .AddSingleton<IDayOffService, DayOffService>()
            .AddPostStartup<IDayOffService>(service =>
            {
                var config = new DayOffServiceConfig();

                cfg?.Invoke(config);

                return service.Initialize(config);
            });

        return services;
    }
}