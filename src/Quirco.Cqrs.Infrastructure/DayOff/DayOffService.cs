using System.Collections.Concurrent;
using isdayoff;
using isdayoff.Contract;
using Microsoft.Extensions.Logging;

namespace Quirco.Cqrs.Infrastructure.DayOff;

/// <summary>
/// Сервис для проверки выходных дней
/// </summary>
public interface IDayOffService
{
    /// <summary>
    /// Инициализирует даты за период в кэш
    /// </summary>
    /// <param name="config"></param>
    /// <returns></returns>
    Task Initialize(DayOffServiceConfig config);

    /// <summary>
    /// Возвращает из каэша или отправляет запрос на получение
    /// </summary>
    /// <param name="day"></param>
    /// <returns></returns>
    Task<DayType> CheckDayAsync(System.DateTime day);

    /// <summary>
    /// Возвращает сразу из кэша use <see cref="IDayOffService.Initialize"/>
    /// </summary>
    /// <param name="day"></param>
    /// <returns></returns>
    DayType CheckDay(System.DateTime day);
}

public class DayOffService : IDayOffService
{
    private readonly ConcurrentDictionary<System.DateTime, DayType> _cache = new();
    private int _cachedYearsFrom;
    private int _cachedYearsTo;

    private readonly IsDayOff _isDayOff;
    private readonly ILogger<DayOffService> _logger;
    private System.DateTime? _lastDateNotAvailable;

    public DayOffService(IsDayOff isDayOff, ILogger<DayOffService> logger)
    {
        _isDayOff = isDayOff;
        _logger = logger;
    }

    public DayType CheckDay(System.DateTime day)
    {
        return _cache.TryGetValue(day, out var type)
            ? type
            : GetDefaultDayType(day);
    }

    public async Task<DayType> CheckDayAsync(System.DateTime day)
    {
        if (_cache.TryGetValue(day, out var type))
        {
            return type;
        }

        if (_lastDateNotAvailable is not null)
        {
            return GetDefaultDayType(day);
        }

        try
        {
            var dayType = await _isDayOff.CheckDayAsync(day);

            _cache.TryAdd(day.Date, dayType);

            return dayType;
        }
        catch (Exception ex)
        {
            _lastDateNotAvailable = System.DateTime.UtcNow;

            _logger.LogTrace(ex, "Can't check day off '{Date}' from 'IsDayOff' Service", day);

            return GetDefaultDayType(day);
        }
    }

    private static DayType GetDefaultDayType(System.DateTime day)
    {
        return day.Date.DayOfWeek is DayOfWeek.Sunday or DayOfWeek.Saturday
            ? DayType.NotWorkingDay
            : DayType.WorkingDay;
    }

    public async Task Initialize(DayOffServiceConfig config)
    {
        if (!await TryInitCache(config.InitYearFrom, config.InitYearTo))
        {
            throw new Exception("Can't initialize cache for DayOffService");
        }
    }

    private async Task<bool> TryInitCache(int yearFrom, int yearTo)
    {
        try
        {
            for (var year = yearFrom; year <= yearTo; year++)
            {
                var days = await _isDayOff.CheckYearAsync(year);

                foreach (var date in days)
                {
                    _cache.TryAdd(date.DateTime.Date, date.DayType);
                }
            }

            if (_cachedYearsFrom is 0 || _cachedYearsFrom > yearFrom)
            {
                _cachedYearsFrom = yearFrom;
            }

            if (_cachedYearsTo is 0 || _cachedYearsTo < yearTo)
            {
                _cachedYearsTo = yearTo;
            }

            return true;
        }
        catch (Exception ex)
        {
            _lastDateNotAvailable = System.DateTime.UtcNow;

            _logger.LogWarning(ex, "Can't initialize cache for DayOffService");

            return false;
        }
    }
}

public class DayOffServiceConfig
{
    public const int MaxYearsOffset = 3;

    /// <summary>
    /// Количество годов для автокэширования при запросах
    /// </summary>
    public int MaxCachedYearsOffset { get; } = MaxYearsOffset;

    /// <summary>
    /// Начало первой инициализации кэша по году
    /// </summary>
    public int InitYearFrom { get; } = System.DateTime.Today.Year - MaxYearsOffset;

    /// <summary>
    /// Конец первой инициализации кэша по году
    /// </summary>
    public int InitYearTo { get; } = System.DateTime.Today.Year + MaxYearsOffset;
}