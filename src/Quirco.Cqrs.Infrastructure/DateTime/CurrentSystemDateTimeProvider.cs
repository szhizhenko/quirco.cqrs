namespace Quirco.Cqrs.Infrastructure.DateTime;

/// <summary>
/// Provide current system's date and time.
/// </summary>
public class CurrentSystemDateTimeProvider : ICurrentDateTimeProvider
{
    public System.DateTime Now => System.DateTime.Now;
}