namespace Quirco.Cqrs.Infrastructure.DateTime;

/// <summary>
/// Abstraction to provide current date and time. Useful for testing purposes
/// </summary>
public interface ICurrentDateTimeProvider
{
    System.DateTime Now { get; }
}