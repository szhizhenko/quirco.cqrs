namespace Quirco.Cqrs.Infrastructure.DateTime;

/// <summary>
/// Provide current system's date and time with UTC timezone.
/// </summary>
public class CurrentSystemUtcDateTimeProvider : ICurrentDateTimeProvider
{
    public System.DateTime Now => System.DateTime.UtcNow;
}