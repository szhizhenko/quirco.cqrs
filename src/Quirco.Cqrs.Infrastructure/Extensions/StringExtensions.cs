using System.Globalization;

namespace Quirco.Cqrs.Infrastructure.Extensions;

public static class StringExtensions
{
    public static string ToSnakeCase(this string source)
    {
        return string.Concat(source.Select((x, i) => i > 0 && char.IsUpper(x) ? "_" + x : x.ToString())).ToLowerInvariant();
    }

    public static string ToPascalCaseFromSnakeCase(this string str) =>
        string.Concat(
            str.Split('_')
                .Select(CultureInfo.CurrentCulture.TextInfo.ToTitleCase));
}