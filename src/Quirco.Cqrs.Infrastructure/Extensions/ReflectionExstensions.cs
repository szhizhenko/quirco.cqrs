﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Reflection;

namespace Quirco.Cqrs.Infrastructure.Extensions;

public static class ReflectionExstensions
{
    /// <summary>
    /// Возвращает название из атрибута <see cref="DisplayNameAttribute"/> для свойства
    /// </summary>
    /// <param name="propertyInfo"></param>
    /// <returns></returns>
    public static string GetDisplayName(this PropertyInfo propertyInfo)
    {
        var attr = propertyInfo.GetCustomAttributes()
            .FirstOrDefault(ca => ca is DisplayNameAttribute);
        return (attr != null ? (attr as DisplayNameAttribute)?.DisplayName : propertyInfo.Name) ?? propertyInfo.Name;
    }

    /// <summary>
    /// Возвращает формат из атрибута <see cref="DisplayNameAttribute"/> для свойства
    /// </summary>
    /// <param name="propertyInfo"></param>
    /// <returns></returns>
    public static string GetDisplayFormat(this PropertyInfo propertyInfo)
    {
        var attr = propertyInfo.GetCustomAttributes()
            .FirstOrDefault(ca => ca is DisplayFormatAttribute);
        return (attr != null ? (attr as DisplayFormatAttribute)?.DataFormatString : string.Empty) ?? string.Empty;
    }
}