namespace Quirco.Cqrs.Infrastructure.Extensions;

public static class CollectionExtensions
{
    public static IEnumerable<(T1, T2)> ZipGreedly<T1, T2>(this IEnumerable<T1> collection1, IEnumerable<T2> collection2)
    {
        var enumerator1 = collection1.GetEnumerator();
        var enumerator2 = collection2.GetEnumerator();
        bool collection1HasItems;
        bool collection2HasItems;
        do
        {
            collection1HasItems = enumerator1.MoveNext();
            collection2HasItems = enumerator2.MoveNext();
            var elt1 = collection1HasItems ? enumerator1.Current : default;
            var elt2 = collection2HasItems ? enumerator2.Current : default;
            yield return (elt1, elt2);
        } while (collection1HasItems || collection2HasItems);
    }
}