using System.ComponentModel.DataAnnotations;

namespace Quirco.Cqrs.Infrastructure.Extensions;

public static class EnumExtensions
{
    public static string? GetDisplayName<T>(this T value)
        where T : struct, Enum
    {
        var fieldInfo = value.GetType().GetField(value.ToString());
        if (fieldInfo == null)
            return null;

        if (!(fieldInfo.GetCustomAttributes(typeof(DisplayAttribute), false) is DisplayAttribute[]
                descriptionAttributes))
            return string.Empty;
        return descriptionAttributes.Length > 0 ? descriptionAttributes[0].Name : value.ToString();
    }
}