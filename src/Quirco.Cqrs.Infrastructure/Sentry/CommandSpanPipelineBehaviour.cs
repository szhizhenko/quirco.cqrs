﻿using MediatR;
using Sentry;

namespace Quirco.Cqrs.Infrastructure.Sentry;

/// <summary>
/// Wraps execution of mediatr pipeline into Sentry transaction to allow performance spans monitoring
/// </summary>
public class SentryPipelineBehaviour<TRequest, TResponse> : IPipelineBehavior<TRequest, TResponse>
    where TRequest : notnull
{
    public async Task<TResponse> Handle(
        TRequest request,
        RequestHandlerDelegate<TResponse> next,
        CancellationToken cancellationToken)
    {
        var requestName = GetName(typeof(TRequest));

        var transaction = SentrySdk.StartTransaction(requestName, requestName);

        try
        {
            return await next();
        }
        catch (Exception)
        {
            transaction.Status = SpanStatus.InternalError;

            throw;
        }
        finally
        {
            transaction.Finish();
        }
    }

    private static string GetName(Type type)
    {
        return type.DeclaringType != null
            ? $"{type.DeclaringType.Name}.{type.Name}"
            : type.Name;
    }
}