namespace Quirco.Cqrs.Elasticsearch.Behaviors;

public class SearchPipelineContext
{
    /// <summary>
    /// If set to true, Elastic won't be used for search in certain request
    /// </summary>
    public bool SkipElasticsearch { get; set; }
}