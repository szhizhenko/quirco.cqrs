using MediatR;
using Nest;
using Quirco.Cqrs.Core.Models;

namespace Quirco.Cqrs.Elasticsearch.Behaviors;

/// <summary>
/// Base pipeline to use Elastic as search engine
/// </summary>
public abstract class SearchPipelineBehavior<TQuery, TModel> : IPipelineBehavior<TQuery, PagedResponse<TModel>>
    where TModel : class
    where TQuery : notnull
{
    protected readonly SearchPipelineContext Context;

    /// <summary>
    /// Initializes a new instance of the <see cref="SearchPipelineBehavior{TQuery, TModel}"/>.
    /// </summary>
    protected SearchPipelineBehavior(SearchPipelineContext context, IEnumerable<IQueryContainerModifier<TModel>> modifiers)
    {
        Context = context;
        Modifiers = modifiers;
    }

    private IEnumerable<IQueryContainerModifier<TModel>> Modifiers { get; }

    /// <inheritdoc />
    public abstract Task<PagedResponse<TModel>> Handle(
        TQuery request,
        RequestHandlerDelegate<PagedResponse<TModel>> next,
        CancellationToken cancellationToken);

    /// <summary>
    /// Накладывает необходимые фильтры.
    /// </summary>
    protected QueryContainer Modify(QueryContainerDescriptor<TModel> query)
    {
        return Modifiers.Aggregate((QueryContainer)query, (acc, modifier) => acc & modifier.Modify(query));
    }
}