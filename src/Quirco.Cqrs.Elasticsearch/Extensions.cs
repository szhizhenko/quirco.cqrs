﻿using Microsoft.Extensions.DependencyInjection;
using Nest;
using Quirco.Cqrs.Elasticsearch.Behaviors;

namespace Quirco.Cqrs.Elasticsearch;

public static class Extensions
{
    public static IServiceCollection AddElasticsearch(this IServiceCollection services, IConnectionSettingsValues settings)
    {
        var client = new ElasticClient(settings);
        services.AddSingleton<IElasticClient>(client);
        services.AddScoped<SearchPipelineContext>();
        return services;
    }
}