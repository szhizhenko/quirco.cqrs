using System.Diagnostics;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Nest;
using Quirco.Cqrs.DataAccess.Models;
using Quirco.Cqrs.Domain.Models;
using Quirco.Cqrs.Elasticsearch.Behaviors;

namespace Quirco.Cqrs.Elasticsearch.Commands.IndexAll;

/// <summary>
/// Base command handler for indexing all entities.
/// </summary>
public abstract class IndexAllCommandHandler
<
    TCommand,
    TModel,
    TContext,
    TId
> : IRequestHandler<TCommand>
    where TCommand : IndexAllCommand
    where TModel : class, IHasId<TId>, new()
    where TContext : DbContext
{
    private readonly ILogger _logger;
    private readonly SearchPipelineContext _searchContext;

    /// <summary>
    /// Инициализирует новый экземпляр класса
    /// </summary>
    protected IndexAllCommandHandler(
        TContext context,
        IMediator mediator,
        IElasticClient elasticClient,
        SearchPipelineContext searchContext,
        ILogger logger)
    {
        _logger = logger;
        _searchContext = searchContext;
        Context = context;
        Mediator = mediator;
        Client = elasticClient;
    }

    private TContext Context { get; }

    private IMediator Mediator { get; }

    private IElasticClient Client { get; }

    /// <inheritdoc />
    public async Task Handle(TCommand request, CancellationToken cancellationToken)
    {
        // Drop existing index
        var indexDescriptor =
            await Client.Indices.ExistsAsync(new IndexExistsRequest(GetIndexName()), cancellationToken);

        if (indexDescriptor.Exists)
        {
            await Client.Indices.DeleteAsync(new DeleteIndexRequest(GetIndexName()), cancellationToken);
        }

        // Create new one
        await Client.Indices.CreateAsync(GetIndexName(), IndexDescriptorBuilder, cancellationToken);

        // Feed index with data
        var startFrom = 0;
        var processed = 0;
        _searchContext.SkipElasticsearch = true;

        do
        {
            var query = BuildQuery();
            query.Offset = startFrom;
            query.Limit = request.BatchSize;
            var res = await Mediator.Send(query, cancellationToken);

            _logger.LogInformation("Processing entries from {Start}/{Total}, limit: {Limit}...",
                                   startFrom,
                                   res.Count,
                                   query.Limit);

            var sw = new Stopwatch();
            sw.Start();
            await Client.IndexManyAsync(res.Results, cancellationToken: cancellationToken, index: GetIndexName());
            _logger.LogInformation("Processed in {Elapsed} ms", sw.ElapsedMilliseconds);

            processed += res.Results.Length;
            startFrom = processed;

            if (processed >= res.Count)
                break;
        } while (true);

        _logger.LogInformation("Total records processed: {RecordsProcessed}", processed);
    }

    /// <summary>
    /// Describes elasticsearch index
    /// </summary>
    protected virtual ICreateIndexRequest? IndexDescriptorBuilder(CreateIndexDescriptor request)
    {
        return null;
    }

    /// <summary>
    /// Фабрика для построения запроса на получение списка моделей
    /// </summary>
    protected abstract ListQuery<TModel, TId> BuildQuery();

    /// <summary>
    /// Returns index name, defaulting to model type name
    /// </summary>
    protected virtual string GetIndexName()
    {
        var type = typeof(TModel);

        return type.DeclaringType != null
            ? $"{type.DeclaringType.Name}.{type.Name}"
            : type.Name;
    }
}