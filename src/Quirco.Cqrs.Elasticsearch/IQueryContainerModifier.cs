using Nest;

namespace Quirco.Cqrs.Elasticsearch;

/// <summary>
/// Elasticsearch query modifier
/// </summary>
public interface IQueryContainerModifier<TModel>
    where TModel : class
{
    /// <summary>
    /// Модифицировать исходной запрос.
    /// </summary>
    /// <param name="query"></param>
    /// <returns></returns>
    QueryContainer Modify(QueryContainerDescriptor<TModel> query);
}