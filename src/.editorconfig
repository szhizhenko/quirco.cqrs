[*.cs]

### Align

# Align multiline constructs
resharper_align_first_arg_by_paren = false

# Align even if the resulting indentation is too large
resharper_csharp_allow_far_alignment = false
resharper_allow_far_alignment = false

resharper_csharp_alignment_tab_fill_style = optimal_fill
resharper_alignment_tab_fill_style = optimal_fill

resharper_csharp_align_multiline_parameter = false
resharper_align_multiline_parameter = false
resharper_csharp_align_multiline_calls_chain = false
resharper_align_multiline_calls_chain = false
resharper_csharp_align_multiline_argument = true
resharper_align_multiline_argument = true

### BRACES

# Block under "case" label
resharper_csharp_case_block_braces                                                = next_line

#### NEW LINES

# attribute always on new line
resharper_place_attribute_on_same_line = false

# After statements with child blocks
resharper_csharp_blank_lines_after_block_statements = 1
resharper_blank_lines_after_block_statements = 1

# Before multiline statements
resharper_csharp_blank_lines_before_multiline_statements = 1
resharper_blank_lines_before_multiline_statements = 1

# After multiline statements
resharper_csharp_blank_lines_after_multiline_statements = 1
resharper_blank_lines_after_multiline_statements = 1

# Blank lines in declarations
resharper_csharp_keep_blank_lines_in_declarations = 1
resharper_keep_blank_lines_in_declarations = 1

# new line before if
resharper_csharp_blank_lines_before_control_transfer_statements = 1
# new line after if
resharper_csharp_blank_lines_after_control_transfer_statements = 1

# new line before block of code
resharper_csharp_blank_lines_before_block_statements = 1
# new line after block of code
resharper_csharp_blank_lines_after_block_statements = 1

# Around multiline 'case' section in switch statement
resharper_csharp_blank_lines_around_multiline_case_section = 1

# Around single line property/event
resharper_csharp_blank_lines_around_single_line_property = 1
resharper_blank_lines_around_single_line_property = 1
resharper_csharp_blank_lines_around_property = 1
resharper_csharp_blank_lines_around_auto_property = 1
resharper_csharp_blank_lines_around_single_line_auto_property = 1

### LINE BREAKS
# Place simple embedded statement on the same line
resharper_place_simple_embedded_statement_on_same_line = false

# Wrap invocation arguments
resharper_csharp_wrap_arguments_style = chop_if_long
resharper_wrap_arguments_style = chop_if_long

# Max invocation arguments on a single line
resharper_csharp_max_invocation_arguments_on_line = 3
resharper_max_invocation_arguments_on_line = 3

# Prefer to wrap after '(' in invocation
resharper_csharp_wrap_after_invocation_lpar = false
resharper_wrap_after_invocation_lpar = false


# Prefer wrap before '=>' followed by expressions
resharper_csharp_wrap_before_arrow_with_expressions = true
resharper_wrap_before_arrow_with_expressions = true

## Arrangement of method signatures
resharper_csharp_wrap_parameters_style = chop_if_long

# Max formal parameters on a single line
resharper_csharp_max_formal_parameters_on_line = 3
resharper_max_formal_parameters_on_line = 3

# refer to wrap after '(' in declaration
resharper_csharp_wrap_after_declaration_lpar = true

# Wrap chained method calls
resharper_csharp_wrap_chained_method_calls = chop_if_long

# Prefer wrap before '?' and ':' in ternary expressions
resharper_csharp_wrap_before_ternary_opsigns = true

# Wrap ternary expression
resharper_wrap_ternary_expr_style = chop_always

# csharp_style_var_for_built_in_types=true:silent
# csharp_style_var_when_type_is_apparent=true:silent
# csharp_style_var_elsewhere=true:silent

#############
# Analyzers #
#############

dotnet_diagnostic.CA1810.severity = error # Инициализируйте статические поля ссылочных типов при объявлении
dotnet_diagnostic.CA1822.severity = error # Пометьте члены как статические

##
## StyleCop.Analyzers
##

# Using directive should appear within a namespace declaration
dotnet_diagnostic.SA1200.severity = None

# XML comment analysis is disabled due to project configuration
dotnet_diagnostic.SA0001.severity = None

# The file header is missing or not located at the top of the file
dotnet_diagnostic.SA1633.severity = None

# Use string.Empty for empty strings
dotnet_diagnostic.SA1122.severity = None

# Variable '_' should begin with lower-case letter
dotnet_diagnostic.SA1312.severity = None

# Parameter '_' should begin with lower-case letter
dotnet_diagnostic.SA1313.severity = None

# Elements should be documented
dotnet_diagnostic.SA1600.severity = None

# Prefix local calls with this
dotnet_diagnostic.SA1101.severity = None

# 'public' members should come before 'private' members
dotnet_diagnostic.SA1202.severity = None

# Comments should contain text
dotnet_diagnostic.SA1120.severity = None

dotnet_diagnostic.SA1028.severity = Error

# Constant fields should appear before non-constant fields
dotnet_diagnostic.SA1203.severity = None

# Field '_blah' should not begin with an underscore
dotnet_diagnostic.SA1309.severity = None

# Use trailing comma in multi-line initializers
dotnet_diagnostic.SA1413.severity = None

# A method should not follow a class
dotnet_diagnostic.SA1201.severity = None

# Elements should be separated by blank line
dotnet_diagnostic.SA1516.severity = None

# The parameter spans multiple lines
dotnet_diagnostic.SA1118.severity = None

# Static members should appear before non-static members
dotnet_diagnostic.SA1204.severity = None

# Put constructor initializers on their own line
dotnet_diagnostic.SA1128.severity = None

# Opening braces should not be preceded by blank line
dotnet_diagnostic.SA1509.severity = None

# The parameter should begin on the line after the previous parameter
dotnet_diagnostic.SA1115.severity = None

# File name should match first type name
dotnet_diagnostic.SA1649.severity = None

# File may only contain a single type
dotnet_diagnostic.SA1402.severity = warning

# Enumeration items should be documented
dotnet_diagnostic.SA1602.severity = None

# Element should not be on a single line
dotnet_diagnostic.SA1502.severity = Error

# Closing parenthesis should not be preceded by a space
dotnet_diagnostic.SA1009.severity = None

# Closing parenthesis should be on line of last parameter
dotnet_diagnostic.SA1111.severity = None

# Braces should not be ommitted
dotnet_diagnostic.SA1503.severity = None

##
## SonarAnalyzers.CSharp
##

# Update this method so that its implementation is not identical to 'blah'
dotnet_diagnostic.S4144.severity = None

# Update this implementation of 'ISerializable' to conform to the recommended serialization pattern
dotnet_diagnostic.S3925.severity = None

# Rename class 'IOCActivator' to match pascal case naming rules, consider using 'IocActivator'
dotnet_diagnostic.S101.severity = None

# Extract this nested code block into a separate method
dotnet_diagnostic.S1199.severity = None

# Remove unassigned auto-property 'Blah', or set its value
dotnet_diagnostic.S3459.severity = None

# Remove the unused private set accessor in property 'Version'
dotnet_diagnostic.S1144.severity = None

# Remove this commented out code
dotnet_diagnostic.S125.severity = None

# 'System.Exception' should not be thrown by user code
dotnet_diagnostic.S112.severity = None

# Do not use regions
dotnet_diagnostic.SA1124.severity = Warning

# The XML documentation header for a C# constructor does not contain the appropriate summary text.
dotnet_diagnostic.SA1642.severity = None

# [S4035] Seal class 'EntityEnumValue' or implement 'IEqualityComparer<T>' instead.
dotnet_diagnostic.S4035.severity = Warning

# Complete TO-DO
dotnet_diagnostic.S1135.severity = Warning

# [CS8618] Non-nullable property 'Items' must contain a non-null value when exiting constructor. Consider declaring the property as nullable.
dotnet_diagnostic.CS8618.severity = None

# Obsolete method
dotnet_diagnostic.CS0618.severity = Error

# Nullable
dotnet_diagnostic.CS0407.severity = Error

# The property's documentation summary text should begin with: 'Gets'
dotnet_diagnostic.SA1623.severity = None

# Closing brace should be followed by blank line
dotnet_diagnostic.SA1513.severity = Error

# XML Comments for public members
dotnet_diagnostic.SA1591.severity = None

# [SA1507] Code should not contain multiple blank lines in a row
dotnet_diagnostic.SA1507.severity = Error

# [S2190] Add a way to break out of this property accessor's recursion.
dotnet_diagnostic.S2190.severity = Error

# [SA1116] The parameters should begin on the line after the declaration, whenever the parameter span across multiple lines
dotnet_diagnostic.SA1116.severity = silent

# [SA1003] Operator ':' should not appear at the end of a line.
dotnet_diagnostic.SA1003.severity = Error

# [SA1000] The keyword 'new' should not be followed by a space
dotnet_diagnostic.SA1000.severity = Error

# [SA1127] Generic type constraints should be on their own line
dotnet_diagnostic.SA1127.severity = Error

# Remove unused variables
dotnet_diagnostic.S1481.severity = Error

# Documentation text should end with a period
dotnet_diagnostic.SA1629.severity = None

# The documentation for type parameter 'TId' is missing
dotnet_diagnostic.SA1618.severity = None

# The documentation for parameter 'logger' is missing
dotnet_diagnostic.SA1611.severity = None

# Element return value should be documented
dotnet_diagnostic.SA1615.severity = None


dotnet_diagnostic.SA1408.severity = Error
csharp_indent_labels = one_less_than_current
csharp_using_directive_placement = outside_namespace:silent
csharp_prefer_simple_using_statement = true:error
csharp_prefer_braces = true:silent
csharp_style_namespace_declarations = file_scoped:error
csharp_style_prefer_method_group_conversion = true:silent
csharp_style_prefer_top_level_statements = true:silent
csharp_style_expression_bodied_methods = false:silent
csharp_style_expression_bodied_constructors = false:silent
csharp_style_expression_bodied_operators = false:silent
csharp_style_expression_bodied_properties = true:silent
csharp_style_expression_bodied_indexers = true:silent
csharp_style_expression_bodied_accessors = true:silent
csharp_style_expression_bodied_lambdas = true:silent
csharp_style_expression_bodied_local_functions = false:silent
csharp_space_around_binary_operators = before_and_after
dotnet_diagnostic.S1147.severity = error
dotnet_diagnostic.S2222.severity = error
dotnet_diagnostic.S2551.severity = error
dotnet_diagnostic.S2357.severity = error
csharp_style_throw_expression = true:suggestion
csharp_style_prefer_null_check_over_type_check = true:suggestion
csharp_style_prefer_local_over_anonymous_function = true:suggestion
csharp_prefer_simple_default_expression = true:suggestion
csharp_style_prefer_index_operator = true:suggestion
csharp_style_prefer_range_operator = true:suggestion
csharp_style_implicit_object_creation_when_type_is_apparent = true:suggestion
csharp_style_prefer_tuple_swap = true:suggestion
csharp_style_prefer_utf8_string_literals = true:suggestion
csharp_style_inlined_variable_declaration = true:suggestion
csharp_style_deconstructed_variable_declaration = true:suggestion
csharp_style_unused_value_assignment_preference = discard_variable:suggestion
csharp_style_unused_value_expression_statement_preference = discard_variable:silent
csharp_style_prefer_readonly_struct = true:suggestion
csharp_prefer_static_local_function = true:suggestion
csharp_style_allow_embedded_statements_on_same_line_experimental = true:silent
csharp_style_allow_blank_line_after_colon_in_constructor_initializer_experimental = true:silent
csharp_style_allow_blank_lines_between_consecutive_braces_experimental = true:silent
csharp_style_conditional_delegate_call = true:suggestion
csharp_style_prefer_switch_expression = true:suggestion
csharp_style_prefer_pattern_matching = true:silent
csharp_style_pattern_matching_over_is_with_cast_check = true:suggestion
csharp_style_pattern_matching_over_as_with_null_check = true:suggestion
csharp_style_prefer_not_pattern = true:suggestion
csharp_style_prefer_extended_property_pattern = true:suggestion
csharp_style_var_for_built_in_types = true:silent
csharp_style_var_when_type_is_apparent = true:silent
csharp_style_var_elsewhere = true:silent
dotnet_diagnostic.SA1012.severity = error
dotnet_diagnostic.SA1505.severity = error
dotnet_diagnostic.S2933.severity = error
dotnet_diagnostic.SA1508.severity = error
dotnet_diagnostic.SA1400.severity = error
dotnet_diagnostic.SA1117.severity = error
dotnet_diagnostic.SA1024.severity = error
dotnet_diagnostic.SA1025.severity = error
dotnet_diagnostic.SA1026.severity = error
[*.{cs,vb}]
#### Naming styles ####

# Naming rules

dotnet_naming_rule.interface_should_be_begins_with_i.severity = suggestion
dotnet_naming_rule.interface_should_be_begins_with_i.symbols = interface
dotnet_naming_rule.interface_should_be_begins_with_i.style = begins_with_i

dotnet_naming_rule.types_should_be_pascal_case.severity = suggestion
dotnet_naming_rule.types_should_be_pascal_case.symbols = types
dotnet_naming_rule.types_should_be_pascal_case.style = pascal_case

dotnet_naming_rule.non_field_members_should_be_pascal_case.severity = suggestion
dotnet_naming_rule.non_field_members_should_be_pascal_case.symbols = non_field_members
dotnet_naming_rule.non_field_members_should_be_pascal_case.style = pascal_case

# Symbol specifications

dotnet_naming_symbols.interface.applicable_kinds = interface
dotnet_naming_symbols.interface.applicable_accessibilities = public, internal, private, protected, protected_internal, private_protected
dotnet_naming_symbols.interface.required_modifiers =

dotnet_naming_symbols.types.applicable_kinds = class, struct, interface, enum
dotnet_naming_symbols.types.applicable_accessibilities = public, internal, private, protected, protected_internal, private_protected
dotnet_naming_symbols.types.required_modifiers =

dotnet_naming_symbols.non_field_members.applicable_kinds = property, event, method
dotnet_naming_symbols.non_field_members.applicable_accessibilities = public, internal, private, protected, protected_internal, private_protected
dotnet_naming_symbols.non_field_members.required_modifiers =

# Naming styles

dotnet_naming_style.begins_with_i.required_prefix = I
dotnet_naming_style.begins_with_i.required_suffix =
dotnet_naming_style.begins_with_i.word_separator =
dotnet_naming_style.begins_with_i.capitalization = pascal_case

dotnet_naming_style.pascal_case.required_prefix =
dotnet_naming_style.pascal_case.required_suffix =
dotnet_naming_style.pascal_case.word_separator =
dotnet_naming_style.pascal_case.capitalization = pascal_case

dotnet_naming_style.pascal_case.required_prefix =
dotnet_naming_style.pascal_case.required_suffix =
dotnet_naming_style.pascal_case.word_separator =
dotnet_naming_style.pascal_case.capitalization = pascal_case
dotnet_style_operator_placement_when_wrapping = beginning_of_line
tab_width = 4
indent_size = 4
end_of_line = crlf
dotnet_style_coalesce_expression = true:suggestion
dotnet_style_null_propagation = true:suggestion
dotnet_style_prefer_is_null_check_over_reference_equality_method = true:suggestion
dotnet_style_prefer_auto_properties = true:silent
dotnet_style_object_initializer = true:suggestion
dotnet_style_collection_initializer = true:suggestion
dotnet_style_prefer_simplified_boolean_expressions = true:suggestion
dotnet_style_prefer_conditional_expression_over_assignment = true:silent
dotnet_style_prefer_conditional_expression_over_return = true:silent
dotnet_style_explicit_tuple_names = true:suggestion
dotnet_style_prefer_inferred_tuple_names = true:suggestion
dotnet_style_prefer_inferred_anonymous_type_member_names = true:suggestion
dotnet_style_prefer_compound_assignment = true:suggestion
dotnet_style_prefer_simplified_interpolation = true:suggestion
dotnet_diagnostic.CA1002.severity = error
dotnet_style_namespace_match_folder = true:suggestion
dotnet_style_readonly_field = true:suggestion
dotnet_style_predefined_type_for_member_access = true:silent
dotnet_style_predefined_type_for_locals_parameters_members = true:silent
dotnet_style_require_accessibility_modifiers = for_non_interface_members:silent
dotnet_style_allow_multiple_blank_lines_experimental = true:silent
dotnet_style_allow_statement_immediately_after_block_experimental = true:silent
dotnet_code_quality_unused_parameters = all:suggestion
dotnet_style_parentheses_in_arithmetic_binary_operators = always_for_clarity:silent
dotnet_style_parentheses_in_other_binary_operators = always_for_clarity:silent
dotnet_style_parentheses_in_relational_binary_operators = always_for_clarity:silent
dotnet_style_parentheses_in_other_operators = never_if_unnecessary:silent
dotnet_style_qualification_for_field = false:silent
dotnet_style_qualification_for_property = false:silent
dotnet_style_qualification_for_method = false:silent
dotnet_style_qualification_for_event = false:silent

[*.razor]
dotnet_diagnostic.RZ10012.severity = Error