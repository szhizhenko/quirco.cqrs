﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Migrations.Internal;

namespace Quirco.Cqrs.DataAccess.Npgsql.DataContext;

/// <summary>
/// Context to support different naming conventions
/// </summary>
public class CustomNamingConventionHistoryContext : NpgsqlHistoryRepository
{
    public CustomNamingConventionHistoryContext(HistoryRepositoryDependencies dependencies) : base(dependencies)
    {
    }

    /// <summary>
    /// Configuration for migration table
    /// </summary>
    protected override void ConfigureTable(EntityTypeBuilder<HistoryRow> history)
    {
        base.ConfigureTable(history);
        history.Property(h => h.MigrationId).HasColumnName("MigrationId");
        history.Property(h => h.ProductVersion).HasColumnName("ProductVersion");
    }
}