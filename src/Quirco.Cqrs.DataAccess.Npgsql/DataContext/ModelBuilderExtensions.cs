﻿using Microsoft.EntityFrameworkCore;

namespace Quirco.Cqrs.DataAccess.Npgsql.DataContext;

public static class ModelBuilderExtensions
{
    public static void SetCaseInsensitive(this ModelBuilder modelBuilder)
    {
        modelBuilder.HasPostgresExtension("citext");
        foreach (var entity in modelBuilder.Model.GetEntityTypes())
        {
            foreach (var property in entity.GetProperties())
            {
                if (property.ClrType == typeof(string))
                {
                    var entityTypeBuilder = modelBuilder.Entity(entity.ClrType);
                    entityTypeBuilder.Property(property.ClrType, property.Name).HasColumnType("citext");
                }
            }
        }
    }
}
