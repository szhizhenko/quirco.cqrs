﻿namespace Quirco.Cqrs.Auditor.Core.Interfaces;

/// <summary>
/// CQRS-command that intended to be logged with special auditing details.
/// </summary>
public interface IAuditableCommand
{
}