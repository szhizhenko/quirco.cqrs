﻿using Quirco.Cqrs.Core.Models;

namespace Quirco.Cqrs.Auditor.Core.Models;

/// <summary>
/// Filter for searching audit events in store
/// </summary>
public class AuditEventFilter : LimitQuery
{
    /// <summary>
    /// "Parent" or owner entity type
    /// </summary>
    public string? EntityType { get; set; }

    /// <summary>
    /// ID for parent entity
    /// </summary>
    public string? EntityId { get; set; }

    /// <summary>
    /// Type of entity this event is directly related to
    /// </summary>
    public string? InnerEntityType { get; set; }

    /// <summary>
    /// ID of entity this event is directly related to
    /// </summary>
    public string? InnerEntityId { get; set; }

    /// <summary>
    /// Start date of event (inclusive)
    /// </summary>
    public DateTime? DateFrom { get; set; }

    /// <summary>
    /// End date of event (exclusive)
    /// </summary>
    public DateTime? DateTo { get; set; }
}