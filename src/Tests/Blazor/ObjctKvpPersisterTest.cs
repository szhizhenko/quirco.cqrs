﻿using FluentAssertions;
using Quirco.Cqrs.Blazor.Filters;
using Xunit;

namespace Tests.Blazor;

public class ObjctKvpPersisterTest
{
    public class TestObject
    {
        public int Id { get; set; }

        public DateTime? DateTime { get; set; }

        public string[] SomeValues { get; set; }

        public string? StringValue { get; set; }

        public int[]? SomeNullable { get; set; }
    }

    [Fact]
    public void TestPersistence()
    {
        // Arrange
        var obj = new TestObject
        {
            Id = 1,
            DateTime = DateTime.Today,
            SomeValues = new[] { "s1", "s2", "s 3" },
            StringValue = "str"
        };

        // Act
        var kvp = ObjectKvpJsonPersister.Persist("f", obj).ToArray();

        // Assert
        kvp.Should().BeEquivalentTo(new[]
        {
            new KeyValuePair<string, string?>("f.Id", "1"),
            new KeyValuePair<string, string?>("f.DateTime", DateTime.Today.ToString("O")),
            new KeyValuePair<string, string?>("f.SomeValues", "[\"s1\",\"s2\",\"s 3\"]"),
            new KeyValuePair<string, string?>("f.StringValue", "str"),
            new KeyValuePair<string, string?>("f.SomeNullable", null),
        });
    }

    [Fact]
    public void TestPersistenceWithoutPrefix()
    {
        // Arrange
        var obj = new TestObject
        {
            Id = 1,
            DateTime = DateTime.Today,
            SomeValues = new[] { "s1", "s2", "s 3" },
            StringValue = "str"
        };

        // Act
        var kvp = ObjectKvpJsonPersister.Persist(null, obj).ToArray();

        // Assert
        kvp.Should().BeEquivalentTo(new[]
        {
            new KeyValuePair<string, string?>("Id", "1"),
            new KeyValuePair<string, string?>("DateTime", DateTime.Today.ToString("O")),
            new KeyValuePair<string, string?>("SomeValues", "[\"s1\",\"s2\",\"s 3\"]"),
            new KeyValuePair<string, string?>("StringValue", "str"),
            new KeyValuePair<string, string?>("SomeNullable", null),
        });
    }

    [Fact]
    public void TestRestore()
    {
        // Arrange
        var restored = new TestObject
        {
            SomeNullable = new[] { 1 }
        };

        // Act
        ObjectKvpJsonPersister.Restore("f", restored, new[]
        {
            new KeyValuePair<string, string?>("f.Id", "1"),
            new KeyValuePair<string, string?>("f.DateTime", DateTime.Today.ToString("O")),
            new KeyValuePair<string, string?>("f.SomeValues", "[\"s1\",\"s2\",\"s 3\"]"),
            new KeyValuePair<string, string?>("f.SomeNullable", null),
        });

        // Assert
        restored.Should().BeEquivalentTo(new TestObject
        {
            Id = 1,
            DateTime = DateTime.Today,
            SomeValues = new[] { "s1", "s2", "s 3" },
            SomeNullable = null
        });
    }
}