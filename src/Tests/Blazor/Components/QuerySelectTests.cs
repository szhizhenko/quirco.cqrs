using AntDesign.Select.Internal;
using Bunit;
using FluentAssertions;
using MediatR;
using Quirco.Cqrs.Blazor.Components.Forms;
using Tests.Base;
using Tests.Data;
using Tests.Domain.Models;
using Tests.Domain.Queries;
using Xunit;

namespace Tests.Blazor.Components;

public class QuerySelectTests : BaseBlazorTest
{
    [Fact]
    public async Task Value_Should_NotBeEmpty_When_DefaultActiveFirstOption()
    {
        // Arrange
        await Context.Persons.AddRangeAsync(
            new Person
            {
                Name = "Sergey",
                Age = 39
            }
        );
        await Context.SaveChangesAsync();

        var blazorContext = GetService<TestContext>();

        // Act
        var querySelect =
            blazorContext.RenderComponent<QuerySelect<PersonSlimModel, int?, SearchPersonsQuery>>(
                parameters => { parameters.Add(x => x.DefaultActiveFirstOption, true); });

        querySelect.WaitForState(() => querySelect.Instance.Value > 0);

        // Assert
        querySelect.Instance.Value.Should().HaveValue();
        querySelect.Instance.InnerItems.Should().NotBeEmpty();
    }

    [Fact]
    public async Task QuerySelect_Should_NotBeEmpty_When_Has_Value()
    {
        // Arrange
        var blazorContext = GetService<TestContext>();

        var user = new UserGuid
        {
            Name = "Sergey",
            Id = Guid.NewGuid()
        };
        await Context.Users.AddAsync(user);
        await Context.SaveChangesAsync();

        // Act
        var querySelect =
            blazorContext.RenderComponent<QuerySelectBase<SearchUsersGuid.Model, Guid, Guid, SearchUsersGuid.Query>>(
                parameters => { parameters.Add(x => x.Value, user.Id); });

        // Assert
        querySelect.Instance.InnerItems.Should().NotBeEmpty();
    }

    [Fact]
    public async Task QuerySelect_Should_NotBeEmpty_When_Has_Values()
    {
        // Arrange
        var blazorContext = GetService<TestContext>();

        var users = new UserGuid[]
        {
            new()
            {
                Name = "Sergey",
                Id = Guid.NewGuid()
            },
            new()
            {
                Name = "Sergey2",
                Id = Guid.NewGuid()
            }
        };
        await Context.Users.AddRangeAsync(users);
        await Context.SaveChangesAsync();

        // Act
        var querySelect =
            blazorContext.RenderComponent<QuerySelectBase<SearchUsersGuid.Model, Guid, Guid, SearchUsersGuid.Query>>(
                parameters =>
                {
                    parameters.Add(x => x.Mode, SelectMode.Multiple);
                    parameters.Add(x => x.Values, users.Select(x => x.Id).ToArray());
                });

        // Assert
        querySelect.Instance.InnerItems.Should().NotBeEmpty().And.HaveCount(2);
    }

    [Fact]
    public async Task QuerySelect_Should_NotBeEmpty_When_Has_InitialValues()
    {
        // Arrange
        var blazorContext = GetService<TestContext>();

        await Context.Users.AddRangeAsync(new UserGuid[]
        {
            new()
            {
                Name = "Sergey",
                Id = Guid.NewGuid()
            },
            new()
            {
                Name = "Sergey2",
                Id = Guid.NewGuid()
            }
        });

        await Context.SaveChangesAsync();

        var mediator = GetService<IMediator>();
        var users = await mediator.Send(new SearchUsersGuid.Query());

        // Act
        var querySelect =
            blazorContext.RenderComponent<QuerySelectBase<SearchUsersGuid.Model, Guid, Guid, SearchUsersGuid.Query>>(
                parameters =>
                {
                    parameters.Add(x => x.Mode, SelectMode.Multiple);
                    parameters.Add(x => x.InitialValues, users.Results);
                });

        // Assert
        querySelect.Instance.InnerItems.Should().NotBeEmpty();
    }

    [Fact]
    public async Task QuerySelect_Should_NotBeEmpty_When_Has_InitialValue()
    {
        // Arrange
        var blazorContext = GetService<TestContext>();

        await Context.Users.AddRangeAsync(new UserGuid[]
        {
            new()
            {
                Name = "Sergey",
                Id = Guid.NewGuid()
            }
        });

        await Context.SaveChangesAsync();

        var mediator = GetService<IMediator>();
        var users = await mediator.Send(new SearchUsersGuid.Query());

        // Act
        var querySelect =
            blazorContext.RenderComponent<QuerySelectBase<SearchUsersGuid.Model, Guid, Guid, SearchUsersGuid.Query>>(
                parameters => { parameters.Add(x => x.InitialValue, users.Results.First()); });

        // Assert
        querySelect.Instance.InnerItems.Should().NotBeEmpty();
    }
}