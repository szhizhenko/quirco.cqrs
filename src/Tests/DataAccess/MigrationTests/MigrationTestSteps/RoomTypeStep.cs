﻿using Quirco.Cqrs.DataAccess.DataMigration.Attributes;
using Tests.DataAccess.MigrationTests.MigrationTestSteps.Entities;

namespace Tests.DataAccess.MigrationTests.MigrationTestSteps;

#if NET6_0
[DependsOnEntry(typeof(Facility))]
#else
[DependsOnEntry<Facility>]
#endif
public class RoomTypeStep : BaseTestStep
{
}