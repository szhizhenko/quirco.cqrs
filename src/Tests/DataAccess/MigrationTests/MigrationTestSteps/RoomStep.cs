﻿using Quirco.Cqrs.DataAccess.DataMigration.Attributes;
using Tests.DataAccess.MigrationTests.MigrationTestSteps.Entities;

namespace Tests.DataAccess.MigrationTests.MigrationTestSteps;

[ProducesEntry(typeof(Room))]
public class RoomStep : BaseTestStep
{
}