﻿using Quirco.Cqrs.DataAccess.DataMigration.Interfaces;
using Quirco.Cqrs.DataAccess.DataMigration.Models;

namespace Tests.DataAccess.MigrationTests.MigrationTestSteps;

public class BaseTestStep : IDataMigrationStep
{
    public virtual ValueTask<DataMigrationStepResult> Execute(CancellationToken cancellationToken)
    {
        return ValueTask.FromResult(new DataMigrationStepResult());
    }
}