﻿using Quirco.Cqrs.DataAccess.DataMigration.Attributes;

namespace Tests.DataAccess.MigrationTests.MigrationTestSteps;

#if NET6_0
[ProducesEntry(typeof(Entities.Facility))]
#else
[ProducesEntry<Entities.Facility>]
#endif
public class FacilityStep : BaseTestStep
{
}