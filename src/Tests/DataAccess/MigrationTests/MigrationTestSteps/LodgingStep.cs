﻿using Quirco.Cqrs.DataAccess.DataMigration.Attributes;
using Tests.DataAccess.MigrationTests.MigrationTestSteps.Entities;

namespace Tests.DataAccess.MigrationTests.MigrationTestSteps;

[ProducesEntry(typeof(Lodging))]
[DependsOnEntry(typeof(Facility))]
public class LodgingStep : BaseTestStep
{
}