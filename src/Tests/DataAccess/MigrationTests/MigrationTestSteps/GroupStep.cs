﻿using Quirco.Cqrs.DataAccess.DataMigration.Attributes;
using Tests.DataAccess.MigrationTests.MigrationTestSteps.Entities;

namespace Tests.DataAccess.MigrationTests.MigrationTestSteps;

[DependsOnEntry(typeof(Inventory))]
public class GroupStep : BaseTestStep
{
}