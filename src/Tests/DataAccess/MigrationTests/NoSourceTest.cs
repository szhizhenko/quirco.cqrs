﻿using FluentAssertions;
using Microsoft.Extensions.Logging;
using NSubstitute;
using Quirco.Cqrs.DataAccess.DataMigration;
using Quirco.Cqrs.DataAccess.DataMigration.Attributes;
using Quirco.Cqrs.DataAccess.DataMigration.Exceptions;
using Quirco.Cqrs.DataAccess.DataMigration.Interfaces;
using Quirco.Cqrs.DataAccess.DataMigration.Models;
using Xunit;

namespace Tests.DataAccess.MigrationTests;

public class NoSourceTests
{
    [Fact]
    public void NoSourceTest()
    {
        // Arrange
        var steps = new[] { typeof(Step1) };

        // Act
        var action = () => steps.OrderByDependencies().ToList();

        // Assert
        action.Should().Throw<DataMigrationMissingSourceException>();
    }

    [DependsOnEntry(typeof(string))]
    private class Step1 : IDataMigrationStep
    {
        public ValueTask<DataMigrationStepResult> Execute(CancellationToken cancellationToken)
        {
            return ValueTask.FromResult(new DataMigrationStepResult());
        }
    }

    [Fact]
    public void ApiPutGetTest()
    {
        // Arrange
        var map = new DataMigrationContext(Substitute.For<ILogger<DataMigrationContext>>());

        // Act
        var obj = new Step1();
        map.AddMap(1, obj);
        var res = map.GetFromMapRequired<Step1>(1);

        // Assert
        res.Should().BeSameAs(obj);
    }

    [Fact]
    public void ApiNullabilityTest()
    {
        // Arrange
        var map = new DataMigrationContext(Substitute.For<ILogger<DataMigrationContext>>());

        // Act
        var res = map.GetFromMap<Step1>((long?)null);

        // Assert
        res.Should().BeNull();
    }

    [Fact]
    public void ApiNullabilityTest2()
    {
        // Arrange
        var map = new DataMigrationContext(Substitute.For<ILogger<DataMigrationContext>>());
        int? id = 1;

        // Act
        var obj = new Step1();
        map.AddMap(id.Value, obj);
        var res = map.GetFromMap<Step1>(id);

        // Assert
        res.Should().BeSameAs(obj);
    }

    [Fact]
    public void GetAllEntriesTest()
    {
        // Arrange
        var map = new DataMigrationContext(Substitute.For<ILogger<DataMigrationContext>>());

        // Act
        var obj = new Step1();
        map.AddMap(1, obj);
        map.AddMap(2, obj);
        var res = map.GetAllEntries<Step1>();

        // Assert
        res.Should().HaveCount(2);
    }
}