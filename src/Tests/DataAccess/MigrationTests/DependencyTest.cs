﻿using FluentAssertions;
using Quirco.Cqrs.DataAccess.DataMigration.Interfaces;
using Tests.DataAccess.MigrationTests.MigrationTestSteps;
using Xunit;

namespace Tests.DataAccess.MigrationTests;

public class DependencyTest
{
    [Fact]
    public void TestDependencies()
    {
        // Arrange
        var steps = new[]
        {
            typeof(LodgingStep),
            typeof(RoomTypeStep),
            typeof(FacilityStep),
            typeof(GroupStep),
            typeof(RoomStep)
        };

        // Act
        var ordered = steps.OrderByDependencies().ToList();

        // Assert
        ordered.Should().HaveSameCount(steps);
        ordered.Should()
            .BeEquivalentTo(new[]
            {
                typeof(FacilityStep),
                typeof(RoomTypeStep),
                typeof(LodgingStep),
                typeof(RoomStep),
                typeof(GroupStep)
            });
    }
}