﻿using FluentAssertions;
using Quirco.Cqrs.DataAccess.DataMigration;
using Quirco.Cqrs.DataAccess.DataMigration.Attributes;
using Quirco.Cqrs.DataAccess.DataMigration.Exceptions;
using Quirco.Cqrs.DataAccess.DataMigration.Interfaces;
using Quirco.Cqrs.DataAccess.DataMigration.Models;
using Xunit;

namespace Tests.DataAccess.MigrationTests;

public class SimpleCircularDependencyTests
{
    [Fact]
    public void CircularDependencyTest()
    {
        // Arrange
        var steps = new[] { typeof(Step1), typeof(Step2) };

        // Act
        var action = () => steps.OrderByDependencies().ToList();

        // Assert
        action.Should().Throw<DataMigrationCircularDependencyException>();
    }

    [ProducesEntry(typeof(int))]
    [DependsOnEntry(typeof(long))]
    private class Step1 : IDataMigrationStep
    {
        public ValueTask<DataMigrationStepResult> Execute(CancellationToken cancellationToken)
        {
            return ValueTask.FromResult(new DataMigrationStepResult());
        }
    }

    [ProducesEntry(typeof(long))]
    [DependsOnEntry(typeof(int))]
    private class Step2 : IDataMigrationStep
    {
        public ValueTask<DataMigrationStepResult> Execute(CancellationToken cancellationToken)
        {
            return ValueTask.FromResult(new DataMigrationStepResult());
        }
    }
}