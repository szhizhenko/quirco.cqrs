﻿using FluentAssertions;
using Quirco.Cqrs.DataAccess.DataMigration.Attributes;
using Quirco.Cqrs.DataAccess.DataMigration.Exceptions;
using Quirco.Cqrs.DataAccess.DataMigration.Interfaces;
using Xunit;

namespace Tests.DataAccess.MigrationTests;

public class CircularDependencyTests
{
    [Fact]
    public void ComplexCircularDependencyTest1()
    {
        // Arrange
        var steps = new[]
        {
            typeof(Step1),
            typeof(Step2),
            typeof(Step3)
        };

        // Act
        var action = () => steps.OrderByDependencies().ToList();

        // Assert
        action.Should().Throw<DataMigrationCircularDependencyException>();
    }

    [ProducesEntry(typeof(int))]
    [DependsOnEntry(typeof(long))]
    public class Step1
    {
    }

    [ProducesEntry(typeof(long))]
    [DependsOnEntry(typeof(string))]
    public class Step2
    {
    }

    [ProducesEntry(typeof(string))]
    [DependsOnEntry(typeof(int))]
    public class Step3
    {
    }
}