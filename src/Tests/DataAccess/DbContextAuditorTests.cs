﻿using FluentAssertions;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Tests.Data;
using Xunit;

namespace Tests.DataAccess;

public class DbContextAuditorTests
{
    private readonly ServiceProvider _serviceProvider;

    public DbContextAuditorTests()
    {
        _serviceProvider = new ServiceCollection()
            .AddDbContextFactory<TestDbContext>(o => o.UseInMemoryDatabase(Guid.NewGuid().ToString()))
            .AddMediatR(cfg => cfg.RegisterServicesFromAssembly(GetType().Assembly))
            .BuildServiceProvider();
    }

    [Fact]
    public async Task TestInsertion()
    {
        // Arrange
        var contextFactory = _serviceProvider.GetRequiredService<IDbContextFactory<TestDbContext>>();
        var ctx = await contextFactory.CreateDbContextAsync();

        // Act
        ctx.Persons.Add(new Person
        {
            Name = "Sergio"
        });

        await ctx.SaveChangesAsync();
        var persons = await ctx.Persons.ToListAsync();

        // Assert
        persons.Should().NotBeEmpty();
        persons.Should().HaveCount(1);
        var person = persons.First();
        person.Id.Should().BeGreaterThan(0);
        person.UserCreatedId.Should().Be(1);
    }

    [Fact]
    public async Task TestDeletion()
    {
        // Arrange
        var contextFactory = _serviceProvider.GetRequiredService<IDbContextFactory<TestDbContext>>();
        var ctx = await contextFactory.CreateDbContextAsync();

        ctx.Persons.Add(new Person
        {
            Name = "Sergio"
        });

        await ctx.SaveChangesAsync();

        // Act
        var person = await ctx.Persons.FirstAsync();
        ctx.Persons.Remove(person);
        await ctx.SaveChangesAsync();
        person = await ctx.Persons.IgnoreQueryFilters().FirstOrDefaultAsync();

        // Assert
        person.Should().NotBeNull();
        person.DateDeleted.Should().NotBeNull();
    }

    [Fact]
    public async Task TestInheritedEntityDeletion()
    {
        // Arrange
        var contextFactory = _serviceProvider.GetRequiredService<IDbContextFactory<TestDbContext>>();
        var ctx = await contextFactory.CreateDbContextAsync();

        ctx.Employees.Add(new Employee
        {
            Name = "Sergio",
            WorkPlace = "Job"
        });

        await ctx.SaveChangesAsync();

        // Act
        var employee = await ctx.Employees.FirstAsync();
        ctx.Employees.Remove(employee);
        await ctx.SaveChangesAsync();
        employee = await ctx.Employees.IgnoreQueryFilters().FirstOrDefaultAsync();

        // Assert
        employee.Should().NotBeNull();
        employee.DateDeleted.Should().NotBeNull();
    }

    [Fact]
    public async Task TestIsActiveFilter()
    {
        // Arrange
        var contextFactory = _serviceProvider.GetRequiredService<IDbContextFactory<TestDbContext>>();
        var ctx = await contextFactory.CreateDbContextAsync();

        ctx.Manufacturers.Add(new Manufacturer
        {
            Name = "First",
            IsActive = true
        });

        ctx.Manufacturers.Add(new Manufacturer
        {
            Name = "Second",
            IsActive = false
        });

        await ctx.SaveChangesAsync();

        // Act
        var manufacturies = ctx.Manufacturers.ToList();

        // Assert
        manufacturies.Should().NotBeEmpty().And.HaveCount(1);
    }

    [Fact]
    public async Task TestIsActiveNullableFilter()
    {
        // Arrange
        var contextFactory = _serviceProvider.GetRequiredService<IDbContextFactory<TestDbContext>>();
        var ctx = await contextFactory.CreateDbContextAsync();

        var root = new TreeItem
        {
            Name = "Root",
            IsActive = null,
            Children = new List<TreeItem>(),
            MaterializedPath = "1"
        };

        root.Children.Add(new TreeItem
        {
            Name = "Child_1",
            IsActive = false,
            Children = new List<TreeItem>(),
            MaterializedPath = "1.1"
        });

        root.Children.Add(new TreeItem
        {
            Name = "Child_2",
            IsActive = true,
            Children = new List<TreeItem>(),
            MaterializedPath = "1.2"
        });

        ctx.TreeItems.Add(root);

        await ctx.SaveChangesAsync();

        // Act
        var items = ctx.TreeItems.ToList();

        // Assert
        items.Should().NotBeEmpty().And.HaveCount(2);
    }
}