﻿using FluentAssertions;
using FluentValidation;
using Xunit;

namespace Tests.Validation;

public class ValidatorTests
{
    [Fact(DisplayName = "Проверка валидации массива")]
    public void TestFluentArrayValidation()
    {
        // Arrange
        var garden = new GardenModel(new[] { 1, 2, 0 });

        // Act
        var validator = new GardenModelValidator();
        var result = validator.Validate(garden);

        // Assert
        result.Should().NotBeNull();
    }
}

/// <summary>
/// Test garden model for validation
/// </summary>
internal class GardenModel
{
    /// <summary>
    /// Heights of trees in garden
    /// </summary>
    public int[] TreeHeights { get; }

    public GardenModel(int[] treeHeights)
    {
        TreeHeights = treeHeights;
    }
}

internal class GardenModelValidator : AbstractValidator<GardenModel>
{
    public GardenModelValidator()
    {
        RuleFor(r => r.TreeHeights).NotEmpty();
        RuleForEach(r => r.TreeHeights).GreaterThan(0);
    }
}