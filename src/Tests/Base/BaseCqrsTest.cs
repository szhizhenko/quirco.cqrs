﻿using AutoMapper;
using Quirco.Cqrs.AutoMapper;
using Quirco.Cqrs.Tests;
using Tests.Data;
using Tests.Domain.Profiles;
using IMapper = Quirco.Cqrs.Core.Mapping.IMapper;

namespace Tests.Base;

public class BaseCqrsTest : BaseIntegrationTest<CqrsTestStartup, TestDbContext>
{
    protected override void InitializeDbContext(TestDbContext context)
    {
        context.SaveChanges();
    }

    protected static IMapper Mapper => new AutoMapperAdapter(new Mapper(new MapperConfiguration(cfg => cfg.AddProfile<DomainProfile>())));
}