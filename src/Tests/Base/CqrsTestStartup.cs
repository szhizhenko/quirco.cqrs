﻿using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Quirco.Cqrs.AutoMapper;
using Quirco.Cqrs.Domain;
using Quirco.Cqrs.Domain.Behaviors;
using Quirco.Cqrs.Tests.Startups;
using Tests.Data;

namespace Tests.Base;

public class CqrsTestStartup : StartupBase
{
    public override void Configure(IApplicationBuilder app)
    {
    }

    public override void ConfigureServices(IServiceCollection services)
    {
        services.AddMediatR(cfg => cfg.RegisterServicesFromAssembly(typeof(CqrsTestStartup).Assembly));
        services.AddAutoMapperAdapter(typeof(CqrsTestStartup).Assembly);
        services.AddScoped(typeof(IPipelineBehavior<,>), typeof(FluentValidationPipelineBehavior<,>));
        services.AddQueryableModifiers(typeof(CqrsTestStartup).Assembly);
        services.AddValidatorsFromAssemblies(new[] { typeof(CqrsTestStartup).Assembly }, ServiceLifetime.Transient);
        services.AddTestServices<TestDbContext>();
    }
}