using AntDesign;
using AntDesign.Tests;
using Bunit;
using Microsoft.Extensions.DependencyInjection;
using Quirco.Cqrs.Blazor.MediatR;

namespace Tests.Base;

public class BaseBlazorTest : BaseCqrsTest
{
    protected override void ConvifureServices(IServiceCollection services)
    {
        services.AddSingleton<TestContext>(container =>
        {
            var context = new AntDesignTestBase(true);

            context.JSInterop.Mode = JSRuntimeMode.Loose;
            context.Services.AddFallbackServiceProvider(container);
            context.JSInterop.Setup<AntDesign.JsInterop.DomRect>(JSInteropConstants.GetBoundingClientRect, _ => true)
                .SetResult(new AntDesign.JsInterop.DomRect());

            return context;
        }).AddScopedMediator();

        base.ConvifureServices(services);
    }
}