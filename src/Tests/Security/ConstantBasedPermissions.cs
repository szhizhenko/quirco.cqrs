﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Quirco.Security;
using Quirco.Security.Attributes;

namespace Tests.Security;

[PermissionsData("TestModule")]
public static class ConstantBasedPermissions
{
    [DisplayName("Person")]
    [RequireRoles(BuiltInRoles.Employee)]
    public static class Person
    {
        [RequireRoles(BuiltInRoles.Admin)]
        [Display(Name = "Добавление")]
        public const string Add = "Security.Person.Add";

        public const string View = "Security.Person.View";

        public const string Edit = "Security.Person.Edit";

        public const string Delete = "Security.Person.Delete";
    }
}