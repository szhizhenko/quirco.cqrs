﻿using FluentAssertions;
using Microsoft.Extensions.DependencyInjection;
using Quirco.Security;
using Quirco.Security.Extensions;
using Quirco.Security.Permission;
using Xunit;

namespace Tests.Security;

public class SecurityTests
{
    [Fact]
    public void TestListing()
    {
        // Arrange
        var provider = new AssemblyScanPermissionsProvider(GetType().Assembly);

        // Act
        var permissions = provider.GetAllPermissions().ToList();

        // Assert
        permissions.Should().HaveCount(2);
        permissions[0].ModuleName.Should().Be("TestModule");
        permissions[0].EntityName.Should().Be("Person");
        permissions[0].Permissions.Should().HaveCount(4);
        permissions[0].Permissions.Should().BeEquivalentTo("Security.Person.Add", "Security.Person.View", "Security.Person.Edit", "Security.Person.Delete");
        permissions[0].ReadablePermissions.Should().HaveCount(4);
        permissions[0].ReadablePermissions.Should().BeEquivalentTo("TestModule.Person.Добавление", "TestModule.Person.View", "TestModule.Person.Edit", "TestModule.Person.Delete");

        permissions[1].ModuleName.Should().Be("TestModule");
        permissions[1].EntityName.Should().Be("Person");
        permissions[1].Permissions.Should().HaveCount(4);
        permissions[1].Permissions.Should().BeEquivalentTo("PropertyBasedPermissions.Person.Add", "PropertyBasedPermissions.Person.View", "PropertyBasedPermissions.Person.Edit", "PropertyBasedPermissions.Person.Delete");
        permissions[1].ReadablePermissions.Should().HaveCount(4);
        permissions[1].ReadablePermissions.Should().BeEquivalentTo("TestModule.Person.Добавление", "TestModule.Person.View", "TestModule.Person.Edit", "TestModule.Person.Delete");
    }

    [Fact]
    public void PropertyBasedPermissions_Should_Be_SetToProperValues()
    {
        // Arrange
        new ServiceCollection()
            .AddPermissions(opts =>
                opts
                    .AddAssemblies(GetType().Assembly)
                    .SetImplicitRoles(true))
            .BuildServiceProvider();

        // Act
        var assemblyScannedPermissions = new AssemblyScanPermissionsProvider(GetType().Assembly).GetAllPermissions();

        // Assert
        PropertyBasedPermissions.Person.Add.Should().Be("PropertyBasedPermissions.Person.Add");
        PropertyBasedPermissions.Person.Delete.Should().Be("PropertyBasedPermissions.Person.Delete");
        PropertyBasedPermissions.Person.Edit.Should().Be("PropertyBasedPermissions.Person.Edit");
        PropertyBasedPermissions.Person.View.Should().Be("PropertyBasedPermissions.Person.View");

        assemblyScannedPermissions.Should().BeEquivalentTo(new[]
        {
            new Permission(
                "TestModule",
                "Person",
                new[] { "TestModule.Person.Добавление", "TestModule.Person.View", "TestModule.Person.Edit", "TestModule.Person.Delete" },
                new[] { "Security.Person.Add", "Security.Person.View", "Security.Person.Edit", "Security.Person.Delete" },
                new[] { "Employee" }),
            new Permission(
                "TestModule",
                "Person",
                new[] { "TestModule.Person.Добавление", "TestModule.Person.View", "TestModule.Person.Edit", "TestModule.Person.Delete" },
                new[]
                {
                    "PropertyBasedPermissions.Person.Add", "PropertyBasedPermissions.Person.View", "PropertyBasedPermissions.Person.Edit",
                    "PropertyBasedPermissions.Person.Delete"
                },
                new[] { "Employee" }),
        });
    }

    [Fact]
    public void PermissionsRolePoliciesTest()
    {
        // Act
        var rolePolicies = new[] { GetType().Assembly }.GetRequiredRolePolicies();

        // Assert
        rolePolicies["Security.Person.Add"].Should().BeEquivalentTo(BuiltInRoles.Admin, BuiltInRoles.Employee);
        rolePolicies["Security.Person.View"].Should().BeEquivalentTo(BuiltInRoles.Employee);

        rolePolicies["PropertyBasedPermissions.Person.Add"].Should().BeEquivalentTo(BuiltInRoles.Admin, BuiltInRoles.Employee);
        rolePolicies["PropertyBasedPermissions.Person.Edit"].Should().BeEquivalentTo(BuiltInRoles.Employee);
    }
}