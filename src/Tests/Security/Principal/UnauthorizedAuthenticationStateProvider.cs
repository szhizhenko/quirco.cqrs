using System.Security.Claims;
using Microsoft.AspNetCore.Components.Authorization;
using Quirco.Security.Principal;

namespace Tests.Security.Principal;

public class UnauthorizedAuthenticationStateProvider : AuthenticationStateProvider
{
    public override Task<AuthenticationState> GetAuthenticationStateAsync()
    {
        return Task.FromResult(new AuthenticationState((ClaimsPrincipal)IPrincipalAccessor.PrincipalUnauthorized));
    }
}