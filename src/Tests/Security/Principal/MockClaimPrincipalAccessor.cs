﻿using Quirco.Security;
using Quirco.Security.Principal;
using System.Security.Claims;
using System.Security.Principal;

namespace Tests.Security.Principal;

/// <summary>
/// By default imitates access providing to principal, that has user's ID in its claims
/// </summary>
public class MockClaimPrincipalAccessor : IPrincipalAccessor
{
    private IPrincipal _principal;

    public Task<IPrincipal> GetPrincipal()
    {
        if (_principal == null)
            SetPrincipal(new ClaimsPrincipal());

        return Task.FromResult(_principal);
    }

    public void SetPrincipal(IPrincipal principal)
    {
        _principal = principal
                     ?? throw new ArgumentNullException(nameof(principal));

        if (_principal is ClaimsPrincipal claimsPrincipal)
        {
            var identityClaim = new List<Claim>
            {
                new(Constants.ClaimTypes.UserId, "1")
            };

            claimsPrincipal.AddIdentity(new ClaimsIdentity(identityClaim));
        }
    }
}