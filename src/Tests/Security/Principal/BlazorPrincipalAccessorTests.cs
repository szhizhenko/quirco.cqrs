using FluentAssertions;
using Microsoft.AspNetCore.Http;
using Quirco.Security.Principal;
using Xunit;

namespace Tests.Security.Principal;

public class BlazorPrincipalAccessorTests
{
    [Fact]
    public async Task GetPrincipal_Should_Unauthorized_When_Not_Authorized()
    {
        // Arrange
        var storage = new ScopePrincipalStorage();
        var principalAccessor = new BlazorPrincipalAccessor(new HttpContextAccessor(), new UnauthorizedAuthenticationStateProvider(), storage);

        // Act
        var principal = await principalAccessor.GetPrincipal();

        // Assert
        principal.Should().Be(IPrincipalAccessor.PrincipalUnauthorized);
        storage.GetPrincipal().Should().Be(IPrincipalAccessor.PrincipalUnauthorized);
    }
}
