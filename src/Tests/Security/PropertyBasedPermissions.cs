﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Quirco.Security;
using Quirco.Security.Attributes;

namespace Tests.Security;

[PermissionsData("TestModule")]
public static class PropertyBasedPermissions
{
    [DisplayName("Person")]
    [RequireRoles(BuiltInRoles.Employee)]
    public static class Person
    {
        [RequireRoles(BuiltInRoles.Admin)]
        [Display(Name = "Добавление")]
        public static string Add { get; private set; }

        public static string View { get; private set; }

        public static string Edit { get; private set; }

        public static string Delete { get; private set; }
    }
}