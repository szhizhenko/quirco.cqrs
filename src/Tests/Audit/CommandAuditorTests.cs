﻿using System.Security.Principal;
using FluentAssertions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using NSubstitute;
using Quirco.Cqrs.Auditor;
using Quirco.Cqrs.Auditor.Commands;
using Quirco.Cqrs.Auditor.Core.Interfaces;
using Quirco.Cqrs.Auditor.DataAccess;
using Quirco.Cqrs.Auditor.Interfaces;
using Quirco.Cqrs.Infrastructure.DateTime;
using Quirco.Cqrs.Tests;
using Quirco.Cqrs.Tests.Principal;
using Quirco.Security.Principal;
using Xunit;

namespace Tests.Audit;

public class CommandAuditorTests
{
    [Fact]
    public async Task CommandAuditorTest()
    {
        // Arrange
        var serviceProvider = new ServiceCollection()
            .AddScoped<ICommandProcessingAuditor<SomeCommand>, SomeCommandAuditor>()
            .AddAuditor(c => c.UseInMemoryDatabase(Guid.NewGuid().ToString()))
            .AddMediatR(c => c.RegisterServicesFromAssemblyContaining<CommandAuditorTests>())
            .AddHttpContextAccessor()
            .AddSingleton(Substitute.For<IPrincipal>())
            .AddTransient<ICurrentDateTimeProvider, CurrentMockUtcDateTimeProvider>()
            .AddTransient<IPrincipalAccessor, TestPrincipalAccessor>()
            .BuildServiceProvider();

        // Act
        var mediator = serviceProvider.GetRequiredService<IMediator>();
        await mediator.Send(new SomeCommand { Name = "test command" });

        // Assert
        var auditCtx = serviceProvider.GetRequiredService<AuditDataContext>();
        auditCtx.AuditEvents.Count().Should().Be(1);
    }

    public class SomeCommand : IRequest, IAuditableCommand
    {
        public string Name { get; set; }
    }

    public class SomeCommandHander : IRequestHandler<SomeCommand>
    {
        public Task Handle(SomeCommand request, CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }
    }

    public class SomeCommandAuditor : CommandProcessingAuditor<SomeCommand>
    {
        public override ValueTask PostProcess(
            AuditScope auditScope,
            SomeCommand command,
            CancellationToken cancellationToken)
        {
            auditScope.AddEvent("test-event", e => e.Message("Some message"));

            return base.PostProcess(auditScope,
                                    command,
                                    cancellationToken);
        }
    }
}