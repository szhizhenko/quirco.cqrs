﻿using System.Security.Claims;
using FluentAssertions;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Quirco.Cqrs.Auditor;
using Quirco.Cqrs.Auditor.Builders;
using Quirco.Cqrs.Auditor.DataAccess;
using Quirco.Cqrs.Auditor.DataAccess.Models;
using Quirco.Cqrs.Auditor.Interfaces;
using Quirco.Cqrs.Infrastructure.DateTime;
using Quirco.Cqrs.Tests;
using Quirco.Cqrs.Tests.Principal;
using Quirco.Security.Principal;
using Tests.Data;
using Xunit;

namespace Tests.Audit;

public class AuditTests : IHttpContextAccessor
{
    private readonly ServiceProvider _serviceProvider;

    public AuditTests()
    {
        _serviceProvider = new ServiceCollection()
            .AddAuditor(c => c.UseInMemoryDatabase(Guid.NewGuid().ToString()))
            .AddSingleton<IHttpContextAccessor>(this)
            .AddSingleton<IPrincipalAccessor>(new TestPrincipalAccessor(new ClaimsPrincipal()))
            .AddSingleton<ICurrentDateTimeProvider, CurrentMockUtcDateTimeProvider>()
            .BuildServiceProvider();

        _httpContext.Request.Headers["x-correlation-id"] = "123";
    }

    [Fact]
    public async Task TestSimpleEvent()
    {
        // Arrange
        var scopeFactory = _serviceProvider.GetRequiredService<IAuditScopeFactory>();
        var context = _serviceProvider.GetRequiredService<AuditDataContext>();

        // Act
        await using (var scope = await scopeFactory.Create(null, CancellationToken.None))
        {
            scope.AddEvent("Message", e => e.Message("Test message"))
                .AddEvent("Some other event");
        }

        var events = context.AuditEvents.ToList();

        // Assert
        events.Should().HaveCount(2);

        events.First()
            .Should()
            .BeEquivalentTo(new AuditEvent
                            {
                                EventType = "Message",
                                Message = "Test message",
                                CorrelationId = "123"
                            },
                            c => c.Excluding(e => e.DateTime).Excluding(e => e.Id));
    }

    [Fact]
    public async Task TestEntityEvent()
    {
        // Arrange
        var scopeFactory = _serviceProvider.GetRequiredService<IAuditScopeFactory>();
        var context = _serviceProvider.GetRequiredService<AuditDataContext>();

        // Act
        await using (var scope = await scopeFactory.Create(null, CancellationToken.None))
        {
            scope.AddEntityEvent<Person>("Created", c => c.Message("New person was created"));
        }

        var events = context.AuditEvents.ToList();

        // Assert
        events.Should().HaveCount(1);

        events.First()
            .Should()
            .BeEquivalentTo(new AuditEvent
                            {
                                EntityType = typeof(Person).Name,
                                EventType = "Created",
                                Message = "New person was created",
                                CorrelationId = "123"
                            },
                            c => c.Excluding(e => e.DateTime).Excluding(e => e.Id));
    }

    [Fact]
    public async Task TestPropertyChangedEvent()
    {
        // Arrange
        var scopeFactory = _serviceProvider.GetRequiredService<IAuditScopeFactory>();
        var context = _serviceProvider.GetRequiredService<AuditDataContext>();

        // Act
        await using (var scope = await scopeFactory.Create(null, CancellationToken.None))
        {
            scope.AddEntityEvent<Person, int>(
                1,
                e =>
                    e.Message("Some properties has changed")
                        .PropertyChanged(nameof(Person.Age), 18, 18)
                        .PropertyChanged(nameof(Person.Name), "Joe", "Bill"));
        }

        var events = context.AuditEvents.ToList();

        // Assert
        events.Should().HaveCount(1);

        events.First()
            .Should()
            .BeEquivalentTo(
                new AuditEvent
                {
                    EventType = AuditEventBuilder.PropertyChangedEventType,
                    EntityType = typeof(Person).Name,
                    EntityId = "1",
                    CorrelationId = "123",
                    Parameter = nameof(Person.Name),
                    OldValue = "Joe",
                    OldValueReadable = "Joe",
                    NewValue = "Bill",
                    NewValueReadable = "Bill",
                    Message = "Some properties has changed"
                },
                c => c.Excluding(e => e.DateTime).Excluding(e => e.Id));
    }

    private HttpContext _httpContext = new DefaultHttpContext();

    public HttpContext? HttpContext
    {
        get => _httpContext;
        set => _httpContext = value;
    }
}