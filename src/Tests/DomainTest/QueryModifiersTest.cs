﻿using FluentAssertions;
using Microsoft.EntityFrameworkCore;
using Quirco.Cqrs.Domain.Exceptions;
using Tests.Base;
using Tests.Data;
using Tests.Domain.Queries;
using Tests.Domain.Queries.Modifiers;
using Xunit;

namespace Tests.DomainTest;

public class QueryModifiersTest : BaseCqrsTest
{
    protected override void InitializeDbContext(TestDbContext context)
    {
        context.Persons.AddRange(new List<Person>
        {
            new() { Age = 16, Name = "Alexey" },
            new() { Age = 39, Name = "Sergey" },
            new() { Age = 18, Name = "Oksana" },
            new() { Age = 20, Name = "Anna" }
        });
        base.InitializeDbContext(context);
    }

    [Fact]
    public async Task ListQueryModifiersTest()
    {
        // Arrange
        var modifiers = new[] { new AdultFilterModifier() };
        var query = new SearchPersonsQuery();
        var handler = new SearchPersonsQueryHandler(Mapper, ContextFactory, modifiers);

        // Act
        var pagedResponse = await handler.Handle(query, CancellationToken.None);
        var result = pagedResponse.Results;

        // Assert
        result.Should().HaveCount(3);
        result.Should().NotContain(
            x => x.Name == "Alexey",
            "Person with age < 18 was removed from queryable by modifier.");
    }

    [Fact]
    public async Task GetByIdQueryModifiersTest()
    {
        // Arrange
        var notAdult = await Context.Persons.FirstOrDefaultAsync(x => x.Age < 18);
        var adult = await Context.Persons.FirstOrDefaultAsync(x => x.Age >= 18);
        var query1 = new GetPersonByIdQuery(notAdult.Id);
        var query2 = new GetPersonByIdQuery(adult.Id);

        var modifiers = new[] { new AdultFilterModifier() };
        var handler = new GetPersonByIdQueryHandler(Mapper, Context, modifiers);

        // Act, Assert
        await handler.Awaiting(h => h.Handle(query1, CancellationToken.None)).Should()
            .ThrowAsync<ObjectNotFoundException<int>>("Person with age < 18 was removed from queryable by modifier.");
        (await handler.Handle(query2, CancellationToken.None)).Name.Should().Be(adult.Name);
    }
}