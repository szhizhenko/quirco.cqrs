﻿using FluentAssertions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using NSubstitute;
using Tests.Data;
using Xunit;

namespace Tests.DomainTest;

public class SoftDeletionFiltersTest
{
    protected readonly IServiceProvider _serviceProvider;

    public SoftDeletionFiltersTest()
    {
        _serviceProvider = new ServiceCollection()
            .AddTransient(_ => Substitute.For<IMediator>())
            .AddDbContext<TestDbContext>(c => c.UseInMemoryDatabase(Guid.NewGuid().ToString()))
            .BuildServiceProvider();
    }

    [Fact]
    public async Task TestUndeleted()
    {
        // Arrange
        var context = _serviceProvider.GetRequiredService<TestDbContext>();
        context.Manufacturers.AddRange(new Manufacturer { Name = "Manuf1", IsActive = true }, new Manufacturer { Name = "Manuf2", IsActive = true });
        await context.SaveChangesAsync();

        // Act
        var all = await context.Manufacturers.ToListAsync();

        // Assert
        all.Should().HaveCount(2);
    }

    [Fact]
    public async Task TestDeletedFiltered()
    {
        // Arrange
        var context = _serviceProvider.GetRequiredService<TestDbContext>();
        context.Manufacturers.AddRange(new Manufacturer { Name = "Manuf1", IsActive = true}, new Manufacturer { Name = "Manuf2", IsActive = true, DateDeleted = DateTime.Now });
        await context.SaveChangesAsync();

        // Act
        var all = await context.Manufacturers.ToListAsync();

        // Assert
        all.Should().HaveCount(1);
        all.First().Name.Should().Be("Manuf1");
    }

    [Fact]
    public async Task TestQueryFilterIgnorance()
    {
        // Arrange
        var context = _serviceProvider.GetRequiredService<TestDbContext>();
        context.Manufacturers.AddRange(new Manufacturer { Name = "Manuf1" }, new Manufacturer { Name = "Manuf2", DateDeleted = DateTime.Now });
        await context.SaveChangesAsync();

        // Act
        var all = await context.Manufacturers.IgnoreQueryFilters().ToListAsync();

        // Assert
        all.Should().HaveCount(2);
    }

    [Fact]
    public async Task TestNavigationDeleted()
    {
        // Arrange
        var context = _serviceProvider.GetRequiredService<TestDbContext>();
        context.Cars.AddRange(
            new Car
            {
                Id = 1,
                Manufacturer = new Manufacturer { Name = "Manuf1", IsActive = true }
            },
            new Car
            {
                Id = 2,
                Manufacturer = new Manufacturer { Name = "Manuf2", IsActive = true, DateDeleted = DateTime.Now }
            }
        );
        await context.SaveChangesAsync();

        // Act
        var all = await context.Cars.Include(c => c.Manufacturer).ToListAsync();

        // Assert
        all.Should().HaveCount(1);
    }

    [Fact]
    public async Task TestNavigationDeletedQueryFiltersDisabled()
    {
        // Arrange
        var context = _serviceProvider.GetRequiredService<TestDbContext>();
        context.Cars.AddRange(
            new Car
            {
                Id = 1,
                Manufacturer = new Manufacturer { Name = "Manuf1", IsActive = true }
            },
            new Car
            {
                Id = 2,
                Manufacturer = new Manufacturer { Name = "Manuf2", IsActive = true, DateDeleted = DateTime.Now }
            }
        );
        await context.SaveChangesAsync();

        // Act
        var all = await context.Cars.Include(c => c.Manufacturer).IgnoreQueryFilters().ToListAsync();

        // Assert
        all.Should().HaveCount(2);
    }
}