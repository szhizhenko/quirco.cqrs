﻿using FluentAssertions;
using Quirco.Cqrs.Domain.Queries;
using Tests.Base;
using Tests.Data;
using Tests.Domain.Queries;
using Xunit;

namespace Tests.DomainTest;

public class SearchTests : BaseCqrsTest
{
    [Fact(DisplayName = "Test for search of all records")]
    public async Task PagingTest1()
    {
        // Arrange
        await Context.Persons.AddRangeAsync(
            new Person
            {
                Name = "Sergey",
                Age = 39
            },
            new Person
            {
                Name = "Julia",
                Age = 37
            },
            new Person
            {
                Name = "Darya",
                Age = 9
            }
        );

        // Act
        var handler = new SearchPersonsQueryHandler(Mapper, ContextFactory, null);
        var result = await handler.Handle(new SearchPersonsQuery(), CancellationToken.None);

        // Assert
        result.Count.Should().Be(3);
        result.Results.Should().HaveCount(3);
    }

    [Fact(DisplayName = "Test for search paged result")]
    public async Task PagingTest2()
    {
        // Arrange
        await Context.Persons.AddRangeAsync(
            new Person
            {
                Name = "Sergey",
                Age = 39
            },
            new Person
            {
                Name = "Julia",
                Age = 37
            },
            new Person
            {
                Name = "Darya",
                Age = 9
            }
        );

        // Act
        var handler = new SearchPersonsQueryHandler(Mapper, ContextFactory, null);

        var result = await handler.Handle(
            new SearchPersonsQuery
            {
                Offset = 1,
                Limit = 1,
                Query = "Julia"
            },
            CancellationToken.None);

        // Assert
        result.Count.Should().Be(3);
        result.Results.Should().HaveCount(1);
        result.Results[0].Name.Should().Be("Julia");
    }

    [Fact(DisplayName = "Test for search paged result for late-mapped entities")]
    public async Task TestPagingInLateMapping()
    {
        // Arrange
        await Context.Persons.AddRangeAsync(
            new Person
            {
                Name = "Sergey",
                Age = 39
            },
            new Person
            {
                Name = "Julia",
                Age = 37
            },
            new Person
            {
                Name = "Darya",
                Age = 9
            }
        );

        // Act
        var handler = new SearchPersonsLateMapping.Handler(Mapper, ContextFactory, null);

        var query = new SearchPersonsLateMapping.Query
        {
            Offset = 1,
            Limit = 1
        };

        var result = await handler.Handle(query, CancellationToken.None);

        // Assert
        result.Count.Should().Be(3);
        result.Results.Should().HaveCount(1);
        result.Results[0].Name.Should().Be("Julia");
    }

    [Fact]
    public async Task Search_Should_Sort_Descending_By_DateCreated_When_Entity_Has_DateCreated()
    {
        // Arrange
        await Context.Persons.AddAsync(new Person
        {
            Name = "Sergey",
            Age = 39
        });

        await Context.SaveChangesAsync(default);
        await Task.Delay(1000);

        await Context.Persons.AddAsync(new Person
        {
            Name = "Julia",
            Age = 37
        });

        await Context.SaveChangesAsync(default);
        await Task.Delay(1000);

        await Context.Persons.AddAsync(new Person
        {
            Name = "Darya",
            Age = 9
        });

        await Context.SaveChangesAsync(default);

        // Act
        var handler = new SearchPersonsQueryHandler(Mapper, ContextFactory, null);
        var result = await handler.Handle(new SearchPersonsQuery(), default);

        // Assert
        result.Results
            .Should()
            .BeInDescendingOrder(x => x.DateCreated);
    }

    [Fact]
    public async Task Search_Should_Sort_By_Name_When_Search_Has_SortableFields()
    {
        // Arrange
        await Context.Persons.AddRangeAsync(
            new Person
            {
                Name = "Sergey",
                Age = 39
            },
            new Person
            {
                Name = "Julia",
                Age = 37
            },
            new Person
            {
                Name = "Darya",
                Age = 9
            }
        );

        // Act
        var handler = new SearchPersonsQueryHandler(Mapper, ContextFactory, null);
        var query = new SearchPersonsQuery();

        query.OrderBy(x => x.Age, false);

        var result = await handler.Handle(query, default);

        // Assert
        result.Results
            .Should()
            .BeInAscendingOrder(x => x.Name);

        // Arrange
        query.ClearSort().OrderByDescending(x => x.Name);

        // Act
        result = await handler.Handle(query, default);

        // Assert
        result.Results
            .Should()
            .BeInDescendingOrder(x => x.Name);

        // Arrange
        query.ClearSort().OrderBy(x => x.Name);
        query.Limit = 1;

        // Act
        result = await handler.Handle(query, default);

        // Assert
        result.Results.First()
            .Name
            .Should()
            .Be("Darya");
    }

    [Fact]
    public async Task Search_Should_Sort_With_Then_By()
    {
        // Arrange
        await Context.Persons.AddRangeAsync(
            new Person
            {
                Id = 1,
                Name = "Sergey",
                Age = 9
            },
            new Person
            {
                Id = 2,
                Name = "Julia",
                Age = 9
            },
            new Person
            {
                Id = 3,
                Name = "Darya",
                Age = 39
            }
        );

        // Act
        var handler = new SearchPersonsQueryHandler(Mapper, ContextFactory, null);
        var query = new SearchPersonsQuery();

        query.OrderByDescending(x => x.Age).OrderByDescending(x => x.Name);

        var result = await handler.Handle(query, default);

        // Assert
        result.Results
            .Should()
            .Equal(result.Results.First(x => x.Id == 3),
                   result.Results.First(x => x.Id == 1),
                   result.Results.First(x => x.Id == 2));
    }

    [Fact]
    public async Task SearchCar_Should_Sort_With_Nested_Property()
    {
        // Arrange
        await Context.Persons.AddRangeAsync(
            new Person
            {
                Id = 1,
                Name = "Sergey",
                Age = 9,
                Cars = new List<Car>
                {
                    new()
                    {
                        Manufacturer = new Manufacturer { Name = "1" }
                    }
                }
            },
            new Person
            {
                Id = 2,
                Name = "Julia",
                Age = 9,
                Cars = new List<Car>
                {
                    new()
                    {
                        Manufacturer = new Manufacturer { Name = "2" }
                    }
                }
            },
            new Person
            {
                Id = 3,
                Name = "Darya",
                Age = 39,
                Cars = new List<Car>
                {
                    new()
                    {
                        Manufacturer = new Manufacturer { Name = "3" }
                    }
                }
            }
        );

        // Act
        var handler = new SearchCarsQueryHandler(Mapper, ContextFactory, null);
        var query = new SearchCarsQuery();

        query.OrderByDescending(x => x.Owner.Age);

        var result = await handler.Handle(query, default);

        // Assert
        result.Results
            .Should()
            .BeInDescendingOrder(x => x.Owner.Age);
    }

    [Fact]
    public void Search_Should_Throw_ArgumentException_When_SortableProperty_Is_Not_Exists()
    {
        // Arrange
        var query = new SearchPersonsQuery();

        // Act
        var act = () => query.OrderByDescending("test");

        // Assert
        act.Should().Throw<ArgumentException>();
    }

    [Fact]
    public async Task Search_Should_Sort_When_PostMapping()
    {
        // Arrange  // Arrange
        await Context.Persons.AddRangeAsync(
            new Person
            {
                Id = 1,
                Name = "Sergey",
                Age = 1
            },
            new Person
            {
                Id = 3,
                Name = "Julia",
                Age = 7
            },
            new Person
            {
                Id = 2,
                Name = "Julia",
                Age = 4
            },
            new Person
            {
                Id = 4,
                Name = "Darya",
                Age = 14
            }
        );

        await Context.SaveChangesAsync();
        var query = new SearchPersonsLateMapping.Query();
        var handler = new SearchPersonsLateMapping.Handler(Mapper, ContextFactory, null);
        query.OrderBy("Age");

        // Act
        var page = await handler.Handle(query, default);

        // Assert
        page.Results.Should().BeInAscendingOrder(x => x.Age);
    }

    [Fact]
    public async Task SearchPersons_Should_Sort_With_SortByIds_And_Name()
    {
        // Arrange
        await Context.Persons.AddRangeAsync(
            new Person
            {
                Id = 1,
                Name = "Sergey",
                Age = 9
            },
            new Person
            {
                Id = 3,
                Name = "Julia",
                Age = 9
            },
            new Person
            {
                Id = 2,
                Name = "Julia",
                Age = 9
            },
            new Person
            {
                Id = 4,
                Name = "Darya",
                Age = 39
            }
        );

        await Context.SaveChangesAsync();

        var sortById = 2;

        // Act
        var handler = new SearchPersonsQueryHandler(Mapper, ContextFactory, null);
        var query = new SearchPersonsQuery();

        query.AddSortByIds(sortById);
        query.OrderBy(x => x.Name);

        var result = await handler.Handle(query, default);

        // Assert
        result.Results.First().Id.Should().Be(sortById);
        result.Results.Last().Name.Should().Be("Sergey");
    }
}