﻿using FluentAssertions;
using Quirco.Cqrs.DataAccess.Models;
using Quirco.Cqrs.Domain.Extensions;
using Xunit;

namespace Tests.DomainTest;

public class CollectionExtensionsTest
{
    private class SourceModel : IHasId
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    private class TargetModel : IHasId
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    private static SourceModel[] Source => new[]
    {
        new SourceModel { Id = 9, Name = "Петр" },
        new SourceModel { Id = 0, Name = "Оксана" },
        new SourceModel { Id = 0, Name = "Оксана" },
        new SourceModel { Id = 3, Name = "Анна" },
        new SourceModel { Id = 1, Name = "Игорь" },
    };

    private static List<TargetModel> Target => new()
    {
        new TargetModel { Id = 1, Name = "Алексей" },
        new TargetModel { Id = 2, Name = "Сергегй" },
        new TargetModel { Id = 3, Name = "Анна" },
    };

    [Fact]
    public void FastSyncTest()
    {
        var target = Target;
        Source.Sync(
            target,
            (s, t) =>
            {
                t.Id = s.Id;
                t.Name = s.Name;
                return t;
            }
        );

        target.Should().Contain(x => x.Name == "Игорь" && x.Id == 1);
        target.Should().Contain(x => x.Name == "Анна" && x.Id == 2);
        target.Should().Contain(x => x.Name == "Игорь" && x.Id == 1);
        target.Should().Contain(x => x.Name == "Петр" && x.Id == 3);
        target.Where(x => x.Name == "Оксана").Should().HaveCount(2);
    }

    [Fact]
    public void NullCollectionSyncTest()
    {
        var target = Target;
        var targetSize = target.Count;
        SourceModel[] source = null;
        source.Sync(
            target,
            (s, t) =>
            {
                t.Id = s.Id;
                t.Name = s.Name;
                return t;
            }
        );

        target.Should().HaveCount(targetSize, "Unchanged");
    }

    [Fact]
    public void SyncNoLookupTest()
    {
        var target = Target;
        Source.SyncByCompare(
            target,
            (s, t) =>
            {
                t.Id = s.Id;
                t.Name = s.Name;
                return t;
            }
        );

        target.Should().Contain(x => x.Name == "Игорь" && x.Id == 1);
        target.Should().Contain(x => x.Name == "Анна" && x.Id == 3);
        target.Should().Contain(x => x.Name == "Петр" && x.Id == 0);
        target.Should().Contain(x => x.Name == "Оксана" && x.Id == 0);
        target.Where(x => x.Name == "Оксана").Should().HaveCount(2);
    }

    [Fact]
    public void SyncLookupTest()
    {
        var target = Target;
        Source.SyncByCompare(
            target,
            (s, t) =>
            {
                t.Id = s.Id;
                t.Name = s.Name;
                return t;
            },
            (s, t) => s.Name == t.Name
        );

        target.Should().Contain(x => x.Name == "Игорь" && x.Id == 0);
        target.Should().Contain(x => x.Name == "Анна" && x.Id == 3);
        target.Should().Contain(x => x.Name == "Петр" && x.Id == 0);
        target.Should().Contain(x => x.Name == "Оксана" && x.Id == 0);
        target.Where(x => x.Name == "Оксана").Should().HaveCount(1);
    }
}