﻿using FluentAssertions;
using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Quirco.Cqrs.AutoMapper;
using Quirco.Cqrs.Domain.Commands;
using Quirco.Security.Principal;
using Tests.Data;
using Tests.Domain.Commands;
using Tests.Security.Principal;
using Xunit;

namespace Tests.DomainTest;

public class CreateUpdateTests
{
    private readonly IServiceProvider _serviceProvider;

    public CreateUpdateTests()
    {
        _serviceProvider = new ServiceCollection()
            .AddDbContext<TestDbContext>(c => c.UseInMemoryDatabase(Guid.NewGuid().ToString()))
            .AddMediatR(cfg => cfg.RegisterServicesFromAssemblies(typeof(DeleteCommand<>).Assembly,
                                                                  typeof(DeleteCarCommandHandler).Assembly))
            .AddAutoMapperAdapter(typeof(CreateUpdatePerson.Command).Assembly)
            .AddScoped<AuthenticationStateProvider, UnauthorizedAuthenticationStateProvider>()
            .AddScoped<IPrincipalAccessor, BlazorPrincipalAccessor>()
            .AddScoped<ScopePrincipalStorage>()
            .AddValidatorsFromAssemblyContaining(typeof(DeleteCommand<>))
            .BuildServiceProvider();
    }

    [Fact]
    public async Task TestCreate()
    {
        // Arrange
        var mediator = _serviceProvider.GetRequiredService<IMediator>();

        // Act
        var personId = await mediator.Send(
            new CreateUpdatePerson.Command(
                new CreateUpdatePerson.Model
                {
                    Name = "Sergio",
                    Age = 40
                }));

        var person = await _serviceProvider.GetRequiredService<TestDbContext>()
            .Persons.FirstOrDefaultAsync(p => p.Id == personId);

        // Assert
        person.Should().NotBeNull();
        person.Name.Should().Be("Sergio");
        person.Age.Should().Be(40);
    }

    [Fact]
    public async Task TestUpdate()
    {
        // Arrange
        var mediator = _serviceProvider.GetRequiredService<IMediator>();

        var personId = await mediator.Send(
            new CreateUpdatePerson.Command(
                new CreateUpdatePerson.Model
                {
                    Name = "Sergio",
                    Age = 40
                }));

        await mediator.Send(
            new CreateUpdatePerson.Command(
                personId,
                new CreateUpdatePerson.Model
                {
                    Name = "Sergio2",
                    Age = 41
                }));

        // Act
        var person = await _serviceProvider.GetRequiredService<TestDbContext>()
            .Persons.FirstOrDefaultAsync(p => p.Id == personId);

        // Assert
        person.Should().NotBeNull();
        person.Name.Should().Be("Sergio2");
        person.Age.Should().Be(41);
    }

    [Fact]
    public async Task TestUpdateByName()
    {
        // Arrange
        var mediator = _serviceProvider.GetRequiredService<IMediator>();

        var personId = await mediator.Send(
            new CreateUpdatePerson.Command(
                new CreateUpdatePerson.Model
                {
                    Name = "Sergio",
                    Age = 40
                }));

        await mediator.Send(
            new CreateUpdatePerson.Command(
                new CreateUpdatePerson.Model
                {
                    Name = "Sergio",
                    Age = 41
                }));

        // Act
        var person = await _serviceProvider.GetRequiredService<TestDbContext>()
            .Persons.FirstOrDefaultAsync(p => p.Id == personId);

        // Assert
        person.Should().NotBeNull();
        person.Name.Should().Be("Sergio");
        person.Age.Should().Be(41);
    }
}