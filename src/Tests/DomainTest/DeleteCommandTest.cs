﻿using FluentAssertions;
using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using NSubstitute;
using Quirco.Cqrs.Domain.Commands;
using Quirco.Cqrs.Infrastructure.Security;
using Quirco.Security.Principal;
using Tests.Data;
using Tests.Domain.Commands;
using Tests.Security.Principal;
using Xunit;

namespace Tests.DomainTest;

public class DeleteCommandTest
{
    private readonly IServiceProvider _serviceProvider;

    public DeleteCommandTest()
    {
        _serviceProvider = new ServiceCollection()
            .AddDbContext<TestDbContext>(c => c.UseInMemoryDatabase(Guid.NewGuid().ToString()))
            .AddMediatR(cfg => cfg.RegisterServicesFromAssemblies(typeof(DeleteCommand<>).Assembly,
                                                                  typeof(DeleteCarCommandHandler).Assembly))
            .AddScoped<AuthenticationStateProvider, UnauthorizedAuthenticationStateProvider>()
            .AddScoped<IPrincipalAccessor, BlazorPrincipalAccessor>()
            .AddScoped<ScopePrincipalStorage>()
            .AddScoped(_ => Substitute.For<IHttpContextAccessor>())
            .AddValidatorsFromAssemblyContaining(typeof(DeleteCommand<>))
            .BuildServiceProvider();
    }

    [Fact]
    public async Task DeleteTest()
    {
        // Arrange
        await using var context = _serviceProvider.GetRequiredService<TestDbContext>();
        var car = new Car { Manufacturer = new Manufacturer { Name = "Ford" } };

        context.Persons.Add(new Person
        {
            Name = "Test",
            Cars = new List<Car>
            {
                car
            }
        });

        await context.SaveChangesAsync();

        // Act
        var mediator = _serviceProvider.GetRequiredService<IMediator>();
        await mediator.Send(new DeleteCommand<Car, int>(car.Id));

        // Assert
        context.Cars.Count().Should().Be(0, "No cars in database after deletion");
    }

    [Fact]
    public async Task TestSoftDeletion()
    {
        // Arrange
        var principalAccessor = _serviceProvider.GetRequiredService<IPrincipalAccessor>();
        var userDeletedId = (await principalAccessor.GetPrincipal()).GetId();

        await using var context = _serviceProvider.GetRequiredService<TestDbContext>();

        var person = new Person
        {
            Name = "Test"
        };

        context.Persons.Add(person);
        await context.SaveChangesAsync();

        // Act
        var mediator = _serviceProvider.GetRequiredService<IMediator>();
        await mediator.Send(new DeleteCommand<Person, int>(person.Id));

        // Assert
        var dbPerson = await context.Persons.IgnoreQueryFilters().FirstOrDefaultAsync();
        dbPerson.Should().NotBeNull();
        dbPerson.DateDeleted.Should().NotBeNull();
        dbPerson.UserDeletedId.Should().Be(userDeletedId);
    }

    [Fact]
    public async Task TestInheritedSoftDeletion()
    {
        // Arrange
        await using var context = _serviceProvider.GetRequiredService<TestDbContext>();

        var manuf = new Manufacturer
        {
            Name = "Ford",
            Country = "USA"
        };

        context.Manufacturers.Add(manuf);
        await context.SaveChangesAsync();

        // Act
        context.Manufacturers.RemoveRange(manuf);
        context.SaveChanges();

        // Assert
        var r = await context.Manufacturers.ToListAsync();
        r.Should().HaveCount(0);
        r = await context.Manufacturers.IgnoreQueryFilters().ToListAsync();
        r.Should().HaveCount(1);
        r.First().DateDeleted.Should().NotBeNull();
    }

    [Fact]
    public async Task TestMultipleDelete()
    {
        // Arrange
        await using var context = _serviceProvider.GetRequiredService<TestDbContext>();

        var person = new Person
        {
            Name = "Test"
        };

        var person2 = new Person
        {
            Name = "Test2"
        };

        context.Persons.AddRange(person, person2);
        await context.SaveChangesAsync();

        // Act
        var mediator = _serviceProvider.GetRequiredService<IMediator>();
        await mediator.Send(new DeleteCommand<Person, int>(new[] { person.Id, person2.Id }));

        // Assert
        var dbPersons = await context.Persons.ToListAsync();
        dbPersons.All(a => a.DateDeleted.HasValue).Should().BeTrue();
    }
}