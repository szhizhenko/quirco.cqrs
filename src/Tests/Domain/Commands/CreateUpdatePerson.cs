﻿
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Quirco.Cqrs.Auditor.Core.Interfaces;
using Quirco.Cqrs.Domain.Commands;
using Quirco.Cqrs.Domain.Security;
using Tests.Data;
using IMapper = Quirco.Cqrs.Core.Mapping.IMapper;

namespace Tests.Domain.Commands;

public static class CreateUpdatePerson
{
    public class Model
    {
        public string Name { get; set; }
        public int Age { get; set; }
    }

    public class Command : CreateUpdateCommand<Model>, IAuditableCommand
    {
        public Command()
        {
        }

        public Command(Model model) : base(model)
        {
        }

        public Command(int? id, Model model) : base(id, model)
        {
        }
    }

    public class CommandHandler : CreateUpdateCommandHandler<Command, TestDbContext, Model, Person>
    {
        public CommandHandler(TestDbContext context, IMapper mapper, IEnumerable<IPermissionEvaluator<Person>> evaluators) : base(context, mapper, evaluators)
        {
        }

        protected override async Task<Person> TryGet(Command request, CancellationToken cancellationToken)
        {
            Person person = null;
            if (request.Id == null && !string.IsNullOrEmpty(request.Model.Name))
            {
                person = await Context.Persons.Where(p => p.Name == request.Model.Name).FirstOrDefaultAsync(cancellationToken);
            }

            return person ?? await base.TryGet(request, cancellationToken);
        }
    }

    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Model, Person>();
        }
    }
}