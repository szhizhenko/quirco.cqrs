﻿using Quirco.Cqrs.Domain.Commands;
using Quirco.Cqrs.Domain.Security;
using Quirco.Security.Principal;
using Tests.Data;

namespace Tests.Domain.Commands;

public class DeleteCarCommandHandler : DeleteCommandHandler<TestDbContext, Car>
{
    public DeleteCarCommandHandler(TestDbContext context, IEnumerable<IPermissionEvaluator<Car>>? evaluators, IPrincipalAccessor principal)
        : base(context, evaluators, principal)
    {
    }
}