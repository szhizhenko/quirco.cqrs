﻿using Quirco.Cqrs.Domain.Commands;
using Quirco.Cqrs.Domain.Security;
using Quirco.Security.Principal;
using Tests.Data;

namespace Tests.Domain.Commands;

public class DeletePersonCommandHandler : DeleteCommandHandler<TestDbContext, Person, int>
{
    public DeletePersonCommandHandler(TestDbContext context, IEnumerable<IPermissionEvaluator<Person>>? evaluators, IPrincipalAccessor principal)
        : base(context, evaluators, principal)
    {
    }
}