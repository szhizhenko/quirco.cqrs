﻿using Quirco.Cqrs.DataAccess.Models;

namespace Tests.Domain.Models;

public class PersonSlimModel : IHasId, IHasDateCreated
{
    public int Id { get; set; }
    public string Name { get; set; }
    public int Age { get; set; }
    public DateTime DateCreated { get; set; }
}