namespace Tests.Domain.Models;

public class CarModel
{
    public int Id { get; set; }

    public string Model { get; set; }

    public int OwnerId { get; set; }

    public string Name => Model;

    public PersonModel Owner { get; set; }
}