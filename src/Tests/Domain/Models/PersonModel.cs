﻿using Quirco.Cqrs.DataAccess.Models;

namespace Tests.Domain.Models;

public class PersonModel : IHasId
{
    public int Id { get; set; }
    public string Name { get; set; }
    public int Age { get; set; }
}