﻿using AutoMapper;
using Tests.Data;
using Tests.Domain.Models;

namespace Tests.Domain.Profiles;

public class DomainProfile : Profile
{
    public DomainProfile()
    {
        CreateMap<Person, PersonModel>();
        CreateMap<Person, PersonSlimModel>();
        CreateMap<Car, CarModel>();
    }
}