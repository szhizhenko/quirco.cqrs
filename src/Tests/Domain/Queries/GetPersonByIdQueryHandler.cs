﻿using Quirco.Cqrs.Core.Mapping;
using Quirco.Cqrs.Domain.Filters;
using Quirco.Cqrs.Domain.Queries;
using Tests.Data;
using Tests.Domain.Models;

namespace Tests.Domain.Queries;

public class GetPersonByIdQueryHandler : GetEntityByIdQueryHandler
<
    GetPersonByIdQuery,
    PersonModel,
    Person,
    TestDbContext
>
{
    public GetPersonByIdQueryHandler(IMapper mapper, TestDbContext context, IEnumerable<IQueryableModifier<Person>> modifiers)
        : base(mapper, context, modifiers)
    {
    }
}