﻿using Microsoft.EntityFrameworkCore;
using Quirco.Cqrs.Core.Mapping;
using Quirco.Cqrs.Domain.Filters;
using Quirco.Cqrs.Domain.Queries;
using Tests.Data;
using Tests.Domain.Models;

namespace Tests.Domain.Queries;

public static class SearchPersonsLateMapping
{
    public class Query : SearchQuery<PersonSlimModel>
    {
    }

    public class Handler : ListQueryPostMappedHandler<PersonSlimModel, Person, TestDbContext, Query>
    {
        public Handler(IMapper mapper, IDbContextFactory<TestDbContext> context, IEnumerable<IQueryableModifier<Person>>? modifiers)
            : base(mapper, context, modifiers)
        {
        }
    }
}