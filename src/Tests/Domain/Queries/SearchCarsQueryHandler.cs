using Microsoft.EntityFrameworkCore;
using Quirco.Cqrs.Core.Mapping;
using Quirco.Cqrs.Domain.Filters;
using Quirco.Cqrs.Domain.Queries;
using Tests.Data;
using Tests.Domain.Models;

namespace Tests.Domain.Queries;

public class SearchCarsQueryHandler : ListQueryHandler
<
    CarModel,
    Car,
    TestDbContext,
    SearchCarsQuery
>
{
    public SearchCarsQueryHandler(IMapper mapper, IDbContextFactory<TestDbContext> context, IEnumerable<IQueryableModifier<Car>> modifiers) : base(mapper, context, modifiers)
    {
    }
}