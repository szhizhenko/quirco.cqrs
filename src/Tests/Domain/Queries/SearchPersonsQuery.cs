﻿using Quirco.Cqrs.Domain.Queries;
using Tests.Domain.Models;

namespace Tests.Domain.Queries;

public class SearchPersonsQuery : SearchQuery<PersonSlimModel>
{
}