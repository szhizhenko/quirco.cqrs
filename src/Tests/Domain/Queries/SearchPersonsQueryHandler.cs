﻿using Microsoft.EntityFrameworkCore;
using Quirco.Cqrs.Core.Mapping;
using Quirco.Cqrs.Domain.Filters;
using Quirco.Cqrs.Domain.Queries;
using Tests.Data;
using Tests.Domain.Models;

namespace Tests.Domain.Queries;

public class SearchPersonsQueryHandler : ListQueryHandler
<
    PersonSlimModel,
    Person,
    TestDbContext,
    SearchPersonsQuery
>
{
    public SearchPersonsQueryHandler(IMapper mapper, IDbContextFactory<TestDbContext> context, IEnumerable<IQueryableModifier<Person>>? modifiers) : base(mapper, context, modifiers)
    {
    }
}