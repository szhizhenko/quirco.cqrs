
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Quirco.Cqrs.DataAccess.Models;
using Quirco.Cqrs.Domain.Filters;
using Quirco.Cqrs.Domain.Queries;
using Tests.Data;
using IMapper = Quirco.Cqrs.Core.Mapping.IMapper;

namespace Tests.Domain.Queries;

public static class SearchUsersGuid
{
    public class Query : SearchQuery<Model, Guid>
    {
    }

    public class Model : IHasId<Guid>
    {
        public Guid Id { get; set; }

        public string Name { get; set; }
    }

    internal class Handler : ListQueryHandler
    <
        Model,
        UserGuid,
        TestDbContext,
        Query,
        Guid
    >
    {
        public Handler(IMapper mapper, IDbContextFactory<TestDbContext> context, IEnumerable<IQueryableModifier<UserGuid>>? modifiers)
            : base(mapper, context, modifiers)
        {
        }
    }

    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<UserGuid, Model>();
        }
    }
}