using Quirco.Cqrs.DataAccess.Models;

namespace Tests.Data;

public class UserGuid : IHasId<Guid>
{
    public Guid Id { get; set; }

    public string Name { get; set; }
}