﻿using Quirco.Cqrs.DataAccess.Models.Dictionaries;

namespace Tests.Data;

public class Manufacturer : DictionaryEntity
{
    public string? Country { get; set; }
}