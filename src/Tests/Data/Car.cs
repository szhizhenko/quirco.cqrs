﻿using Quirco.Cqrs.DataAccess.Models;

namespace Tests.Data;

public class Car : IHasId
{
    public int Id { get; set; }

    public int ManufacturerId { get; set; }
    public Manufacturer Manufacturer { get; set; }

    public int OwnerId { get; set; }

    public Person Owner { get; set; }
}