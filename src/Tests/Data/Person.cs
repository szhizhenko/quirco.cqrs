﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Quirco.Cqrs.DataAccess.Models;
using Quirco.Cqrs.DataAccess.Npgsql.DataContext;

namespace Tests.Data;

public class PersonConfiguration : IEntityTypeConfiguration<Person>
{
    public void Configure(EntityTypeBuilder<Person> builder)
    {
        builder.ToTableInSnakeCase(nameof(Person), "dbo");
        builder.HasMany(p => p.Cars).WithOne(c => c.Owner);
    }
}

public class Person : Entity, IHasDateDeleted, IHasUserCreated, IHasUserDeleted, IHasDateCreated
{
    public string Name { get; set; }

    public int Age { get; set; }

    public DateTime? DateDeleted { get; set; }

    public int UserCreatedId { get; set; }

    public int? UserDeletedId { get; set; }

    public ICollection<Car> Cars { get; set; }
    public DateTime DateCreated { get; set; }
}