﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Quirco.Cqrs.Auditor.DataAccess.DbContextAuditor;
using Quirco.Cqrs.DataAccess.DataContext;

namespace Tests.Data;

public class TestDbContext : DbContext
{
    private readonly IMediator _mediator;
    public DbSet<Person> Persons { get; set; }
    public DbSet<Employee> Employees { get; set; }
    public DbSet<Car> Cars { get; set; }
    public DbSet<TreeItem> TreeItems { get; set; }

    public DbSet<Manufacturer> Manufacturers { get; set; }

    public DbSet<UserGuid> Users { get; set; }

    private static readonly HashSet<Type> AuditableTypes = new()
    {
        typeof(Person)
    };

    public TestDbContext(IMediator mediator, DbContextOptions<TestDbContext> options) : base(options)
    {
        _mediator = mediator;
    }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.AddInterceptors(new DbContextAuditorInterceptor<int>(_mediator, AuditableTypes, TryGetUserId));
        base.OnConfiguring(optionsBuilder);
    }

    public static bool TryGetUserId(out int userId)
    {
        userId = 1;
        return true;
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.EnableSoftDeletionQueryFilters();
        modelBuilder.EnableIsActiveQueryFilters();
        base.OnModelCreating(modelBuilder);
    }
}