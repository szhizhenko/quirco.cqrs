﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Quirco.Cqrs.DataAccess.Npgsql.DataContext;

namespace Tests.Data;

public class EmployeeConfiguration : IEntityTypeConfiguration<Employee>
{
    public void Configure(EntityTypeBuilder<Employee> builder)
    {
        builder.ToTableInSnakeCase(nameof(Employee), "dbo");
    }
}

public class Employee : Person
{
    public string WorkPlace { get; set; }
}