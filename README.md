# Quirco.Cqrs

CQRS patterns and reusable parts of code to build scalable applications.

These libraries covers different layers and parts of application and described below.
Most of features are based on following 3rd party packages:

- AutoMapper
- FluentValidation
- MediatR
- EF Core
- Hangfire
- xUnit
- NEST

## Domain

Commands, queries, some base models for CQRS-based business domain layer. Includes base abstract commands:
- `CreateUpdate` to create or update (if ID specified) of entities
- `Delete` to delete entity or multiple entities (see constructors overload), also supports soft delete in combination with `DbContextAuditorInterceptor` (entity should implement `IHasDateDeleted` interface)

Queries are:
- `GetEntityById`/`GetEntityByIdHandler` name says by itself
- `GetEntityByIdPostMappedHandler` is alternative to `GetEntityByIdHandler` implementation, that maps entity to model at application level (after entity was materialized)
- `ListQuery`/`ListQueryHandler` are base abstract queries with paging support. Handler maps an entity to the model at database level. Mapping occurs with the help of `AutoMapper`'s projections (`'.ProjectTo<TModel>()`)
- `ListQueryPostMappedHandler` is alternative to `ListQueryHandler` implementation, that maps entity to model at application level (queries are materialized to entities and then mapping occurs)
- `SearchQuery` is extension to the `ListQuery` that supports `Query` string parameter for search lookups (not supported by base handlers).
All of these base implementations (handlers) are abstract so you'll need to declare implicit handlers for your
entities/models.

Includes `IAffectEntityCommand` interface so that some pipeline logging logic can enrich log records with entity type and id.

In order domain layer to work you must choose and register mapper implementation. There are two mappers available: `AutoMapper` and `Mapster`:
```csharp
// For AutoMapper use:
services.AddAutoMapperAdapter(typeof(SomeCommand).Assembly);
// For Mapster use:
services.AddMapsterAdapter(typeof(SomeCommand).Assembly);
```
Adapter are located in `Quirco.Cqrs.AutoMapper` and `Quirco.Cqrs.Mapster` assemblies.

### Exceptions

This package contains some common useful base exceptions:

- `AccessDeniedException` - thrown on authorization failures
- `BusinessRuleException` - any custom business rule violation
- `ConcurrencyException` - thrown on concurrent update of EF entity
- `ObjectNotFoundException` - thrown when entity is not found in data context

## DataAccess

Package contains reusable parts for data access layer. Main features are described below.

### Base interfaces for entities

Several common interfaces were introduced to unify processing of entites. These are:

- `Entity<T>` - base entity class with `Id` of type `T`
- `IHasCode` - entity that has string `Code` field
- `IHasDateCreated` - entity with `DateCreated` field that contains creation date
- `IHasDateDeleted` - entity with `DateDeleted` field that contains deletion date and supports soft deletion mechanism
- `IHasDateModified` - entity with `DateModified` field that contains last modificatio date (when any of the field was changed)
- `IHasDraft` - entity with `IsDraft` flag indicating that entity is not ready to be presented to end users and is somewhere on middle of process
- `IHasId<T>` - interface for classes that has `Id` field of type `T`. Can be used both for entities and models

### Auditable DbContext

`DbContextAuditorInterceptor` solves two tasks.
First of all it updates audit fields, such as `DateCreated`, `DateModified`, `DateDeleted` (and same with `User-` suffix) based on change tracker status.

Second thing is to easily monitor changes in context via set of MediatR events.
When entity of specified type (passed as HashSet of entity types to monitor) will be updated or deleted, corresponding
events will be sent to `MediatR` pipeline (as notifications).

Events are:
`EntityUpdatingNotification` - fires before saving changes to database.
`EntityUpdatedNotification` - fires right after save.

Events are fired only for really changed entries (auditor uses `ChangeTracker` for this). Each event has extra flag, indicating if entity was inserted, updated
or deleted.

Minimal configuration example:

```csharp
public class PaymentDbContext {
    protected static readonly HashSet<Type> AuditableEntityTypes = new() {
        typeof(Customer),
        typeof(Payment)
    };
    private IMediator _mediator;
     
    public PaymentDbContext(IMediator mediator) 
    {
        _mediator = mediator;                
    }
    
    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.AddInterceptors(new DbContextAuditorInterceptor<int>(_mediator, AuditableEntityTypes, () => 1));
        base.OnConfiguring(optionsBuilder);
    }
}
```

### Behaviors

`TransactionAttribute` - allows to mark command with `[Transaction]` attribute, to enable `TransactionPipelineBehavior` to automatically wrap execution of
command handler into single transaction.

```csharp
[Transaction(IsolationLevel=TransactionIsolationLevel.Serializable)]
public class ProcessPaymentCommand {
    public int PaymentId { get; }
    public ProcessPaymentCommand(int paymentId) {
        PaymentId = paymentId;
    }
}
```
So you shouldn't care about transactions in your handlers and keep code clearer.

### ModelBuilder extensions
You can use some extensions for `ModelBuilder` type.

- `builder.DisableCascadeDeleteGlobally()`
Disables cascade delete behaviour by default. All foreign keys will have `DeleteBehavior.Restrict` behaviour.

- `builder.EnumsAsString()`
Store enum types as strings in database. Does not affects enums with `Flags` attribute because they're not supported.

- `builder.EnableSoftDeletionQueryFilters()`
Automatically filters out all entities that implements IHasDateDeleted and has non-null value in DateDeleted column


### Data migration
Data migration pack consists of several interfaces and implementation, helping to build data migration process in separate steps.
Step is defined as `IDataMigrationStep<TId>` interface. All implementations could be registered by bulk as follows:
```csharp
services.AddDataMigrationSteps<int>(typeof(Extensions).Assembly)
```
All implementation from specified assembly will be scanned and registered as data migration steps. To control the order of migration steps execution, use
`DependsOn` attribute that can be declared on migration step class, for example:
```csharp
[DependsOn(typeof(MigrateFacilities), typeof(MigrateRoomTypes)]
public class MigrateRooms : IDataMigrationStep<int> {
}
```
This attribute will ensure that `MigrateFacilities` and `MigrateRoomTypes` will be executed prior to `MigrateRooms` step.
To have access to legacy mapped entities, migration step will have access to `DataMigrationContext` instance, that contains several mapping methods:
```csharp
public DataMigrationContext<TId> AddMap<TEntity>(TId legacyId, TEntity obj) where TEntity : notnull
```
Adds record to map from legacy ID to newly migrated entity.
And second one is:
```csharp
public TEntity? GetFromMap<TEntity>(TId legacyId)
```
Allows to retrieve newly migrated record by it's legacy ID.

## Infrastructure

Contains lot of things, better to look at sources. Most useful are: `IJobsService` - abstraction around any background job library. This package
contains [Hangfire](##Hangfire) implementation of this interface.

### Current date abstraction

`ICurrentDateProvider` is used to make abstraction from `DateTime.Now` for easier testing. Package contains two implementations:

- `CurrentSystemDateProvider` that returns `DateTime.Now`, and
- `CurrentSystemUtcDateProvider` returning `DateTime.UtcNow`

For testing purposes see [Tests](##Tests) section

## Hangfire

Contains `Hangfire` implementation for `IJobsService` abstraction. This interface allows you to schedule and enqueue background jobs.
You'll need to register service:

```csharp
services.AddScoped<IJobsService, HangfireJobsService>();
```

## Mvc

### Controllers

Includes base implementation for CRUD controller and code generator template, that can be copied to your project and will allow quickly generate CRUD
controllers for simple entities.

### Filters

Filters includes all common processing of exceptions. For example, they contains `AccessDeniedExceptionFilter` that will turn `AccessDeniedException` from
domain layer into 401 response. Filters are:

- `AccessDeniedExceptionFilter`
- `ConcurrencyExceptionFilter` - will convert concurrency errors into 409 response codes
- `ObjectNotFoundExceptionFilter` will return 404 when no entity found
- `OperationCancelledExceptionFilter` is used to mute operation cancelled errors from main application log, will return 400 code to client
- `ValidationExceptionFilter` handles `ValidationException` and turns it info detailed 400 response

## Elasticsearch

Useful commands and behaviors to wrap some queries in CQRS pattern and return result not from database, but from Elasticsearch index.
To intercept queries you should inherit and implement `SearchPipelineBehavior`.

## Security

`Quirco.Security` package is designed to provide general permission-based security system.
Package includes `IPrincipalAccessor` implementations to access `IPrincipal` in different scenarios, implementations included are:
- `HttpContextPrincipalAccessor` - accesses principal stored in current HttpContenxt
- `TestPrincipalAccessor` - allows easily substitute principal for testing purposes
- `BlazorPrincipalAccessor` - uses `AuthenticationStateProvider` as primary source of principal, falling back to `HttpContextAccessor` if no authenticated user
  found in blazor's provider.

Also this package contains convenience extension methods for `IPrincipal`, they are:
- `HasPermissions` - checks if `ClaimsPrincipal` has specified permissions in their claim (type of claim should be `Permission`)
- `IsInRoles` - checks if `ClaimsPrincipal` has role claims (handled via standard `ClaimsPrincipal.IsInRole`)

See `Tests/Security` for more information and examples.

## Blazor

Set of components and utilities to use in blazor server apps.

### Base list pages

These are pages to represent list of entities. Page is based on `ListQuery`/`SearchQuery`.
Filter support is also included, they're described mode detailed in next section.

### Filters in list pages

Filters are currently has several important virtual method calls in their lifecycle.

`InitializeFilterWithDefaultParameters` should be used when user comes to page with filters at first time. Also this method called when `Clear filters` button is pressed.
```csharp
protected override Task InitializeFilterWithDefaultParameters(SearchReservations.Query filter)
{
    filter.DateFrom = DateTime.Today;
    filter.DateTo = DateTime.Today.AddDays(14);
    filter.Statuses = new string[] { ReservationStatus.InHouse, ReservationStatus.Reservation };
    return Task.CompletedTask;
}
```

`OnAfterFilterRestoredAsync` is very important method - it should be used for filter fields that cannot be passed via query string. For example this may be important because of security reasons etc.
```charp
protected override async Task OnAfterFilterRestoredAsync(SearchReservations.Query filter)
{
    var state = await PrincipalAccessor.GetPrincipal();
    if (state.TryGetClaimValue<int>(Constants.LogusClaimTypes.CompanyId, out var companyId))
        filter.AgentCompanyId = companyId;
}
```